<?php
/*
* File name: config.inc.php
* Created by: Alex Pereiro
* Contact: trasno0@gmail.com
* Last modified: 21-01-2014
*
* Configuration file does the following things:
* - Has site settings in one location.
* - Stores URLs and URIs as constants.
* - Sets how errors will be handled.
*/

# ******************** #
# ***** SETTINGS ***** #

// Errors are emailed here.
$contact_email = 'info@hazplanes.com';
$events_email = 'eventos@hazplanes.com';

// Determine whether we're working on a local server
// or on the real server:
if(php_sapi_name() != 'cli') {
    if (stristr($_SERVER['HTTP_HOST'], 'local') || (substr($_SERVER['HTTP_HOST'], 0, 7) == '192.168') || (substr($_SERVER['HTTP_HOST'], 0, 7) == '127.0.0') || (substr($_SERVER['HTTP_HOST'], 0, 6) == '10.0.0')) {
        $local = TRUE;
    } else {
        $local = FALSE;
    }
} else {
    $local = TRUE;
}
//$local = FALSE;

// Determine location of files and the URL of the site:
// Allow for development on different servers.
if ($local) {

    // Always debug when running locally:
    $debug = TRUE;

    // Define the constants:
    define("BASE_URI", "/srv/http/hazplanes/");
    define("BASE_URL", "http://local.hazplanes/");
    define("BDTYPE","mysql");
    define("BDSERVER","127.0.0.1");
    define("BDPORT","");
    define("BDNAME","hazplanes");
    define("BDUSER","root");
    define("BDPASS","trasnete");
    define("BDPREFIX","");

} else {
    define("BASE_URI", "/var/www/alex/hazplanes/");
	if( isset($_SERVER['HTTPS'] ) ) {
    	define("BASE_URL", "https://hazplanes.com/");
	} else {
		define("BASE_URL", "http://hazplanes.com/");
	}
	define("BASE_URL_SSL", "https://hazplanes.com/");
    define("BDTYPE","mysql");
    define("BDSERVER","localhost");
    define("BDPORT","");
    define("BDNAME","hazplanes");
    define("BDUSER","alex");
    define("BDPASS","trasnete");
    define("BDPREFIX","");
}

//DEFINICIONES
define("PAGENAME","<span class='logo-part1'>Haz</span><span class='logo-part2'>Planes</span>");
//Versión de la web
//define("VERSION","alpha v0.96");
define("VERSION","v1.30");
//Copyright
define("COPYRIGHT","&copy; ".date("Y",strtotime("now")));
//Ruta de la api
define("APIPATH",BASE_URI."api/v2/");
//Ruta del directorio temporal de almacen
define("PATH_TEMP",BASE_URI."temp/");
//Ruta de los avatares y medidas
define("PATH_AVATAR",BASE_URI."img/avatar/");
define("AVATAR_H","150");
define("AVATAR_W","150");
define("AVATAR_TH","30");
define("AVATAR_TW","30");
//Medidas de posters
define("POSTER_H","350");
define("POSTER_W","350");
define("POSTER_TH","270");
define("POSTER_TW","270");
//Medidas de imagen
define("IMAGE_H","350");
define("IMAGE_W","350");
//Medidas de Fanart
define("FANART_H","1200");
define("FANART_W","1200");
//Rutas de los posters y fanart
define("PATH_CINE_POSTER","img/peliculas/posters/");
define("PATH_CINE_FANART","img/peliculas/fanart/");
define("PATH_CINE_CINES","img/cines/");
define("PATH_CINE_PROMOS","img/promos/");
define("PATH_ENTRADAS","img/entradas/");
define("PATH_TEATROS","img/teatros/");
define("PATH_LOCALES","img/locales/");
define("PATH_PABELLONES","img/pabellones/");
define("PATH_DEPORTES","img/deportes/posters/");
define("PATH_CURSOS","img/cursos/posters/");
define("PATH_CONCIERTO_POSTER","img/conciertos/posters/");
define("PATH_CONCIERTO_FANART","img/conciertos/fanart/");
define("PATH_OBRA_POSTER","img/obrasteatro/posters/");
define("PATH_OBRA_FANART","img/obrasteatro/fanart/");
define("PATH_EVENTO_POSTER","img/eventos/posters/");
define("PATH_EVENTO_FANART","img/eventos/fanart/");
define("PATH_EXPO_POSTER","img/expos/posters/");
define("PATH_EXPO_FANART","img/expos/fanart/");
define("PATH_MUSEOS","img/museos/");

$dias = array("Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

date_default_timezone_set("Europe/Madrid");

// Assume debugging is off.
if (!isset($debug)) {
    $debug = FALSE;
}

# ***** SETTINGS ***** #
# ******************** #


# **************************** #
# ***** ERROR MANAGEMENT ***** #

// Create the error handler.
function my_error_handler ($e_number, $e_message, $e_file, $e_line, $e_vars) {

    global $debug, $contact_email;

    $message = "An error occurred in script '$e_file' on line $e_line: \n<br />$e_message\n<br/>";
    $message .= "Date/Time: " . date('n-j-Y H:i:s') . "\n<br />";
    $message .= "<pre>" . print_r ($e_vars, 1) . "</pre>\n<br />";

    if ($debug) { // Show the error.
        echo '<p class="error">' . $message . '</p>';
    } else {
        // Log the error:
        error_log ($message, 1, $contact_email); // Send email.

        // Only print an error message if the error isn't a notice or strict.
        if ( ($e_number != E_NOTICE) && ($e_number < 2048)) {
            echo '<p class="error">Ha ocurrido un error en el sistema. Sentimos las molestias.</p>';
        }
    } // End of $debug IF.

    //Check if can write in log file
    $path = BASE_URI."log";
    $filename = $path."/error.log";
    if(!file_exists($filename) || is_writable($filename)) {
        if(!file_exists($filename)) {
            if(!is_dir($path)) {
                mkdir($path);
            }
            $fo = fopen($filename, "w");
            fclose($fo);
        } elseif(!is_writable($filename)) {
            chmod($filename, 0777);
        }
    }
    error_log ($message."\n", 3, $filename); // Log error in file.
} // End of my_error_handler() definition.

// Use my error handler:
set_error_handler ('my_error_handler');

# ***** ERROR MANAGEMENT ***** #
# **************************** #


//require_once('general.inc.php');
require_once('images.inc.php');
?>
