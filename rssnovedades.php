<?php
header('Content-type: text/xml; charset="iso-8859-1"', true);
echo '<?xml version="1.0" encoding="iso-8859-1"?>';

include_once("includes/api.inc.php");
include_once("includes/general.inc.php");
// Y generamos nuestro documento
echo "<rss version='2.0'>";
echo "<channel>
    <title>HazPlanes - Novedades</title>
    <link>http://hazplanes.com/</link>
    <language>es-ES</language>
    <description>Todas las novedades de la web</description>
    <generator>AperCloud</generator>";
$options = array(
    "novedades" => "creado",
    "limite" => 25
    );
$general = new GeneralController($options,$db);
$data = $general->readAction();
foreach($data as $k => $d) {
    $type = $d["tipoevento"];
    if($type == "deportes") {
        if(!empty($d["genero"])) {
            $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["genero"]);
            $tipodeporte = new Deportes($options2,$db);
            $d2 = $tipodeporte->readAction();
            if(!empty($d2)) {
                $d2 = $d2[0];
                $d["genero"] = $d2["nombre"];
            } else {
                $d["genero"] = "Sin especificar";
            }
        } else {
            $d["genero"] = "Sin especificar";
        }
    }
    switch($type) {
        case "peliculas":
            $type = "PELICULA";
            $link = "pelicula/";
            break;
        case "conciertos":
            $type = "CONCIERTO";
            $link = "concierto/";
            break;
        case "obrasteatro":
            $type = "TEATRO";
            $link = "obra/";
            break;
        case "eventos":
            $type = "EVENTO";
            $link = "evento/";
            break;
        case "exposiciones":
            $type = "EXPOSICIÓN";
            $link = "exposicion/";
            break;
        case "deportes":
            $type = "DEPORTE";
            $link = "competicion/";
            break;
        case "formacion":
            $type = "FORMACIÓN";
            $link = "curso/";
            break;
    }
    echo "<item>
        <title>".$d["nombre"]."</title>
        <link>".$link.$d["id"]."-".urlAmigable($d["nombre"])."</link>
        <pubDate>".$d["creado"]."</pubDate>
        <category>".$type."</category>
        <description><![CDATA[".$d["genero"]."]]></description>
    </item>";
}
echo "</channel>
    </rss>";
?>