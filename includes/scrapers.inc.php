<?php
/*
 * Funciones comunes a los scrapers
 */

function is3d($titulo) {
    if(stripos($titulo,"(3D)") !== false || stripos($titulo," 3d") !== false || stripos($titulo,"3d ") !== false || stripos($titulo," 3d ") !== false  || stripos($titulo,"(3d)") !== false) {
        return 1;
    } else {
        return 0;
    }
}

function isVo($titulo) {
    if(stripos($titulo,'vose') !== false || stripos($titulo,'v.o.s.e.') !== false || stripos($titulo,'v.o.') !== false || stripos($titulo,'v.o') !== false || stripos($titulo,'v.o.s.') !== false || stripos($titulo,'V.o.s.') !== false) {
        return 1;
    } else {
        return 0;
    }
}

function getEventType($titulo) {
    if(stripos(strtolower($titulo),'met live') !== false) {
        return "obra";
    } else {
        return "pelicula";
    }
}

function normalize_title($titulo) {
    $titulo = str_replace("(VOSE)","",$titulo);
    $titulo = str_replace("(V.O.S.E.)","",$titulo);
    $titulo = str_replace("(vose)","",$titulo);
    $titulo = str_replace("(v.o.s.e)","",$titulo);
    $titulo = str_replace("(V.O.)","",$titulo);
    $titulo = str_replace("(v.o.)","",$titulo);
    $titulo = str_replace("VOSE","",$titulo);
    $titulo = str_replace("V.O.S.E.","",$titulo);
    $titulo = str_replace("vose","",$titulo);
    $titulo = str_replace("v.o.s.e","",$titulo);
    $titulo = str_replace("V.O.","",$titulo);
    $titulo = str_replace("v.o.","",$titulo);
    $titulo = str_replace("V.O.S.","",$titulo);
    $titulo = str_replace("v.o.s.","",$titulo);
    $titulo = str_replace("V.o.s.","",$titulo);
    $titulo = str_replace("(HD)","",$titulo);
    $titulo = str_replace("(3D)","",$titulo);
    $titulo = str_replace("(3d)","",$titulo);
    $titulo = str_replace(" 3d ","",$titulo);
    $titulo = str_replace("3d ","",$titulo);
    $titulo = str_replace(" 3d","",$titulo);
    $titulo = str_replace(" (digital)","",$titulo);
    $titulo = trim($titulo);
    //$titulo = utf8_encode($titulo);
    $titulo = strtolower($titulo);
    $titulo = str_replace("Ñ","ñ",$titulo);
    $titulo = str_replace("Ü","ü",$titulo);
    $titulo = str_replace("Ú","u",$titulo);
    $titulo = str_replace("Ó","o",$titulo);
    $titulo = str_replace("Í","i",$titulo);
    $titulo = str_replace("É","e",$titulo);
    $titulo = str_replace("Á","a",$titulo);
    $titulo = preg_replace("/\(.*\)/i", "", $titulo);
    $titulo = ucwords($titulo);

    return trim($titulo);
}

function getAllUrlCompra($filter) {
    global $db;

    $query = "select distinct urlcompra from (
        select c.urlcompra from cines_has_deportes c where urlcompra like '%".$filter."%' union
        select c.urlcompra from locales_has_deportes c where urlcompra like '%".$filter."%' union
        select c.urlcompra from teatros_has_deportes c where urlcompra like '%".$filter."%' union
        select c.urlcompra from pabellones_has_deportes c where urlcompra like '%".$filter."%' union
        select c.urlcompra from museos_has_deportes c where urlcompra like '%".$filter."%' union

        select c.urlcompra from cines_has_cursos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from locales_has_cursos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from teatros_has_cursos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from museos_has_cursos c where urlcompra like '%".$filter."%' union

        select c.urlcompra from cines_has_obrasteatro c where urlcompra like '%".$filter."%' union
        select c.urlcompra from locales_has_obrasteatro c where urlcompra like '%".$filter."%' union
        select c.urlcompra from teatros_has_obrasteatro c where urlcompra like '%".$filter."%' union
        select c.urlcompra from museos_has_obrasteatro c where urlcompra like '%".$filter."%' union

        select c.urlcompra from cines_has_conciertos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from locales_has_conciertos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from teatros_has_conciertos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from museos_has_conciertos c where urlcompra like '%".$filter."%' union

        select c.urlcompra from cines_has_peliculas c where urlcompra like '%".$filter."%' union
        select c.urlcompra from locales_has_peliculas c where urlcompra like '%".$filter."%' union
        select c.urlcompra from teatros_has_peliculas c where urlcompra like '%".$filter."%' union
        select c.urlcompra from museos_has_peliculas c where urlcompra like '%".$filter."%' union

        select c.urlcompra from cines_has_eventos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from locales_has_eventos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from teatros_has_eventos c where urlcompra like '%".$filter."%' union
        select c.urlcompra from museos_has_eventos c where urlcompra like '%".$filter."%'


    ) as consulta order by urlcompra asc;";
    $r = $db->query($query);

    return $r;
}

function existPeliculaUrlCompra($filter) {
    global $db;

    $query = "select c.urlcompra from cines_has_peliculas c where urlcompra='".$filter."'";
    $r = $db->query($query);
    if($db->count($r) > 0) {
        return true;
    } else {
        return false;
    }
}

function existPelicula($name) {
    global $db;

    $query = "select nombre from peliculas where nombre='".$db->secure_field($name)."'";
    $r = $db->query($query);
    if($db->count($r) > 0) {
        return true;
    } else {
        return false;
    }
}


function file_get_contents_curl($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

?>