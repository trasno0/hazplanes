<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $cancel = "index.php?p=adminlistfilm";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "idpelicula");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newpelicula">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        if(isset($id) && !empty($id)) {
            $options = array("idpelicula" => $id);
            $pelicula = new Peliculas($options,$db);
            $d = $pelicula->readAction();
            $d = $d[0];
            $act = "m";
            $d["publicado"] == 0? $d["publicado"] = "" : $d["publicado"] = "checked";
            $d["infantil"] == 0? $d["infantil"] = "" : $d["infantil"] = "checked";
            $d["reserva"] == 0? $d["reserva"] = "" : $d["reserva"] = "checked";
            //Convertimos los horarios de la BD en json para angularjs
            $horarios = horariosToJson($d);
        } else {
            $d = array(
                "idpelicula" => "",
                "nombre" => "",
                "original" => "",
                "director" => "",
                "actores" => "",
                "nacionalidad" => "",
                "distribuidora" => "",
                "edad" => "",
                "duracion" => "",
                "genero" => "",
                "trailer" => "",
                "web" => "",
                "fechaestreno" => "",
                "sinopsis" => "",
                "notas" => "",
                "poster" => "",
                "fanart" => "",
                "menciones" => "",
                "publicado" => "checked",
                "infantil" => "",
                "fichafa" => "",
                "fichaimdb" => "",
                "fichatmdb" => "",
                "reserva" => "",
                "limitereserva" => ""
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}
?>
    <script>
        $(document).ready(function() {
            initialize();
        });
    </script>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo "<div class='error'>".$msg["errormsg"]."</div>";
                exit;
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar película ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo película ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newpelicula" id="newpelicula" class="form" action="index.php?p=adminformfilm" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="idpelicula" value="<?php echo $d["idpelicula"]; ?>"/>
                <input type="hidden" name="from" value="form"/>
                <div class='divsmall'>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" value="<?php echo htmlspecialchars($d["nombre"]); ?>" />
                    <label for="form-nacionalidad">Nacionalidad</label>
                    <input type="text" name="nacionalidad" id="form-nacionalidad" class="campo" value="<?php echo $d["nacionalidad"]; ?>" />
                    <label for="form-trailer">Trailer</label>
                    <input type="url" name="trailer" id="form-trailer" class="campo" value="<?php echo $d["trailer"]; ?>" />
                    <label for="form-fichafa">Ficha en FilmAffinity</label>
                    <input type="url" name="fichafa" id="form-fichafa" class="campo" value="<?php echo $d["fichafa"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-original">Nombre original</label>
                    <input type="text" name="original" id="form-original" class="campo" value="<?php echo $d["original"]; ?>" />
                    <label for="form-distribuidora">Distribuidora</label>
                    <input type="text" name="distribuidora" id="form-distribuidora" class="campo" value="<?php echo $d["distribuidora"]; ?>" />
                    <label for="form-web">Página web</label>
                    <input type="url" name="web" id="form-web" class="campo" value="<?php echo $d["web"]; ?>" />
                    <label for="form-fichaimdb">Ficha en IMDB</label>
                    <input type="url" name="fichaimdb" id="form-fichaimdb" class="campo" value="<?php echo $d["fichaimdb"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-genero">Género</label>
                    <input type="text" name="genero" id="form-genero" class="campo" value="<?php echo $d["genero"]; ?>" />
                    <label for="form-edad">Edad</label>
                    <input type="number" name="edad" id="form-edad" class="campo" value="<?php echo $d["edad"]; ?>" />
                    <label for="form-fechaestreno">Fecha de estreno</label>
                    <input type="datetime" name="fechaestreno" id="form-fechaestreno" class="campo" value="<?php echo $d["fechaestreno"]; ?>" />
                    <label for="form-fichatmdb">Ficha en The Movie Database</label>
                    <input type="url" name="fichatmdb" id="form-fichatmdb" class="campo" value="<?php echo $d["fichatmdb"]; ?>" />
                </div>
                <div class='divbig'>
                    <div style='display: inline-block;'>
                        <label for="form-publicado">Publicado</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                            <label class="check" for="form-publicado"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-infantil">Infantil</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-infantil" name="infantil" <?php echo $d["infantil"]; ?> />
                            <label class="check" for="form-infantil"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-reserva">Reservar</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-reserva" name="reserva" <?php echo $d["reserva"]; ?> />
                            <label class="check" for="form-reserva"></label>
                        </span>
                        <input type='datetime' id='form-limitereserva' name='limitereserva' class="campo field-limitereserva" value="<?php echo $d["limitereserva"]; ?>" />
                    </div>
                    <label for="form-duracion">Duración</label>
                    <input type="number" name="duracion" id="form-duracion" class="campo" value="<?php echo $d["duracion"]; ?>" />
                </div>
                <div class='divbig'>
                    <label for="form-director">Director/es</label>
                    <textarea name="director" id="form-director" class="campo"><?php echo $d["director"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-actores">Actores</label>
                    <textarea name="actores" id="form-actores" class="campo"><?php echo $d["actores"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-sinopsis">Sinopsis</label>
                    <textarea name="sinopsis" id="form-sinopsis" class="campo"><?php echo $d["sinopsis"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-notas">Notas</label>
                    <textarea name="notas" id="form-notas" class="campo"><?php echo $d["notas"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-menciones">Menciones / Agradecimientos / Créditos</label>
                    <textarea name="menciones" id="form-menciones" class="campo"><?php echo $d["menciones"]; ?></textarea>
                </div>
                <div class='divbig'>
                </div>
                <div class='divbig'>
                    <label for="form-posterURL">Poster desde URL</label>
                    <input type="url" name="posterURL" id="form-posterURL" class="campo" />
                    <input type="file" style='display:none;' name="poster" id="form-poster" class="campo" />
                    <button type="button" onclick='$("#form-poster").click();'>Subir</button>
                    <?php
                    if(!empty($d["poster"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["poster"]."?".strtotime("now")."' alt='Poster de la película' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg1' onclick='deleteImage(this,".$d["idpelicula"].",\"".$d["poster"]."\",\"Peliculas\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <div class='divbig'>
                    <label for="form-fanartURL">Fanart desde URL</label>
                    <input type="text" name="fanartURL" id="form-fanartURL" class="campo" />
                    <input type="file" style='display:none;' name="fanart" id="form-fanart" class="campo" />
                    <button type="button" onclick='$("#form-fanart").click();'>Subir</button>
                    <?php
                    if(!empty($d["fanart"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["fanart"]."?".strtotime("now")."' alt='Fanart de la película' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg2' onclick='deleteImage(this,".$d["idpelicula"].",\"".$d["fanart"]."\",\"Peliculas\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <br>
                <?php
                formHorariosCines("pelicula");
                echo "<br>";
                formHorariosLocales("pelicula");
                echo "<br>";
                formHorariosAuditorios("pelicula");
                echo "<br>";
                formHorariosMuseos("pelicula");
                echo "<br>";
                formHorariosLugares("pelicula");
                ?>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Película creada con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Película modificada con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformfilm&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformfilm">Crear una nueva</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>
    <script>
        cines = <?php if(!empty($horarios["horasCine"])) echo $horarios["horasCine"]; else echo "[]"; ?>;
        lugares = <?php if(!empty($horarios["horasLugar"])) echo $horarios["horasLugar"]; else echo "[]"; ?>;
        locales = <?php if(!empty($horarios["horasLocal"])) echo $horarios["horasLocal"]; else echo "[]"; ?>;
        auditorios = <?php if(!empty($horarios["horasAuditorio"])) echo $horarios["horasAuditorio"]; else echo "[]"; ?>;
        museos = <?php if(!empty($horarios["horasMuseo"])) echo $horarios["horasMuseo"]; else echo "[]"; ?>;
    </script>