<?php
/**
 * Clase con metodos para operaciones con servicios de un cine
 *
 * @package API
 * @author Trasno  
 */
class ServiciosItem {
    protected $db;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }
    
    /**
     * Coge todos los servicios
     *
     * @param string $order (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listServicios($order = "idservicios") {
        $query = sprintf("select s.* from %sservicios s order by c.%s", BDPREFIX, $this->db->secure_field($order));
        $r = $this->db->query($query);
        
        $result = array();
        while($servicios = $this->db->fetch($r)) {
            $result[] = $servicios;
        }
        
        return $result;
    }
    
    /**
     * Coge los servicios de un cine y todos sus datos
     *
     * @param int $idservicios id de los servicios 
     * @return array|false
     */
     public function dataServicios($idcine) {
        if(!empty($idcine)) {
            $query = sprintf("select s.* from %sservicios s where s.idcine = %d", BDPREFIX, $this->db->secure_field($idcine));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }
     
    /*
     * Eliminar un servicios
     *
     * @param int $idservicios id de los servicios
     * @return boolean
     */
    public function deleteServicios($idcine) {
        if(!empty($idcine)) {
            $query = sprintf("delete from %sservicios where idcine = %d",BDPREFIX, $this->db->secure_field($idcine));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            }
        }
        return false;
    }
    
    /*
     * Insertar servicios de un cine
     *
     * @param array $servicios datos del servicios
     * @param int $servicios['idcine']
     * @param boolean $servicios['minusvalidos']
     * @param boolean $servicios['hd']
     * @param boolean $servicios['3d']
     * @param boolean $servicios['parking']
     * @param boolean $servicios['carnetjoven']
     * @param boolean $servicios['carnetestudiante']
     * @return int
     */
    public function addServicios($servicios) {
        if(!$this->checkServicios($servicios["idcine"])) {
            $fields = "";
            $values = "";
            foreach($servicios as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %sservicios (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addServicios] Error en la query: ".$query, 1);    
            }
        } else {
            throw new Exception("[addServicios] Ya existen servicios para ese cine.", 1);
        }
    }
    
    /*
     * Actualiza los campos de servicios de un cine
     *
     * @param array $servicios datos de servicios
     * @param int $servicios['idservicios'] identificador de servicios
     * @param int $servicios['idcine'] identificador del cine
     * @param boolean $servicios['minusvalidos']
     * @param boolean $servicios['hd']
     * @param boolean $servicios['3d']
     * @param boolean $servicios['parking']
     * @param boolean $servicios['carnetjoven']
     * @param boolean $servicios['carnetestudiante']
     * @return boolean
     */
    public function updateServicios($servicios) {
        if($this->checkServicios($servicios["idcine"])) {
            $fields = "";
            $serviciosACero = array("minusvalidos" => 0, "hd" => 0, "3d" => 0, "parking" => 0, "carnetjoven" => 0, "carnetestudiante" => 0,);
            $servicios = array_merge($serviciosACero,$servicios);
            foreach($servicios as $key => $value) {
                if($key != "idservicios" && $key != "idcine") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %sservicios set %s where idcine = %d", BDPREFIX, $fields, $this->db->secure_field($servicios["idcine"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateServicios] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateServicios] No existen servicios para ese cine.", 1);
        }
    }
    
    /*
     * Comprueba si existen los servicios de un cine
     * 
     * @param int $idcine id del cine a comprobar
     * @return int|false
     */
    private function checkServicios($idcine) {
        $query = sprintf("select s.* from %sservicios s where s.idcine = %d", BDPREFIX, $this->db->secure_field($idcine));
        $r = $this->db->query($query);
        
        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END