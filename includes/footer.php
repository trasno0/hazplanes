    <?php if(strpos($p,"admin") === false) { ?>
    <footer>
        <div id='footer-social'>
            <a href='https://play.google.com/store/apps/details?id=com.hazplanes.hazplanes' target='_blank'><img src='img/interface/logops-29.png' alt='Logo Google Play Store' width='29' height='29'/></a>
            <a href='https://plus.google.com/+HazplanesWeb?rel=author' target='_blank'><img src='img/interface/logogp-29.png' alt='Logo Google+' width='29' height='29'/></a>
            <a href='https://www.facebook.com/hazplanesweb' target='_blank'><img src='img/interface/logofb-29.png' alt='Logo Facebook' width='29' height='29'/></a>
            <a href='https://twitter.com/HazPlanes' target='_blank'><img src='img/interface/logotw-29.png' alt='Logo Twitter' width='29' height='29'/></a>
        </div>
    </footer>
    <?php } ?>
    <?php
    if((!isset($_COOKIE["cookies"]) || $_COOKIE["cookies"] != 1) && strpos($p,"admin") === false) {
    ?>
    <div id="cookie-law-info-bar"><span>Utilizamos cookies propias y de terceros para mejorar nuestros servicios y mostrarte publicidad relacionada con tus preferencias mediante el análisis de tus hábitos de navegación. Si continúas navegando, consideramos que aceptas su uso. <a href="#" id="cookie_action_close_header">Aceptar</a> <a href="politica-cookies">Más información</a></span></div>
    <?php
    }
    ?>
<?php
if(isset($dbc)){
    db_close($dbc);
}
if(strpos($p,"admin") === false && $local === false) {

?>

<!--Google Analytics-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48359390-1', 'hazplanes.com');
  ga('send', 'pageview');

</script>

    <?php
    if((isset($id) && !empty($id)) || $p == "acercade") {
    ?>
    <!--Compartir Google+-->
    <!-- Place this tag after the last share tag. -->
    <script type="text/javascript">
      window.___gcfg = {lang: 'es',parsetags: 'onload'};

      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script>
    <!--Fin compartir Google+-->

    <!--Compartir twitter-->
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    <!--Fin compartir twitter-->

    <!--Compartir facebook-->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&appId=1496972530518484&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!--Fin compartir facebook-->

<?php
    }
} elseif(strpos($p,"admin") !== false) {
    ?>
    <script type="text/javascript" src="js/app.js"></script>
    <?php
}
?>
    <!--<script type="text/javascript">
        var $buoop = {};
        $buoop.ol = window.onload;
        window.onload=function(){
            try {if ($buoop.ol) $buoop.ol();}catch (e) {}
            var e = document.createElement("script");
            e.setAttribute("type", "text/javascript");
            e.setAttribute("src", "//browser-update.org/update.js");
            document.body.appendChild(e);
        }
    </script>-->
    <script type="text/javascript" src="js/jquery-ui-1.10.4.min.js"></script>
</body>
</html>
