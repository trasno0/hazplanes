<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $salas = "";
    $entradas = "";
    $promos = "";
    $cancel = "index.php?p=adminlistcine";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "idcine");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newcine">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        $diaespectador = array(
            "Ninguno" => "",
            "Lunes" => "",
            "Martes" => "",
            "Miércoles" => "",
            "Jueves" => "",
            "Viernes" => "",
            "Sábado" => "",
            "Domingo" => "",
            );
        $entrada = new Entradas("",$db);
        $allEntradas = $entrada->readAction();
        $promo = new Promociones("",$db);
        $allPromos = $promo->readAction();
        if(isset($id) && !empty($id)) {
            $options = array("idcine" => $id);
            $cine = new Cines($options,$db);
            $d = $cine->readAction();
            $d = $d[0];
            $diaespectador[$d["diadelespectador"]] = "selected";
            $act = "m";
            if($d["publicado"] == 0) {
                $d["publicado"] = "";
            } elseif($d["publicado"] == 1) {
                $d["publicado"] = "checked";
            }
            $sala = new Salas($options,$db);
            $s = $sala->readAction();
            foreach($s as $k => $sala) {
                $salas = $salas."{idsala: '".$sala["idsala"]."', numero: '".$sala["numero"]."', capacidad: '".$sala["capacidad"]."', filas: '".$sala["filas"]."', numerada: ".$sala["numerada"]."},";
            }
            foreach($d["entradas"] as $k => $ent) {
               $entradas = $entradas."{id: '".$ent["identradas"]."'},";
            }
            foreach($d["promos"] as $k => $prom) {
                $promos = $promos."{id: '".$prom["idpromocion"]."'},";
            }
            $serv = new Servicios($options,$db);
            $servicios = $serv->readAction();
            $servicios = array_change_key_case($servicios[0], CASE_LOWER);
            foreach($servicios as $key => $value) {
                if($value == 0) {
                    $servicios[$key] = "";
                } elseif($value == 1) {
                    $servicios[$key] = "checked";
                }
            }
        } else {
            $d = array(
                "idcine" => "",
                "nombre" => "",
                "direccion" => "",
                "localidad" => "",
                "provincia" => "",
                "codigopostal" => "",
                "coordenadas" => "",
                "telefono" => "",
                "email" => "",
                "web" => "",
                "twitter" => "",
                "facebook" => "",
                "google" => "",
                "otrasocial" => "",
                "notas" => "",
                "descripcion" => "",
                "imagen" => "",
                "menciones" => "",
                "publicado" => "",
                "normal" => "",
                "reducida" => "",
                "diaespectador" => "",
                "normal3d" => "",
                "reducida3d" => "",
                "diaespectador3d" => "",
                "gafas" => "",
                "butacaespecial" => "",
                "laborables" => "",
                "findesyfestivos" => "",
                "tipolocal" => ""
                );
            $servicios = array(
                "minusvalidos" => "",
                "hd" => "",
                "3d" => "",
                "parking" => "",
                "carnetjoven" => "",
                "carnetestudiante" => "",
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}

if(!empty($d["coordenadas"])) {
    $d["coordenadas"] = trim($d["coordenadas"],"\(\)");
    $coord = explode(",",$d["coordenadas"]);
} else {
    $coord = array("","");
}
?>
    <script>
        $(document).ready(function() {
            initialize();
            <?php
            if(!empty($d["coordenadas"])) {
                echo "setPoints('".$coord[0]."','".$coord[1]."');";
            }
            ?>
        });
    </script>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo $msg["errormsg"];
                exit;
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar cine ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo cine ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newcine" id="newcine" class="form" action="index.php?p=adminformcine" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="idcine" value="<?php echo $d["idcine"]; ?>"/>
                <div class='divsmall'>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" value="<?php echo htmlspecialchars($d["nombre"]); ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-direccion">Dirección</label>
                    <input type="text" name="direccion" id="form-direccion" class="campo" value="<?php echo $d["direccion"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-localidad">Localidad</label>
                    <input type="text" name="localidad" id="form-localidad" class="campo" value="<?php echo $d["localidad"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-provincia">Provincia</label>
                    <input type="text" name="provincia" id="form-provincia" class="campo" value="<?php echo $d["provincia"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-codigopostal">Código Postal</label>
                    <input type="text" name="codigopostal" id="form-codigopostal" class="campo" value="<?php echo $d["codigopostal"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-telefono">Teléfono</label>
                    <input type="text" name="telefono" id="form-telefono" class="campo" value="<?php echo $d["telefono"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-email">E-Mail</label>
                    <input type="email" name="email" id="form-email" class="campo" value="<?php echo $d["email"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-web">Página web</label>
                    <input type="url" name="web" id="form-web" class="campo" value="<?php echo $d["web"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-twitter">Twitter</label>
                    <input type="url" name="twitter" id="form-twitter" class="campo" value="<?php echo $d["twitter"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-facebook">Facebook</label>
                    <input type="url" name="facebook" id="form-facebook" class="campo" value="<?php echo $d["facebook"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-google">Google+</label>
                    <input type="url" name="google" id="form-google" class="campo" value="<?php echo $d["google"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-otrasocial">Otra web social</label>
                    <input type="url" name="otrasocial" id="form-otrasocial" class="campo" value="<?php echo $d["otrasocial"]; ?>" />
                </div>
                <div class='divbig'>
                    <label for="form-descripcion">Descripción</label>
                    <textarea name="descripcion" id="form-descripcion" class="campo"><?php echo $d["descripcion"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-notas">Notas</label>
                    <textarea name="notas" id="form-notas" class="campo"><?php echo $d["notas"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-menciones">Menciones / Agradecimientos / Créditos</label>
                    <textarea name="menciones" id="form-menciones" class="campo"><?php echo $d["menciones"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-imagenURL">Imagen desde URL</label>
                    <input type="url" name="imagenURL" id="form-imagenURL" class="campo" />
                    <input type="file" name="imagen" style='display:none;' id="form-imagen" class="campo" />
                    <button type="button" onclick='$("#form-imagen").click();'>Subir</button>
                    <?php
                    if(!empty($d["imagen"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["imagen"]."' alt='Imagen del cine' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg' onclick='deleteImage(this,".$d["idcine"].",\"".$d["imagen"]."\",\"Cines\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <div class='divbig'>
                    <label for="form-diaespectador">Día del espectador</label>
                    <select name="diadelespectador" id="form-diaespectador" class="campo">
                        <option value="Ninguno" <?php echo $diaespectador["Ninguno"]; ?>>No tiene</option>
                        <option value="Lunes" <?php echo $diaespectador["Lunes"]; ?>>Lunes</option>
                        <option value="Martes" <?php echo $diaespectador["Martes"]; ?>>Martes</option>
                        <option value="Miércoles" <?php echo $diaespectador["Miércoles"]; ?>>Miércoles</option>
                        <option value="Jueves" <?php echo $diaespectador["Jueves"]; ?>>Jueves</option>
                        <option value="Viernes" <?php echo $diaespectador["Viernes"]; ?>>Viernes</option>
                        <option value="Sábado" <?php echo $diaespectador["Sábado"]; ?>>Sábado</option>
                        <option value="Domingo" <?php echo $diaespectador["Domingo"]; ?>>Domingo</option>
                    </select>
                </div>
                <div class='divbig'>
                    <div style='display: inline-block;'>
                        <label for="form-publicado">Publicado</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                            <label class="check" for="form-publicado"></label>
                        </span>
                    </div>
                    <div style="padding: 10px 20px;">
                        Votos: <?php echo isset($d["numvotos"]) && !empty($d["numvotos"])?$d["numvotos"]:0 ?>
                        <br>Media: <?php echo number_format(isset($d["numvotos"]) && !empty($d["numvotos"])?$d["puntos"]/$d["numvotos"]:0,2); ?>
                    </div>
                </div>
                <div class='divbig'>
                    <label for="form-tipolocal">Tipo de local</label>
                    <input type="text" name="tipolocal" id="form-tipolocal" class="campo" value="<?php echo $d["tipolocal"]; ?>" />
                </div>
                <table class='tabla-fechas'>
                <caption style='text-align: left'>Coordenadas</caption>
                <tr>
                    <td colspan='4'>
                        <input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
                        <div id="map_canvas"></div>
                        <input type="hidden" name="coordenadas" id="coordenadastemp" value='<?php echo $d["coordenadas"]; ?>' />
                    </td>
                    <td colspan='4'>
                        <label for='form-coordenadas'>Coordenadas <div id="puntosPoly"><?php echo "(".$d["coordenadas"].")"; ?></div></label>
                        <button type="button" id='cleanCoord'>Borrar</button>
                    </td>
                </tr>
                </table>
                <br>
                <table class="tabla">
                    <tr>
                        <th>Minusválidos</th>
                        <th>HD</th>
                        <th>3D</th>
                        <th>Parking</th>
                        <th>Carnet joven</th>
                        <th>Carnet estudiante</th>
                    </tr>
                    <tr style='text-align: center;'>
                        <td>
                            <input type="checkbox" name="servicios[minusvalidos]" <?php echo $servicios["minusvalidos"]; ?> />
                        </td>
                        <td>
                            <input type="checkbox" name="servicios[hd]" <?php echo $servicios["hd"]; ?> />
                        </td>
                        <td>
                            <input type="checkbox" name="servicios[3d]" <?php echo $servicios["3d"]; ?> />
                        </td>
                        <td>
                            <input type="checkbox" name="servicios[parking]" <?php echo $servicios["parking"]; ?> />
                        </td>
                        <td>
                            <input type="checkbox" name="servicios[carnetjoven]" <?php echo $servicios["carnetjoven"]; ?> />
                        </td>
                        <td>
                            <input type="checkbox" name="servicios[carnetestudiante]" <?php echo $servicios["carnetestudiante"]; ?> />
                        </td>
                    </tr>
                </table>
                <table class="tabla">
                    <tr>
                        <th colspan='4'>Tarifas</th>
                    </tr>
                    <tr>
                        <td>Normal</td>
                        <td><input type="text" name="tarifas[normal]" value="<?php echo $d["normal"]; ?>" /></td>
                        <td>Normal 3D</td>
                        <td><input type="text" name="tarifas[normal3d]" value="<?php echo $d["normal3d"]; ?>" /></td>
                    </tr>
                    <tr>
                        <td>Reducida</td>
                        <td><input type="text" name="tarifas[reducida]" value="<?php echo $d["reducida"]; ?>" /></td>
                        <td>Reducida 3D</td>
                        <td><input type="text" name="tarifas[reducida3d]" value="<?php echo $d["reducida3d"]; ?>" /></td>
                    </tr>
                    <tr>
                        <td>Día del espectador</td>
                        <td><input type="text" name="tarifas[diaespectador]" value="<?php echo $d["diaespectador"]; ?>" /></td>
                        <td>Día del espectador 3D</td>
                        <td><input type="text" name="tarifas[diaespectador3d]" value="<?php echo $d["diaespectador3d"]; ?>" /></td>
                    </tr>
                    <tr>
                        <td>Laborables</td>
                        <td><input type="text" name="tarifas[laborables]" value="<?php echo $d["laborables"]; ?>" /></td>
                        <td>Fin de semana y festivos</td>
                        <td><input type="text" name="tarifas[findesyfestivos]" value="<?php echo $d["findesyfestivos"]; ?>" /></td>
                    </tr>
                    <tr>
                        <th colspan='4'>Extras</th>
                    </tr>
                    <tr>
                        <td>Gafas</td>
                        <td><input type="text" name="tarifas[gafas]" value="<?php echo $d["gafas"]; ?>" /></td>
                        <td>Butaca especial</td>
                        <td><input type="text" name="tarifas[butacaespecial]" value="<?php echo $d["butacaespecial"]; ?>" /></td>
                    </tr>
                </table>
                <fieldset>
                    <legend>Salas</legend>
                    <div ng-repeat="sala in thing.salas">
                        <input type="hidden" name="sala[{{$index}}][idsala]" value="{{sala.idsala}}" />
                        Número: <input type="text" name="sala[{{$index}}][numero]" class="campo small" ng-model="sala.numero" />
                        Capacidad: <input type="text" name="sala[{{$index}}][capacidad]" class="campo medium" ng-model="sala.capacidad" />
                        Filas: <input type="text" name="sala[{{$index}}][filas]" class="campo" ng-model="sala.filas" />
                        Numerada: <input type="checkbox" name="sala[{{$index}}][numerada]" class="campo" ng-model="sala.numerada" ng-checked="sala.numerada" />
                        <button class="campo small" ng-click="removeSala(sala)">&times;</button>
                    </div>
                    <br>
                    <button type="button" ng-click="addSala()">Añadir</button><br>
                </fieldset>
                <br>
                <fieldset>
                    <legend>Venta de entradas</legend>
                    <div ng-repeat="entrada in thing.entradas">
                        <select name="entradas[]" class="campo" ng-model="entrada.id">
                            <?php
                            foreach($allEntradas as $k => $ent) {
                                echo "<option value='".$ent["identradas"]."'>".$ent["nombre"]."</option>";
                            }
                            ?>
                        </select>
                        <button class="campo small" ng-click="removeEntrada(entrada)">&times;</button>
                    </div>
                    <br>
                    <button type="button" ng-click="addEntrada()">Añadir</button><br>
                </fieldset>
                <br>
                <fieldset>
                    <legend>Promociones</legend>
                    <div ng-repeat="promo in thing.promos">
                        <select name="promos[]" class="campo" ng-model="promo.id">
                            <?php
                            foreach($allPromos as $k => $prom) {
                                echo "<option value='".$prom["idpromocion"]."'>".$prom["nombre"]."</option>";
                            }
                            ?>
                        </select>
                        <button class="campo small" ng-click="removePromo(promo)">&times;</button>
                    </div>
                    <br>
                    <button type="button" ng-click="addPromo()">Añadir</button><br>
                </fieldset>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Cine creado con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Cine modificado con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformcine&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformcine">Crear uno nuevo</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>
    <script>
        salas = [<?php echo $salas; ?>];
        promos = [<?php echo $promos; ?>];
        entradas = [<?php echo $entradas; ?>];
    </script>