<?php
pageAccessControl(1);
?>
    <section id='content'>
        <div class="grid">
            <section id='datos'>
                <div class="header-list">
                    <h2>Promociones <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformpromo'">Nueva promoción</button></span></h2>
                    <div id="listOptions">
                        <button type="button" c='Promociones' id='publish'>Publicar</button>
                        <button type="button" c='Promociones' id='unpublish'>No publicar</button>
                        <button type="button" c='Promociones' id='delete'>Eliminar</button>
                    </div>
                </div>
                <table id="list" class="tabla">
                    <thead>
                        <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                        <th class='listPublishedCell'>Publicado</th>
                        <th>Nombre</th>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th>Poster</th>
                    </thead>
                    <?php
                    $promo = new Promociones("",$db);
                    $data = $promo->readAction();
                    foreach($data as $k => $d) {
                        echo "<tr class='row'>
                            <td><input type='checkbox' name='checkListItem' id='".$d["idpromocion"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                            <td>".($d["publicado"]?"Si":"No")."</td>
                            <td><a href='index.php?p=adminformpromo&i=".$d["idpromocion"]."'>".$d["nombre"]."</a></td>
                            <td>".date("d-m-Y",strtotime($d["inicio"]))."</td>
                            <td>".date("d-m-Y",strtotime($d["fin"]))."</td>
                            <td>".(empty($d["imagen"])?"No":"Si")."</td>
                        </tr>";
                    }
                    ?>
                </table>
            </section>
        </div>
    </section>
