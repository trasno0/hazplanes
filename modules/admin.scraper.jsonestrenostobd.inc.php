<?php
pageAccessControl(1);

try {
    if(isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        unset($newitem["search_db"], $newitem["search_tmdb"], $newitem["search_fa"]);
        $id = formAction($newitem, $action, "idpelicula");
    }
    if($totalPelis < 1) {
        $titulo = "";
        $horas = array();
    }
    $msg['success'] = true;
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}

?>
    <style>
        table { width: 100%; }
        td { vertical-align:top; }
        th { background-color: #ddd; color: black; padding: 10px 0; }
        .form { background: none; margin-top: 0;}
        form label { display:inline-block; width: 200px; height: auto; text-align:left; }
        input, textarea { width: 69%; }
        textarea { height: 120px; }
        button { vertical-align: top; }
        .medium { min-width: 80px; max-width: 150px; }
        .small { min-width: 50px; max-width: 50px; }
        //.checkbox { display: inline-block; width:initial; }
    </style>
    <script>
        $(function() {
            $(".copybtn").on("click",function(){
                var id = $(this).prev().attr("id").split("-");
                var val = $(this).prev().val();
                $("#dbform-"+id[1]).val(val);
            });
            $(".copyallbtntmdb").on("click",function(){
                $('.tmdbform').each(function( index ) {
                    var id = $(this).attr("id").split("-");
                    if(id[1] == "nombre") {
                        $("#dbform-nombre").val($("#nombreabuscar").val());
                    } else {
                        if(id[1] != "fechaestreno") {
                            var val = $(this).val();
                            if(val == 0 || val == "") {
                                val = $("#faform-"+id[1]).val();
                            }
                            $("#dbform-"+id[1]).val(val);
                        }
                    }
                });
                $("#dbform-fichafa").val($("#faform-fichafa").val());
            });
        });
    </script>
    <section id='content' ng-app>
        <div class="grid">
            <section id='datos' ng-controller="Controller">
                <?php
                if(!$msg['success']) {
                    echo "Error: ".$msg['errormsg'];
                }
                ?>
                <h2><?php echo $page_title; ?></h2>
                <form name="newpeliculadb" id="newpeliculadb" class="form" action="index.php?p=adminscraperjsonestrenostobd&pag=<?php echo $pag+1; ?>" method="post" enctype="multipart/form-data">
                    <?php
                    echo "<div class='paginacion' style='margin: auto; text-align: center; display: block;'>
                            <table style='width: 300px; margin: auto;'>
                            <tr>
                                <td style='width: 100px;'>";
                    if($pag > 1) {
                        echo '<a href="index.php?p=adminscraperjsonestrenostobd&pag='.($pag-1).'"><< Anterior</a>';
                    }
                    echo "</td><td style='width: 100px;'>".$pag." de ".$totalPelis."</td><td style='width: 100px;'>";
                    if($pag < $totalPelis) {
                        echo '<a href="index.php?p=adminscraperjsonestrenostobd&pag='.($pag+1).'">Siguiente >></a>';
                    }
                    echo "</td></tr></table></div>";
                    ?>
                    <table style=''>
                    <tr>
                        <th>Local</th>
                        <th>The Movie Database</th>
                        <th>FilmAffinity</th>
                    </tr>
                    <tr>
                        <td><input type='text' name='search_db' ng-model='search_db' id='nombreabuscar' placeholder="Buscar..." /><button type='button' id='search_db' ng-click='searchdb()'>Buscar</button></td>
                        <td><input type='text' name='search_tmdb' ng-model='search_tmdb' placeholder="Buscar..." value='<?php echo $titulo; ?>' /><button type='button' id='search_tmdb' ng-click='searchtmdb()'>Buscar</button></td>
                        <td><input type='text' name='search_fa' ng-model='search_fa' placeholder="Buscar..." value='<?php echo $titulo; ?>' /><button type='button' id='search_fa' ng-click='searchfa()'>Buscar</button></td>
                    </tr>
                    <tr>
                        <td>
                            {{search_db_result}}
                            <br><button type="button" ng-click="prevDB()">Anterior</button> <button type="button" ng-click="nextDB()">Siguiente</button>
                            <button type="submit" form="newpeliculadb">Guardar</button>
                        </td>
                        <td>
                            {{search_tmdb_result}}
                            <br><button type="button" ng-click="prevTMDB()">Anterior</button> <button type="button" ng-click="nextTMDB()">Siguiente</button> <button type="button" class="copyallbtntmdb">Copy All</button>
                        </td>
                        <td>
                            {{search_fa_result}}
                            <br><button type="button" ng-click="prevFA()">Anterior</button> <button type="button" ng-click="nextFA()">Siguiente</button>
                        </td>
                    </tr>
                    </table>
                    <table style=''>
                    <tr>
                    <?php
                    $cols = array("db","tmdb","fa");
                    foreach($cols as $k) {
                        if($k != "db") {
                            $disabled = "disabled='disabled'";
                        } else {
                            $disabled = "";
                        }
                    ?>
                        <td width="33%">
                            <input type="hidden" name="a" ng-value="action"/>
                            <input type="hidden" name="idpelicula" ng-value='idpelicula'/>
                            <div style='height: 30px;'>
                                <span ng-hide="<?php echo $k; ?>.ficha == null"><a id='<?php echo $k; ?>ficha' href="{{<?php echo $k; ?>.ficha}}" target="_blank">Ir a la ficha</a></span>
                            </div>
                            <div>
                                <label for="form-nombre">Nombre</label>
                                <input type="text" name="nombre" id="<?php echo $k; ?>form-nombre" <?php if($k == "db") echo "required='required'" ?> class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.nombre' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-original">Nombre original</label>
                                <input type="text" name="original" id="<?php echo $k; ?>form-original" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.original' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-fechaestreno">Fecha de estreno</label>
                                <input type="date" name="fechaestreno" id="<?php echo $k; ?>form-fechaestreno" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> <?php if($k == "db") echo "value='".(isset($estreno)?date("Y-m-d",strtotime($estreno)):"")."'" ?> />
                            </div>
                            <div>
                                <label for="form-director">Director/es</label>
                                <textarea name="director" id="<?php echo $k; ?>form-director" style='height: 60px;min-height: 60px;' class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.director'></textarea>
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-actores">Actores</label>
                                <textarea name="actores" id="<?php echo $k; ?>form-actores" style='height: 80px;min-height: 80px;' class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.actores'></textarea>
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-nacionalidad">Nacionalidad</label>
                                <input type="text" name="nacionalidad" id="<?php echo $k; ?>form-nacionalidad" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.nacionalidad' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-distribuidora">Distribuidora</label>
                                <input type="text" name="distribuidora" id="<?php echo $k; ?>form-distribuidora" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.distribuidora' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-edad">Edad</label>
                                <input type="number" name="edad" id="<?php echo $k; ?>form-edad" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.edad' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-duracion">Duración</label>
                                <input type="number" name="duracion" id="<?php echo $k; ?>form-duracion" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.duracion' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-genero">Género</label>
                                <input type="text" name="genero" id="<?php echo $k; ?>form-genero" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.genero' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-trailer">Trailer</label>
                                <input type="url" name="trailer" id="<?php echo $k; ?>form-trailer" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.trailer' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-web">Página web</label>
                                <input type="url" name="web" id="<?php echo $k; ?>form-web" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.web' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-sinopsis">Sinopsis</label>
                                <textarea name="sinopsis" id="<?php echo $k; ?>form-sinopsis" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.sinopsis'></textarea>
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <?php if($k != "tmdb") { ?>
                            <div>
                                <label for="form-fichafa">Ficha FilmAffinity</label>
                                <input type="url" name="fichafa" id="<?php echo $k; ?>form-fichafa" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.fichafa' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <?php } else echo "<br><br><br><br><br>"; ?>
                            <?php if($k != "fa") { ?>
                            <div>
                                <label for="form-fichaimdb">Ficha IMDB</label>
                                <input type="url" name="fichaimdb" id="<?php echo $k; ?>form-fichaimdb" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.fichaimdb' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <div>
                                <label for="form-fichatmdb">Ficha TMDB</label>
                                <input type="url" name="fichatmdb" id="<?php echo $k; ?>form-fichatmdb" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.fichatmdb' />
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                            </div>
                            <?php } else echo "<br><br><br><br><br><br><br><br><br>"; ?>
                            <div id='<?php echo $k; ?>poster'>
                                <label for="form-posterURL">Poster desde URL</label>
                                <input type="text" name="posterURL" id="<?php echo $k; ?>form-posterURL" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.poster'/>
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                                <br>
                                <p style='margin: auto; text-align:center;'><img src='{{<?php echo $k; ?>.poster2}}' alt='Poster de la película' style='max-width: 300px; max-height: 200px;'/></p>
                            </div>
                            <div id='<?php echo $k; ?>fanart'>
                                <label for="form-fanartURL">Fanart desde URL</label>
                                <input type="text" name="fanartURL" id="<?php echo $k; ?>form-fanartURL" class="campo <?php echo $k; ?>form" <?php echo $disabled; ?> ng-model='<?php echo $k; ?>.fanart'/>
                                <?php if($k != "db") { ?>
                                <button type="button" class='copybtn'>Copy</button>
                                <?php } ?>
                                <br>
                                <p style='margin: auto; text-align:center;'><img src='{{<?php echo $k; ?>.fanart2}}' alt='Fanart de la película' style='max-width: 300px; max-height: 200px;'/></p>
                            </div>
                            <?php if($k == "db") { ?>
                                <br><br>
                            <div class='divbig'>
                                <div style='display: inline-block;'>
                                    <label for="form-infantil">Infantil</label>
                                    <span class="checkbox">
                                        <input type="checkbox" id="form-infantil" name="infantil" ng-model='<?php echo $k; ?>.infantil' ng-true-value="1" />
                                        <label class="check" for="form-infantil"></label>
                                    </span>
                                </div><br><br>
                                <div style='display: inline-block;'>
                                    <label for="form-publicado">Publicado</label>
                                    <span class="checkbox">
                                        <input type="checkbox" id="form-publicado" name="publicado" ng-model='<?php echo $k; ?>.publicado' ng-true-value="1" />
                                        <label class="check" for="form-publicado"></label>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                        </td>
                    <?php } ?>
                    </tr>
                    </table>
                    <?php
                    echo "<div class='paginacion' style='margin: auto; text-align: center; display: block;'>
                            <table style='width: 300px; margin: auto;'>
                            <tr>
                                <td style='width: 100px;'>";
                    if($pag > 1) {
                        echo '<a href="index.php?p=adminscraperjsonestrenostobd&pag='.($pag-1).'"><< Anterior</a>';
                    }
                    echo "</td><td style='width: 100px;'>".$pag." de ".$totalPelis."</td><td style='width: 100px;'>";
                    if($pag < $totalPelis) {
                        echo '<a href="index.php?p=adminscraperjsonestrenostobd&pag='.($pag+1).'">Siguiente >></a>';
                    }
                    echo "</td></tr></table></div>";
                    ?>
                    <button type="submit" form="newpeliculadb">Guardar</button>
                </form>
            </section>
        </div>
    </section>
<script>
    /*cinesScraper*/ cines = <?php if(!empty($horasCineScraper)) echo $horasCineScraper; else echo "[]"; ?>;
    search_db = "<?php echo $titulo; ?>";
</script>