<?php
pageAccessControl();

$style = "info2";
if(isset($d["fanart"]) && !empty($d["fanart"])) {
    echo "<style>
        html {
            background: #eee url(".$d["fanart"].") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>";
	$style = "info";
}
?>
    <section id='content'>
        <div class="grid">
            <section id='<?php echo $style; ?>' itemscope itemtype="http://data-vocabulary.org/Event">
                <h2>
                    <?php
                    echo "<span itemprop='name'>".$d["nombre"]."</span><span itemprop='summary' style='display:none;'>".$d["nombre"]."</span> (".$d["original"].")";
                    if($d["infantil"] == 1) {
                        echo "<span class='h2-infantil'>Para niños</span>";
                    }
                    ?>
                    <span itemprop="eventType" style='display:none;'>Película</span>
                </h2>
                <div id="info-izq">
                    <?php
                    if(isset($d["reserva"]) && !empty($d["reserva"])) {
                        if(!empty($d["limitereserva"]) && $d["limitereserva"] > "2000-01-01") {
                            echo "<div id='reserva'>Reservar antes del ".date("d-m-Y H:i",strtotime($d["limitereserva"]))."</div>";
                        } else {
                            echo "<div id='reserva'>Reserva necesaria</div>";
                        }
                    }
                    $ampliar = "";
                    if(empty($d["poster"]) || !file_exists($d["poster"])) {
                        $d["poster"] = "img/interface/null-film.png";
                    } else {
                        $ampliar = "<div class='ampliar'>+</div>";
                    }
                    echo "<div id='poster'>";
                    echo $ampliar;
                    echo "  <img src='".$d["poster"]."' id='posterimg' itemprop='photo' alt='Poster de la película'/>";
                    echo "</div>";
                    if(!empty($d["trailer"])) {
                        $videoURL = explode("=",$d["trailer"]);
                        if(isset($videoURL[1]) && !empty($videoURL[1])) {
                            echo '<p id="video"><iframe id="ytplayer" type="text/html" width="340" height="240" src="http://www.youtube.com/embed/'.$videoURL[1].'?autoplay=0&origin=http://hazplanes.com" frameborder="0"></iframe></p>';
                        }
                    } else {
                        echo "<br>";
                    }
                    ?>
                    <h3>Información recogida de:</h3>
                    <div id='info-credits'>
                        <p style='padding: 0 10px 0'>
                            <a href='http://www.filmaffinity.com/es/' target="_blank">FilmAffinity</a><br>
                            <a href='http://www.themoviedb.com' target="_blank">The Movie Database</a>
                        </p>
                    </div>
                    <?php if(!empty($d["menciones"])) { ?>
                        <h3>Créditos / Agradecimientos</h3>
                        <div id='info-credits'>
                            <p style='padding: 0 10px 0;'>
                                <?php echo htmlspecialchars_decode ($d["menciones"]); ?>
                            </p>
                        </div>
                    <?php
                    }
                    echo $publiFicha;
                    ?>
                    <span id='last-update'>
                        Última actualización:
                        <?php
                        if(!empty($d["actualizado"])){
                            echo $d["actualizado"];
                        } else {
                            echo $d["creado"];
                        }
                        ?>
                    </span>
                </div>
                <div id="info-der">
                    <h3>Ficha de la película <span class="ocultar">Ocultar</span></h3>
                    <div id='info-ficha'>
                        <?php
                        if(!empty($d["director"])) {
                            echo "<div id='director'><h4>Director/es</h4>";
                            echo $d["director"]."</div>";
                        }
                        if(!empty($d["actores"])) {
                            echo "<div id='actores'><h4>Actores</h4>";
                            echo $d["actores"]."</div>";
                        }
                        if(!empty($d["nacionalidad"])) {
                            echo "<div id='nacionalidad'><h4>Nacionalidad</h4>";
                            echo $d["nacionalidad"]."</div>";
                        }
                        if(!empty($d["distribuidora"])) {
                            echo "<div id='distribuidora'><h4>Distribuidora</h4>";
                            echo $d["distribuidora"]."</div>";
                        }
                        if(!empty($d["edad"])) {
                            echo "<div id='edad'><h4>Edad mínima</h4>";
                            echo $d["edad"]." años.</div>";
                        }
                        if(!empty($d["duracion"])) {
                            echo "<div id='duracion'><h4>Duración</h4>";
                            echo "<span itemprop='duration' content='PT".(floor($d["duracion"] / 60))."H".($d["duracion"] % 60)."M'>".calculaDuracion($d["duracion"])." (".$d["duracion"]." min)</span></div>";
                        }
                        if(!empty($d["genero"])) {
                            echo "<div id='genero'><h4>Género</h4>";
                            echo $d["genero"]."</div>";
                        }
                        if(!empty($d["web"])) {
                            echo "<div id='web'><h4>Página web</h4> <a href='".$d["web"]."' target='_blank'>".$d["web"]."</a></div>";
                        }
                        $estreno = date("d-m-Y",strtotime($d["fechaestreno"]));
                        if(!empty($d["fechaestreno"]) && $estreno != "30-11--0001" && $estreno != "01-01-1970") {
                            echo "<div id='fechaestreno'><h4>Fecha de estreno</h4>";
                            echo $estreno."</div>";
                        }
                        ?>
                        <div class='share-links'>
                            <!-- Place this tag where you want the share button to render. -->
                            <div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php echo $ogurl; ?>"></div>
                            <div class="fb-share-button" data-href="<?php echo $ogurl; ?>" data-type="button_count"></div>
                            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $ogurl; ?>"  data-via="HazPlanes" data-lang="es">Twittear</a>
                        </div>
                        <?php
                        if(!empty($d["sinopsis"])) {
                            echo "<h3>Sinopsis</h3><div id='sinopsis'>";
                            echo "<p itemprop='description'>".htmlspecialchars_decode($d["sinopsis"])."</p></div>";
                        }
                        if(!empty($d["notas"])) {
                            echo "<h3>Notas</h3><div id='notas'>";
                            echo "<p>".htmlspecialchars_decode($d["notas"])."</p>";
                            echo "</div>";
                        }
                        if(!empty($d["fichafa"]) || !empty($d["fichaimdb"]) || !empty($d["fichatmdb"]) ) {
                            echo "<h3>Ficha externa</h3><div id='sinopsis'><p>";
                            if(!empty($d["fichafa"])) {
                                echo "<a href='".$d["fichafa"]."' target='_blank'>Ficha en \"FilmAffinity\"</a><br>";
                            }
                            if(!empty($d["fichaimdb"])) {
                                echo "<a href='".$d["fichaimdb"]."' target='_blank'>Ficha en \"IMDB\"</a><br>";
                            }
                            if(!empty($d["fichatmdb"])) {
                                echo "<a href='".$d["fichatmdb"]."' target='_blank'>Ficha en \"The Movie Database\"</a><br>";
                            }
                            echo "</p></div>";
                        }
                        ?>
                    </div>
                    <?php
                    infoHoras($d);
                    infoMapLugares($d);
                    ?>
                    <div style='padding: 5px; font-size: 12px;'>
                        * Los precios no tienen en cuenta promociones y ofertas, salvo el día del espectador.
                    </div>
                </div>
            </section>
        </div>
    </section>
