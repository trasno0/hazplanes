<?php
/*
 * Scraper de yelmo cines
 * Modificado el 27-10-2014
 */

include_once("../includes/api.inc.php");
include_once("../includes/scrapers.inc.php");

$opts = array('http'=>array('header' => "User-Agent:Mozilla/5.0\r\n"));
$context = stream_context_create($opts);

date_default_timezone_set("Europe/Madrid");

$sleep = "2";
$local = file_get_contents("../temp/results.json");
$json_total = json_decode($local, true);

//Yelmo Cines
echo "<h1>Yelmo Cines</h1>";
$url = "http://www.yelmocines.es/billboard/33/";
$output = file_get_contents($url,false,$context);

preg_match_all('/billboard\/province\/nojs\/33\/(.*?)\/0\"/s', $output, $fechas);

if(!empty($json_total)) {
    foreach($json_total as $k => $json_tipo) {
        foreach($json_tipo as $k2 => $json_evento) {
            if(isset($json_evento["Yelmo Cines"])) {
                unset($json_total[$k][$k2]["Yelmo Cines"]);
            }
        }
    }
}

$count = array(0,0);
$tot = count($fechas[1]);
//echo "<pre>";
//print_r($pelis);
foreach($fechas[1] as $fecha) {
    $url = "http://www.yelmocines.es/billboard/33/".$fecha;
    //$url = "yelmo.html";
    $output = file_get_contents($url,false,$context);

    preg_match_all('/class=\"full\"(.*?)<\/div>\s*<\/td>\s*<\/tr>/s', $output, $pelis);

    foreach($pelis[1] as $peli) {
        $count[0]++;
        preg_match_all('/mv-title\">.*? title=\"(.*?)\">/s', $peli, $t);
        preg_match_all('/li class=\"([23][Dd]?) tab-value(.*?)<\/li><\/ul><\/li>/s', $peli, $ddd);

        $titulo = ucwords($t[1][0]);
        $tipo = getEventType($titulo);
		$titulo = normalize_title($titulo);

		foreach($ddd[2] as $k => $dd) {
            if(strtoupper($ddd[1][$k]) == "3D") {
                $d3 = 1;
            } else {
                $d3 = 0;
            }
            preg_match_all('/ticketurl=(.*?)\" title=\"tu entrada.*?this\.href]\);\">(.*?)<\/a><\/li>/s', $dd, $h);

			foreach($h[1] as $k2 => $url) {
                $url = $url;
                $u = explode("%3D", $url);
                $sala = $u[count($u)-1];
                $hora = $h[2][$k2];

                $existe = false;
                if(isset($json_total[$tipo][$titulo]["Yelmo Cines"][strtotime($fecha)])) {
                    foreach($json_total[$tipo][$titulo]["Yelmo Cines"][strtotime($fecha)] as $horaexistente) {
                        if($horaexistente["hora"] == $hora) {
                            $existe = true;
                            break;
                        }
                    }
                }
                if(!$existe) {
                    $count[1]++;
                    $json_total[$tipo][$titulo]["Yelmo Cines"][strtotime($fecha)][] = array(
                            "sala" => $sala,
                            "hora" => $hora,
                            "url" => urldecode($url),
                            "3D" => $d3,
                            "vo" => 0
                        );
                }
            }
        }
    }
    sleep($sleep);
}

foreach($json_total as $k => $json_tipo) {
    foreach($json_tipo as $k2 => $json_evento) {
        if(empty($json_evento)) {
            unset($json_total[$k][$k2]);
        }
    }
}

$json_total["yelmo"]["last_update"] = date("Y-m-d H:i", strtotime("now"));
$json_total["yelmo"]["events"] = $count;

$fp = fopen('../temp/results.json', 'w');
fwrite($fp, json_encode($json_total));
fclose($fp);

echo "Encontrados ".$count[0]." eventos. Añadidas ".$count[1]." sesiones.";
?>
<h1>Terminado!</h1>