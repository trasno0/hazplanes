<?php
pageAccessControl(1);

$filename = "modules/maintenancemodeBD.php";
if(isset($_POST["action"]) && $_POST["action"] == "mantenimiento") {
    if(is_readable($filename)) {
        $mode = file($filename);
        if($mode[0] == 1) {
            if (is_writable($filename)) {
                if (!$handle = fopen($filename, 'w')) {
                    $error = "Cannot open file ($filename)";
                }
                if (fwrite($handle, 0) === FALSE) {
                    $error = "Cannot write to file ($filename)";
                }
                fclose($handle);
            } else {
                $error = "The file $filename is not writable";
            }
        } elseif($mode[0] == 0) {
            if (is_writable($filename)) {
                if (!$handle = fopen($filename, 'w')) {
                    $error = "Cannot open file ($filename)";
                }
                if (fwrite($handle, 1) === FALSE) {
                    $error = "Cannot write to file ($filename)";
                }
                fclose($handle);
            } else {
                $error = "The file $filename is not writable";
            }
        }
    } else {
        echo "No se puede leer";
    }
}

if(file_exists($filename) && is_readable($filename)) {
    $checked = file_get_contents($filename);
} else {
    $checked = 0;
}
if($checked) {
    $checked = "checked";
} else {
    $checked = "";
}
?>
    <section id='content'>
        <div class="grid">
            <section id='datos'>
                <div>
                    <h2>Modo mantenimiento</h2>
                    <?php
                    if(isset($error)) {
                        echo "<div class='errormsg'>".$error."</div>";
                    }
                    ?>
                    <form class="form" style='margin: 20px 0;' id="maintenance" name="maintenance" action="index.php?p=admindashboard" method="POST">
                        <input type="hidden" name="action" value="mantenimiento" />
                        <div>
                            <label for="form-mantenimiento">Página en mantenimiento</label>
                            <span class="checkbox">
                                <input type="checkbox" id="form-mantenimiento" name="mantenimiento" <?php echo $checked; ?> />
                                <label class="check" for="form-mantenimiento"></label>
                            </span>
                        </div>
                        <br>
                        <button type="submit" name="boton" id="maintenancebtn">Activar</button>
                    </form>
                </div>

                <h2><?php echo isset($_POST["date"])?$_POST["date"]:"Hoy"; ?></h2>
                <form action="#" method="post">
                	<input type="date" name="date" id="date" value="<?php echo isset($_POST["date"])?$_POST["date"]:date("Y-m-d", strtotime("now")); ?>"/>
                	<button type="submit">Enviar</button> https://hazplanes.com/main/<?php echo isset($_POST["date"])?date("d-m-Y", strtotime($_POST["date"])):date("d-m-Y", strtotime("now")); ?>
                </form>
                <table id="list" class="tabla" style='margin: 0px;'>
                    <thead>
                        <th width='120'>Hora</th>
                        <th>Nombre</th>
                        <th>Genero</th>
                        <th>Tipo</th>
                        <th>Enlace</th>
                    </thead>
                    <?php
                    if(isset($_POST["date"])) {
                    	$date = $_POST["date"];
                        $now = date("Y-m-d 00:00:00", strtotime($date));
                        $fin = date("Y-m-d 23:59:59", strtotime($date));
					} else {
                    	$now = date("Y-m-d 00:00:00", strtotime("now"));
                    	$fin = date("Y-m-d 00:00:00", strtotime("+1 day"));
					}
                    $options = array(
                        //"filter" => "(fecha between '".$now."' and '".$fin."') and publicado = 1",
                        "filter" => "(((fecha between '".$now."' and '".$fin."') or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
                        "agenda" => 1,
                        "hoy" => 1
                        );
                    $general = new GeneralController($options,$db);
                    $data = $general->readAction();
                    $temp = array();
                    foreach($data as $k => $d) {
                        if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $d["tipoevento"] == "exposiciones" || $d["tipoevento"] == "peliculas")) {
                            array_push($temp, $d);
                            unset($data[$k]);
                        }
                    }
                    $data = array_merge($data, $temp);
                    $ids = array();
                    foreach($data as $k => $d) {
                        if(in_array($d["nombre"],$ids)) {
                            continue;
                        }
                        $ids[] = $d["nombre"];
                        switch($d["tipoevento"]) {
                            case "peliculas":
                                $type = "PELICULA";
                                $link = "pelicula/";
                                $adminlink = "adminformfilm";
                            break;
                            case "conciertos":
                                $type = "CONCIERTO";
                                $link = "concierto/";
                                $adminlink = "adminformconcert";
                            break;
                            case "obrasteatro":
                                $type = "TEATRO";
                                $link = "obra/";
                                $adminlink = "adminformobra";
                            break;
                            case "eventos":
                                $type = "EVENTO";
                                $link = "evento/";
                                $adminlink = "adminformevent";
                            break;
                            case "exposiciones":
                                $type = "EXPOSICIÓN";
                                $link = "exposicion/";
                                $adminlink = "adminformexpo";
                            break;
                            case "deportes":
                                $type = "DEPORTE";
                                $link = "competicion/";
                                $adminlink = "adminformdeporte";
                            break;
                            case "formacion":
                                $type = "FORMACIÓN";
                                $link = "curso/";
                                $adminlink = "adminformcurso";
                            break;
                        }
                        $enlace = BASE_URL.$link.$d["id"]."-".urlAmigable($d["nombre"]);
                        echo "<tr class='row'>
                            <td width='120'>".$d["fecha"]."</td>
                            <td><a href='index.php?p=".$adminlink."&i=".$d["id"]."'>".$d["nombre"]."</a></td>
                            <td>".$d["genero"]."</td>
                            <td>".$type."</td>
                            <td><a href='".$enlace."'>".$enlace."</a></td>
                        </tr>";
                    }
                    ?>
                </table>
            </section>
        </div>
    </section>
