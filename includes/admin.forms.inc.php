<?php

function horariosToJson($d) {
    global $db;

    //Horarios
    $horarios = "";
    $tipohorarios = array("horasCine", "horasLocal", "horasAuditorio", "horasLugar", "horasMuseo", "horasPabellon");
    foreach($tipohorarios as $th) {
        if(isset($d[$th]) && !empty($d[$th])) {
            switch($th) {
                case "horasCine":
                    $idtipo = "idcine";
                    break;
                case "horasLocal":
                    $idtipo = "idlocal";
                    break;
                case "horasAuditorio":
                    $idtipo = "idteatro";
                    break;
                case "horasMuseo":
                    $idtipo = "idmuseo";
                    break;
                case "horasLugar":
                    $idtipo = "fecha";
                    break;
                case "horasPabellon":
                    $idtipo = "idpabellon";
                    break;
            }
            $ho = array();
            $id = 0;
            $pos = -1;
            foreach($d[$th] as $h) {
                if($th == "horasCine") {
                    $salas = new Salas(array("idcine"=>$h["idcine"]),$db);
                    $s = $salas->readAction();
                } else {
                    $s = "";
                    $h["idsala"] = "";
                }
                if($th == "horasCine") {
                    if($id != $h[$idtipo]) {
                        $ho[] = array($idtipo => $h[$idtipo], "child" => array(array("idsala" => $h["idsala"], "salas" => $s, "horas" => array("fecha" => $h["fecha"], "precio" => $h["precio"], "urlcompra" => $h["urlcompra"], "3d" => (int)$h["3d"], "vo" => (int)$h["vo"], "agotado" => (int)$h["agotado"], "cancelado" => (int)$h["cancelado"], "tododia" => (int)$h["todoeldia"]))));
                        $pos++;
                    } else {
                        array_push($ho[$pos]["child"],array("idsala" => $h["idsala"], "salas" => $s, "horas" => array("fecha" => $h["fecha"], "precio" => $h["precio"], "urlcompra" => $h["urlcompra"], "3d" => (int)$h["3d"], "vo" => (int)$h["vo"], "agotado" => (int)$h["agotado"], "cancelado" => (int)$h["cancelado"], "tododia" => (int)$h["todoeldia"])));
                    }
                } elseif($th == "horasLocal" || $th == "horasAuditorio" || $th == "horasMuseo" || $th == "horasPabellon") {
                    if($id != $h[$idtipo]) {
                        $ho[] = array($idtipo => $h[$idtipo], "child" => array(array("horas" => array("fecha" => $h["fecha"], "precio" => $h["precio"], "precioanticipada" => $h["precioanticipada"], "urlcompra" => $h["urlcompra"], "vo" => (isset($h["vo"]) && !empty($h["vo"])?1:0), "agotado" => (int)$h["agotado"], "cancelado" => (int)$h["cancelado"], "tododia" => (int)$h["todoeldia"]))));
                        $pos++;
                    } else {
                        array_push($ho[$pos]["child"],array("horas" => array("fecha" => $h["fecha"], "precio" => $h["precio"], "precioanticipada" => $h["precioanticipada"], "urlcompra" => $h["urlcompra"], "agotado" => (int)$h["agotado"], "cancelado" => (int)$h["cancelado"], "tododia" => (int)$h["todoeldia"])));
                    }
                } elseif($th == "horasLugar") {
                    if(!isset($h["duracion"])) {
                        $h["duracion"] = 0;
                    }
                    $ho[] = array("fecha" => $h["fecha"], "duracion" => (int)$h["duracion"], "precio" => $h["precio"], "precioanticipada" => $h["precioanticipada"], "lugar" => $h["lugar"], "localidad" => $h["localidad"], "coordenadas" => $h["coordenadas"], "agotado" => (int)$h["agotado"], "cancelado" => (int)$h["cancelado"], "tododia" => (int)$h["todoeldia"]);
                } elseif($th != "horasLugar") {
                    if($id != $h[$idtipo]) {
                        $ho[] = array($idtipo => $h[$idtipo], "child" => array(array("horas" => array("fecha" => $h["fecha"], "precio" => $h["precio"], "precioanticipada" => $h["precioanticipada"], "agotado" => (int)$h["agotado"], "cancelado" => (int)$h["cancelado"], "tododia" => (int)$h["todoeldia"], "coordenadas" => $h["coordenadas"]))));
                        $pos++;
                    } else {
                        array_push($ho[$pos]["child"],array("horas" => array("fecha" => $h["fecha"], "precio" => $h["precio"], "precioanticipada" => $h["precioanticipada"], "agotado" => (int)$h["agotado"], "cancelado" => (int)$h["cancelado"], "tododia" => (int)$h["todoeldia"], "coordenadas" => $h["coordenadas"])));
                    }
                }
                $id = $h[$idtipo];
            }
            $horarios[$th] = json_encode($ho);
        }
    }
    return $horarios;
}

function formHorariosPabellones($formorigen = "all") {
    global $db;
    $options = array("filter" => "publicado=1");
    $pabellones = new Pabellones($options,$db);
    $allPabellones = $pabellones->readAction();
    ?>
    <table class='tabla-fechas'>
        <caption style='text-align: left'>Pabellones <button type="button" ng-click="addPabellon()">Añadir pabellon</button></caption>
        <tr style='border: 1px solid white;' ng-repeat="pabellon in pabellones">
            <td>
                <table>
                <tr>
                    <th colspan='5' style='text-align: left;'>
                        <select name="pabellones[{{$index}}][][idpabellon]" class="campo" required="required" ng-model="pabellon.idpabellon">
                            <?php
                            foreach($allPabellones as $k => $pabellon) {
                                echo "<option value='".$pabellon["idpabellon"]."'>".$pabellon["nombre"]."</option>";
                            }
                            ?>
                        </select>
                    </th>
                    <th>
                        <button type="button" ng-click="addHoraPabellon(pabellon)">Añadir fecha</button>
                    </th>
                    <th>
                        <button type="button" class="campo small" ng-click="removePabellon(pabellon)">&times;</button>
                    </th>
                </tr>
                <tr>
                    <?php if($formorigen == "exposicion") { ?>
                    <th class="col-fecha">Inicio</th>
                    <th class="col-fecha">Fin</th>
                    <th class="col-checkbox">Permanente</th>
                    <?php } else { ?>
                    <th class="col-fecha">Fecha</th>
                    <th class="col-entradas">Entradas</th>
                    <th class="col-precioanticipada">Precio anticipada</th>
                    <?php } ?>
                    <th class="col-precio">Precio</th>
                    <?php if($formorigen == "pelicula") { ?>
                        <th class="col-checkbox">3D</th>
                        <th class="col-checkbox">V.O.S.E</th>
                    <?php } ?>
                    <th class="col-checkbox">Agotado</th>
                    <th class="col-checkbox">Cancelado</th>
                    <?php if($formorigen != "exposicion") { ?>
                    <th class="col-checkbox">Todo el día</th>
                    <?php } ?>
                    <th class="col-remove"></th>
                </tr>
                <tr ng-repeat="hora in pabellon.child">
                    <?php if($formorigen == "exposicion") { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="pabellones[{{$parent.$index}}][{{$index}}][inicio]" class="campo field-fecha" ng-model="hora.horas.inicio" />
                    </td>
                    <td class="col-fecha">
                        <input type="datetime" name="pabellones[{{$parent.$index}}][{{$index}}][fin]" class="campo field-fecha" ng-model="hora.horas.fin" />
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-permanente{{$parent.$index}}-{{$index}}" name="pabellones[{{$parent.$index}}][{{$index}}][permanente]" ng-model="hora.permanente" ng-checked="hora.horas.permanente" />
                            <label class="check" for="form-permanente{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } else { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="pabellones[{{$parent.$index}}][{{$index}}][fecha]" class="campo field-fecha" ng-model="hora.horas.fecha" />
                    </td>
                    <td class="col-entradas">
                        <input type="text" name="pabellones[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-precioanticipada">
                        <input type="text" name="pabellones[{{$parent.$index}}][{{$index}}][precioanticipada]" class="campo field-precioanticipada" ng-model="hora.horas.precioanticipada" />
                    </td>
                    <?php } ?>
                    <td class="col-precio">
                        <input type="text" name="pabellones[{{$parent.$index}}][{{$index}}][precio]" class="campo field-precio" ng-model="hora.horas.precio" />
                    </td>
                    <?php if($formorigen == "pelicula") { ?>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-3dpabellon{{$parent.$index}}-{{$index}}" name="pabellones[{{$parent.$index}}][{{$index}}][3d]" ng-model="hora.3d" ng-checked="hora.horas.3d" />
                                <label class="check" for="form-3dpabellon{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-vopabellon{{$parent.$index}}-{{$index}}" name="pabellones[{{$parent.$index}}][{{$index}}][vo]" ng-model="hora.vo" ng-checked="hora.horas.vo" />
                                <label class="check" for="form-vopabellon{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                    <?php } ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-agotadopabellon{{$parent.$index}}-{{$index}}" name="pabellones[{{$parent.$index}}][{{$index}}][agotado]" ng-model="hora.agotado" ng-checked="hora.horas.agotado" />
                            <label class="check" for="form-agotadopabellon{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-canceladopabellon{{$parent.$index}}-{{$index}}" name="pabellones[{{$parent.$index}}][{{$index}}][cancelado]" ng-model="hora.cancelado" ng-checked="hora.horas.cancelado" />
                            <label class="check" for="form-canceladopabellon{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php if($formorigen != "exposicion") { ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-tododiapabellon{{$parent.$index}}-{{$index}}" name="pabellones[{{$parent.$index}}][{{$index}}][tododia]" ng-model="hora.tododia" ng-checked="hora.horas.tododia" />
                            <label class="check" for="form-tododiapabellon{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } ?>
                    <td class="col-remove">
                        <button type="button" class="campo small" ng-click="removeHoraPabellon(pabellon,hora)">&times;</button>
                    </td>
                </tr>
                </table>
                <hr style="height:1px;border:none;background-color:white;" />
            </td>
        </tr>
    </table>
    <?php
}

function formHorariosMuseos($formorigen = "all") {
    global $db;
    $options = array("filter" => "publicado=1");
    $museo = new Museos($options,$db);
    $allMuseos = $museo->readAction();
    ?>
    <table class='tabla-fechas'>
        <caption style='text-align: left'>Museos <button type="button" ng-click="addMuseo()">Añadir museo</button></caption>
        <tr style='border: 1px solid white;' ng-repeat="museo in museos">
            <td>
                <table>
                <tr>
                    <th colspan='5' style='text-align: left;'>
                        <select name="museos[{{$index}}][][idmuseo]" class="campo" required="required" ng-model="museo.idmuseo">
                            <?php
                            foreach($allMuseos as $k => $museo) {
                                echo "<option value='".$museo["idmuseo"]."'>".$museo["nombre"]."</option>";
                            }
                            ?>
                        </select>
                    </th>
                    <th>
                        <button type="button" ng-click="addHoraMuseo(museo)">Añadir fecha</button>
                    </th>
                    <th>
                        <button type="button" class="campo small" ng-click="removeMuseo(museo)">&times;</button>
                    </th>
                </tr>
                <tr>
                    <?php if($formorigen == "exposicion") { ?>
                    <th class="col-fecha">Inicio</th>
                    <th class="col-fecha">Fin</th>
                    <th class="col-checkbox">Permanente</th>
                    <?php } else { ?>
                    <th class="col-fecha">Fecha</th>
                    <th class="col-entradas">Entradas</th>
                    <th class="col-precioanticipada">Precio anticipada</th>
                    <?php } ?>
                    <th class="col-precio">Precio</th>
                    <?php if($formorigen == "pelicula") { ?>
                        <th class="col-checkbox">3D</th>
                        <th class="col-checkbox">V.O.S.E</th>
                    <?php } ?>
                    <th class="col-checkbox">Agotado</th>
                    <th class="col-checkbox">Cancelado</th>
                    <?php if($formorigen != "exposicion") { ?>
                    <th class="col-checkbox">Todo el día</th>
                    <?php } ?>
                    <th class="col-remove"></th>
                </tr>
                <tr ng-repeat="hora in museo.child">
                    <?php if($formorigen == "exposicion") { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="museos[{{$parent.$index}}][{{$index}}][inicio]" class="campo field-fecha" ng-model="hora.horas.inicio" />
                    </td>
                    <td class="col-fecha">
                        <input type="datetime" name="museos[{{$parent.$index}}][{{$index}}][fin]" class="campo field-fecha" ng-model="hora.horas.fin" />
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-permanente{{$parent.$index}}-{{$index}}" name="museos[{{$parent.$index}}][{{$index}}][permanente]" ng-model="hora.permanente" ng-checked="hora.horas.permanente" />
                            <label class="check" for="form-permanente{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } else { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="museos[{{$parent.$index}}][{{$index}}][fecha]" class="campo field-fecha" ng-model="hora.horas.fecha" />
                    </td>
                    <td class="col-entradas">
                        <input type="text" name="museos[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-precioanticipada">
                        <input type="text" name="museos[{{$parent.$index}}][{{$index}}][precioanticipada]" class="campo field-precioanticipada" ng-model="hora.horas.precioanticipada" />
                    </td>
                    <?php } ?>
                    <td class="col-precio">
                        <input type="text" name="museos[{{$parent.$index}}][{{$index}}][precio]" class="campo field-precio" ng-model="hora.horas.precio" />
                    </td>
                    <?php if($formorigen == "pelicula") { ?>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-3dmuseo{{$parent.$index}}-{{$index}}" name="museos[{{$parent.$index}}][{{$index}}][3d]" ng-model="hora.3d" ng-checked="hora.horas.3d" />
                                <label class="check" for="form-3dmuseo{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-vomuseo{{$parent.$index}}-{{$index}}" name="museos[{{$parent.$index}}][{{$index}}][vo]" ng-model="hora.vo" ng-checked="hora.horas.vo" />
                                <label class="check" for="form-vomuseo{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                    <?php } ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-agotadomuseo{{$parent.$index}}-{{$index}}" name="museos[{{$parent.$index}}][{{$index}}][agotado]" ng-model="hora.agotado" ng-checked="hora.horas.agotado" />
                            <label class="check" for="form-agotadomuseo{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-canceladomuseo{{$parent.$index}}-{{$index}}" name="museos[{{$parent.$index}}][{{$index}}][cancelado]" ng-model="hora.cancelado" ng-checked="hora.horas.cancelado" />
                            <label class="check" for="form-canceladomuseo{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php if($formorigen != "exposicion") { ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-tododiamuseo{{$parent.$index}}-{{$index}}" name="museos[{{$parent.$index}}][{{$index}}][tododia]" ng-model="hora.tododia" ng-checked="hora.horas.tododia" />
                            <label class="check" for="form-tododiamuseo{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } ?>
                    <td class="col-remove">
                        <button type="button" class="campo small" ng-click="removeHoraMuseo(museo,hora)">&times;</button>
                    </td>
                </tr>
                </table>
                <hr style="height:1px;border:none;background-color:white;" />
            </td>
        </tr>
    </table>
    <?php
}

function formHorariosCines($formorigen = "all") {
    global $db;
    $options = array("filter" => "publicado=1");
    $cine = new Cines($options,$db);
    $allCines = $cine->readAction();
    ?>
    <table class='tabla-fechas'>
        <caption style='text-align: left'>Cines <button type="button" ng-click="addCine()">Añadir cine</button></caption>
        <tr style='border: 1px solid white;' ng-repeat="cine in cines">
            <td>
                <table>
                <tr>
                    <th colspan='6' style='text-align: left;'>
                        <select name="cines[{{$index}}][][idcine]" class="campo" required="required" ng-model="cine.idcine">
                            <?php
                            foreach($allCines as $k => $cine) {
                                echo "<option value='".$cine["idcine"]."'>".$cine["nombre"]."</option>";
                            }
                            ?>
                        </select>
                    </th>
                    <th>
                        <button type="button" ng-click="addHoraCine(cine)">Añadir fecha</button>
                    </th>
                    <th>
                        <button type="button" class="campo small" ng-click="removeCine(cine)">&times;</button>
                    </th>
                </tr>
                <tr>
                    <th class="col-sala">Sala</th>
                    <th class="col-fecha">Fecha</th>
                    <th class="col-entradas">Entradas</th>
                    <th class="col-precio">Precio</th>
                    <th class="col-checkbox">3D</th>
                    <th class="col-checkbox">V.O.S.E</th>
                    <th class="col-checkbox">Agotado</th>
                    <th class="col-checkbox">Cancelado</th>
                    <th class="col-checkbox">Todo el día</th>
                    <th class="col-remove"></th>
                </tr>
                <tr ng-repeat="hora in cine.child">
                    <td class="col-sala">
                        <select class="campo field-sala" required="required" ng-model="hora.idsala" ng-options="o.idsala as o.numero for o in hora.salas"></select>
                        <input type="hidden" name="cines[{{$parent.$index}}][{{$index}}][idsala]" value="{{hora.idsala}}"/>
                    </td>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="cines[{{$parent.$index}}][{{$index}}][fecha]" class="campo field-fecha" ng-model="hora.horas.fecha" />
                    </td>
                    <td class="col-entradas">
                        <input type="text" name="cines[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-precio">
                        <input type="text" name="cines[{{$parent.$index}}][{{$index}}][precio]" class="campo field-precio" ng-model="hora.horas.precio" />
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-3dcine{{$parent.$index}}-{{$index}}" name="cines[{{$parent.$index}}][{{$index}}][3d]" ng-model="hora.3d" ng-checked="hora.horas.3d" />
                            <label class="check" for="form-3dcine{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-vocine{{$parent.$index}}-{{$index}}" name="cines[{{$parent.$index}}][{{$index}}][vo]" ng-model="hora.vo" ng-checked="hora.horas.vo" />
                            <label class="check" for="form-vocine{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-agotadocine{{$parent.$index}}-{{$index}}" name="cines[{{$parent.$index}}][{{$index}}][agotado]" ng-model="hora.agotado" ng-checked="hora.horas.agotado" />
                            <label class="check" for="form-agotadocine{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-canceladocine{{$parent.$index}}-{{$index}}" name="cines[{{$parent.$index}}][{{$index}}][cancelado]" ng-model="hora.cancelado" ng-checked="hora.horas.cancelado" />
                            <label class="check" for="form-canceladocine{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php if($formorigen != "exposicion") { ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-tododiacine{{$parent.$index}}-{{$index}}" name="cines[{{$parent.$index}}][{{$index}}][tododia]" ng-model="hora.tododia" ng-checked="hora.horas.tododia" />
                            <label class="check" for="form-tododiacine{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } ?>
                    <td class="col-remove">
                        <button type="button" class="campo small" ng-click="removeHoraCine(cine,hora)">&times;</button>
                    </td>
                </tr>
                </table>
                <hr style="height:1px;border:none;background-color:white;" />
            </td>
        </tr>
    </table>
    <?php
}

function formHorariosLocales($formorigen = "all") {
    global $db;
    $options = array("filter" => "publicado=1");
    $local = new Locales($options,$db);
    $allLocales = $local->readAction();
    ?>
    <table class='tabla-fechas'>
        <caption style='text-align: left'>Locales <button type="button" ng-click="addLocal()">Añadir local</button></caption>
        <tr style='border: 1px solid white;' ng-repeat="local in locales">
            <td>
                <table>
                <tr>
                    <th colspan='6' style='text-align: left;'>
                        <select name="locales[{{$index}}][][idlocal]" class="campo" required="required" ng-model="local.idlocal">
                            <?php
                            foreach($allLocales as $k => $local) {
                                echo "<option value='".$local["idlocal"]."'>".$local["nombre"]."</option>";
                            }
                            ?>
                        </select>
                    </th>
                    <th>
                        <button type="button" ng-click="addHoraLocal(local)">Añadir fecha</button>
                    </th>
                    <th>
                        <button type="button" class="campo small" ng-click="removeLocal(local)">&times;</button>
                    </th>
                </tr>
                <tr>
                    <?php if($formorigen == "exposicion") { ?>
                    <th class="col-fecha">Inicio</th>
                    <th class="col-fecha">Fin</th>
                    <th class="col-checkbox">Permanente</th>
                    <?php } else { ?>
                    <th class="col-fecha">Fecha</th>
                    <th class="col-entradas">Entradas</th>
                    <th class="col-precioanticipada">Precio anticipada</th>
                    <?php } ?>
                    <th class="col-precio">Precio</th>
                    <?php if($formorigen == "pelicula") { ?>
                        <th class="col-checkbox">3D</th>
                        <th class="col-checkbox">V.O.S.E</th>
                    <?php } ?>
                    <th class="col-checkbox">Agotado</th>
                    <th class="col-checkbox">Cancelado</th>
                    <?php if($formorigen != "exposicion") { ?>
                    <th class="col-checkbox">Todo el día</th>
                    <!--<th class="col-entradas">Poster</th>
                    <th class="col-entradas">Web</th>
                    <th class="col-entradas">Notas</th>
                    <th class="col-checkbox">Reservar</th>
                    <th class="col-fecha">Limite reserva</th>-->
                    <?php } ?>
                    <th class="col-remove"></th>
                </tr>
                <tr ng-repeat="hora in local.child">
                    <?php if($formorigen == "exposicion") { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="locales[{{$parent.$index}}][{{$index}}][inicio]" class="campo field-fecha" ng-model="hora.horas.inicio" />
                    </td>
                    <td class="col-fecha">
                        <input type="datetime" name="locales[{{$parent.$index}}][{{$index}}][fin]" class="campo field-fecha" ng-model="hora.horas.fin" />
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-permanente{{$parent.$index}}-{{$index}}" name="locales[{{$parent.$index}}][{{$index}}][permanente]" ng-model="hora.permanente" ng-checked="hora.horas.permanente" />
                            <label class="check" for="form-permanente{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } else { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="locales[{{$parent.$index}}][{{$index}}][fecha]" class="campo field-fecha" ng-model="hora.horas.fecha" />
                    </td>
                    <td class="col-entradas">
                        <input type="text" name="locales[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-precioanticipada">
                        <input type="text" name="locales[{{$parent.$index}}][{{$index}}][precioanticipada]" class="campo field-precioanticipada" ng-model="hora.horas.precioanticipada" />
                    </td>
                    <?php } ?>
                    <td class="col-precio">
                        <input type="text" name="locales[{{$parent.$index}}][{{$index}}][precio]" class="campo field-precio" ng-model="hora.horas.precio" />
                    </td>
                    <?php if($formorigen == "pelicula") { ?>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-3dlocal{{$parent.$index}}-{{$index}}" name="locales[{{$parent.$index}}][{{$index}}][3d]" ng-model="hora.3d" ng-checked="hora.horas.3d" />
                                <label class="check" for="form-3dlocal{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-volocal{{$parent.$index}}-{{$index}}" name="locales[{{$parent.$index}}][{{$index}}][vo]" ng-model="hora.vo" ng-checked="hora.horas.vo" />
                                <label class="check" for="form-volocal{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                    <?php } ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-agotadolocal{{$parent.$index}}-{{$index}}" name="locales[{{$parent.$index}}][{{$index}}][agotado]" ng-model="hora.agotado" ng-checked="hora.horas.agotado" />
                            <label class="check" for="form-agotadolocal{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-canceladolocal{{$parent.$index}}-{{$index}}" name="locales[{{$parent.$index}}][{{$index}}][cancelado]" ng-model="hora.cancelado" ng-checked="hora.horas.cancelado" />
                            <label class="check" for="form-canceladolocal{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php if($formorigen != "exposicion") { ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-tododialocal{{$parent.$index}}-{{$index}}" name="locales[{{$parent.$index}}][{{$index}}][tododia]" ng-model="hora.tododia" ng-checked="hora.horas.tododia" />
                            <label class="check" for="form-tododialocal{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <!--<td class="col-entradas">
                        <input type="text" name="locales[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-entradas">
                        <input type="text" name="locales[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-entradas">
                        <input type="text" name="locales[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-reservar{{$parent.$index}}-{{$index}}" name="locales[{{$parent.$index}}][{{$index}}][reservar]" ng-model="hora.reservar" ng-checked="hora.horas.reservar" />
                            <label class="check" for="form-reservar{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-fecha">
                        <input type="text" name="locales[{{$parent.$index}}][{{$index}}][limitereserva]" class="campo field-fecha" ng-model="hora.horas.limitereserva" />
                    </td>-->
                    <?php } ?>
                    <td class="col-remove">
                        <button type="button" class="campo small" ng-click="removeHoraLocal(local,hora)">&times;</button>
                    </td>
                </tr>
                </table>
                <hr style="height:1px;border:none;background-color:white;" />
            </td>
        </tr>
    </table>
    <?php
}

function formHorariosAuditorios($formorigen = "all") {
    global $db;
    $options = array("filter" => "publicado=1");
    $teatro = new Teatros($options,$db);
    $allTeatros = $teatro->readAction();
    ?>
    <table class='tabla-fechas'>
        <caption style='text-align: left'>Auditorios <button type="button" ng-click="addAuditorio()">Añadir auditorio</button></caption>
        <tr style='border: 1px solid white;' ng-repeat="auditorio in auditorios">
            <td>
                <table>
                <tr>
                    <th colspan='6' style='text-align: left;'>
                        <select name="auditorios[{{$index}}][][idteatro]" class="campo" required="required" ng-model="auditorio.idteatro">
                            <?php
                            foreach($allTeatros as $k => $auditorio) {
                                echo "<option value='".$auditorio["idteatro"]."'>".$auditorio["nombre"]."</option>";
                            }
                            ?>
                        </select>
                    </th>
                    <th>
                        <button type="button" ng-click="addHoraAuditorio(auditorio)">Añadir fecha</button>
                    </th>
                    <th>
                        <button type="button" class="campo small" ng-click="removeAuditorio(auditorio)">&times;</button>
                    </th>
                </tr>
                <tr>
                    <?php if($formorigen == "exposicion") { ?>
                    <th class="col-fecha">Inicio</th>
                    <th class="col-fecha">Fin</th>
                    <th class="col-checkbox">Permanente</th>
                    <?php } else { ?>
                    <th class="col-fecha">Fecha</th>
                    <th class="col-entradas">Entradas</th>
                    <th class="col-precioanticipada">Precio anticipada</th>
                    <?php } ?>
                    <th class="col-precio">Precio</th>
                    <?php if($formorigen == "pelicula") { ?>
                        <th class="col-checkbox">3D</th>
                        <th class="col-checkbox">V.O.S.E</th>
                    <?php } ?>
                    <th class="col-checkbox">Agotado</th>
                    <th class="col-checkbox">Cancelado</th>
                    <?php if($formorigen != "exposicion") { ?>
                    <th class="col-checkbox">Todo el día</th>
                    <?php } ?>
                    <th class="col-remove"></th>
                </tr>
                <tr ng-repeat="hora in auditorio.child">
                    <?php if($formorigen == "exposicion") { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="auditorios[{{$parent.$index}}][{{$index}}][inicio]" class="campo field-fecha" ng-model="hora.horas.inicio" />
                    </td>
                    <td class="col-fecha">
                        <input type="datetime" name="auditorios[{{$parent.$index}}][{{$index}}][fin]" class="campo field-fecha" ng-model="hora.horas.fin" />
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-permanente{{$parent.$index}}-{{$index}}" name="auditorios[{{$parent.$index}}][{{$index}}][permanente]" ng-model="hora.permanente" ng-checked="hora.horas.permanente" />
                            <label class="check" for="form-permanente{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } else { ?>
                    <td class="col-fecha">
                        <input type="datetime" required="required" name="auditorios[{{$parent.$index}}][{{$index}}][fecha]" class="campo field-fecha" ng-model="hora.horas.fecha" />
                    </td>
                    <td class="col-entradas">
                        <input type="text" name="auditorios[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo field-entradas" ng-model="hora.horas.urlcompra" />
                    </td>
                    <td class="col-precioanticipada">
                        <input type="text" name="auditorios[{{$parent.$index}}][{{$index}}][precioanticipada]" class="campo field-precioanticipada" ng-model="hora.horas.precioanticipada" />
                    </td>
                    <?php } ?>
                    <td class="col-precio">
                        <input type="text" name="auditorios[{{$parent.$index}}][{{$index}}][precio]" class="campo field-precio" ng-model="hora.horas.precio" />
                    </td>
                    <?php if($formorigen == "pelicula") { ?>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-3dteatro{{$parent.$index}}-{{$index}}" name="auditorios[{{$parent.$index}}][{{$index}}][3d]" ng-model="hora.3d" ng-checked="hora.horas.3d" />
                                <label class="check" for="form-3dteatro{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                        <td class="col-checkbox">
                            <span class="checkbox">
                                <input type="checkbox" id="form-voteatro{{$parent.$index}}-{{$index}}" name="auditorios[{{$parent.$index}}][{{$index}}][vo]" ng-model="hora.vo" ng-checked="hora.horas.vo" />
                                <label class="check" for="form-voteatro{{$parent.$index}}-{{$index}}"></label>
                            </span>
                        </td>
                    <?php } ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-agotadoteatro{{$parent.$index}}-{{$index}}" name="auditorios[{{$parent.$index}}][{{$index}}][agotado]" ng-model="hora.agotado" ng-checked="hora.horas.agotado" />
                            <label class="check" for="form-agotadoteatro{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-canceladoteatro{{$parent.$index}}-{{$index}}" name="auditorios[{{$parent.$index}}][{{$index}}][cancelado]" ng-model="hora.cancelado" ng-checked="hora.horas.cancelado" />
                            <label class="check" for="form-canceladoteatro{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php if($formorigen != "exposicion") { ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-tododiateatro{{$parent.$index}}-{{$index}}" name="auditorios[{{$parent.$index}}][{{$index}}][tododia]" ng-model="hora.tododia" ng-checked="hora.horas.tododia" />
                            <label class="check" for="form-tododiateatro{{$parent.$index}}-{{$index}}"></label>
                        </span>
                    </td>
                    <?php } ?>
                    <td class="col-remove">
                        <button type="button" class="campo small" ng-click="removeHoraAuditorio(auditorio,hora)">&times;</button>
                    </td>
                </tr>
                </table>
                <hr style="height:1px;border:none;background-color:white;" />
            </td>
        </tr>
    </table>
    <?php
}

function formHorariosLugares($formorigen = "all") {
    ?>
    <section id="mapafondo">
        <div id="mapa">
            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
            <div id="map_canvas"></div>
            <input type="hidden" id="coordenadastemp" value='' />
            <input type="hidden" id="coordenadasfield" value='' />
            <label for='form-coordenadas'>Coordenadas <div id="puntosPoly"></div></label>
            <button type="button" id='setCoord'>Aceptar</button>
            <button type="button" id='cleanCoord'>Borrar</button>
        </div>
    </section>
    <table class='tabla-fechas'>
        <caption style='text-align: left'>Lugares <button type="button" ng-click="addDiaLugar()">Añadir fecha</button></caption>
        <tr>
            <?php if($formorigen == "exposicion") { ?>
            <th class="col-fecha">Inicio</th>
            <th class="col-fecha">Fin</th>
            <th class="col-checkbox">Permanente</th>
            <?php } else { ?>
                <th class="col-fecha">Fecha</th>
                <?php if($formorigen == "evento") { ?>
                <th class="col-duracion">Duración</th>
                <?php } ?>
                <th class="col-precioanticipada">Precio anticipada</th>
            <?php } ?>
            <th class="col-precio">Precio</th>
            <th class="col-lugar">Lugar</th>
            <th class="col-precio">Localidad</th>
            <?php if($formorigen == "pelicula") { ?>
                <th class="col-checkbox">3D</th>
                <th class="col-checkbox">V.O.S.E</th>
            <?php } ?>
            <th class="col-checkbox">Agotado</th>
            <th class="col-checkbox">Cancelado</th>
            <?php if($formorigen != "exposicion") { ?>
            <th class="col-checkbox">Todo el día</th>
            <?php } ?>
            <th class="col-coord">Coordenadas</th>
            <th class="col-remove"></th>
        </tr>
        <tr ng-repeat="dia in lugares">
            <?php if($formorigen == "exposicion") { ?>
            <td class="col-fecha">
                <input type="datetime" required="required" name="lugar[0][{{$index}}][inicio]" class="campo field-fecha" ng-model="dia.horas.inicio" />
            </td>
            <td class="col-fecha">
                <input type="datetime" name="lugar[0][{{$index}}][fin]" class="campo field-fecha" ng-model="dia.horas.fin" />
            </td>
            <td class="col-checkbox">
                <span class="checkbox">
                    <input type="checkbox" id="form-permanente-{{$index}}" name="lugar[0][{{$index}}][permanente]" ng-model="dia.horas.permanente" ng-checked="dia.horas.permanente" />
                    <label class="check" for="form-permanente-{{$index}}"></label>
                </span>
            </td>
            <td class="col-precio">
                <input type="text" name="lugar[0][{{$index}}][precio]" class="campo field-precio" ng-model="dia.horas.precio" />
            </td>
            <td class="col-lugar">
                <input type="text" required="required" name="lugar[0][{{$index}}][lugar]" class="campo field-lugar" ng-model="dia.horas.lugar" />
            </td>
            <td class="col-precio">
                <input type="text" required="required" name="lugar[0][{{$index}}][localidad]" class="campo field-precio" ng-model="dia.horas.localidad" />
            </td>
            <td class="col-checkbox">
                <span class="checkbox">
                    <input type="checkbox" id="form-agotadolugar{{$index}}" name="lugar[0][{{$index}}][agotado]" ng-model="dia.horas.agotado" ng-checked="dia.horas.agotado" />
                    <label class="check" for="form-agotadolugar{{$index}}"></label>
                </span>
            </td>
            <td class="col-checkbox">
                <span class="checkbox">
                    <input type="checkbox" id="form-canceladolugar{{$index}}" name="lugar[0][{{$index}}][cancelado]" ng-model="dia.horas.cancelado" ng-checked="dia.horas.cancelado" />
                    <label class="check" for="form-canceladolugar{{$index}}"></label>
                </span>
            </td>
            <td class="col-coord">
                <input type="text" required="required" id="coord-{{$index}}" name="lugar[0][{{$index}}][coordenadas]" class="campo field-coordenadas" ng-model="dia.horas.coordenadas" />
                <button type="button" class="campo small setCoord">Set</button>
            </td>
            <?php } else { ?>
                <td class="col-fecha">
                    <input type="datetime" required="required" name="lugar[0][{{$index}}][fecha]" class="campo field-fecha" ng-model="dia.fecha" />
                </td>
                <?php if($formorigen == "evento") { ?>
                    <td class="col-duracion">
                        <input type="number" name="lugar[0][{{$index}}][duracion]" class="campo field-duracion" ng-model="dia.duracion" />
                    </td>
                <?php } ?>
                <td class="col-precioanticipada">
                    <input type="text" name="lugar[0][{{$index}}][precioanticipada]" class="campo field-precioanticipada" ng-model="dia.precioanticipada" />
                </td>
                <td class="col-precio">
                    <input type="text" name="lugar[0][{{$index}}][precio]" class="campo field-precio" ng-model="dia.precio" />
                </td>
                <td class="col-lugar">
                    <input type="text" required="required" name="lugar[0][{{$index}}][lugar]" class="campo field-lugar" ng-model="dia.lugar" />
                </td>
                <td class="col-precio">
                    <input type="text" required="required" name="lugar[0][{{$index}}][localidad]" class="campo field-precio" ng-model="dia.localidad" />
                </td>
                <?php if($formorigen == "pelicula") { ?>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-3dlugar{{$index}}" name="lugar[0][{{$index}}][3d]" ng-model="hora.3d" ng-checked="hora.horas.3d" />
                            <label class="check" for="form-3dlugar{{$index}}"></label>
                        </span>
                    </td>
                    <td class="col-checkbox">
                        <span class="checkbox">
                            <input type="checkbox" id="form-volugar{{$index}}" name="lugar[0][{{$index}}][vo]" ng-model="hora.vo" ng-checked="hora.horas.vo" />
                            <label class="check" for="form-volugar{{$index}}"></label>
                        </span>
                    </td>
                <?php } ?>
                <td class="col-checkbox">
                    <span class="checkbox">
                        <input type="checkbox" id="form-agotadolugar{{$index}}" name="lugar[0][{{$index}}][agotado]" ng-model="dia.agotado" ng-checked="dia.agotado" />
                        <label class="check" for="form-agotadolugar{{$index}}"></label>
                    </span>
                </td>
                <td class="col-checkbox">
                    <span class="checkbox">
                        <input type="checkbox" id="form-canceladolugar{{$index}}" name="lugar[0][{{$index}}][cancelado]" ng-model="dia.cancelado" ng-checked="dia.cancelado" />
                        <label class="check" for="form-canceladolugar{{$index}}"></label>
                    </span>
                </td>
                <td class="col-checkbox">
                    <span class="checkbox">
                        <input type="checkbox" id="form-tododialugar{{$index}}" name="lugar[0][{{$index}}][tododia]" ng-model="dia.tododia" ng-checked="dia.tododia" />
                        <label class="check" for="form-tododialugar{{$index}}"></label>
                    </span>
                </td>
                <td class="col-coord">
                    <input type="text" required="required" id="coord-{{$index}}" name="lugar[0][{{$index}}][coordenadas]" class="campo field-coordenadas" ng-model="dia.coordenadas" />
                    <button type="button" class="campo small setCoord">Set</button>
                </td>
            <?php } ?>
            <td class="col-remove">
                <button type="button" class="campo small" ng-click="removeDiaLugar(dia)">&times;</button>
            </td>
        </tr>
    </table>
    <?php
}

/*
 * Operaciones a realizar cuando se guarda un evento en un formulario
 */
function formAction($newitem, $action, $id) {
    global $db;

    //Imagen upload
    if(isset($_FILES["imagen"]) && $_FILES["imagen"]["size"] > 0) {
        $imageTempNameImg = uploadImage("temp", $_FILES["imagen"], "image", 0);
    } elseif(!empty($newitem["imagenURL"]) && $newitem["imagenURL"] != "http://image.tmdb.org/t/p/original") {
        $imageTempNameImg = uploadImage("temp", $newitem["imagenURL"], "image", 1);
    }
    //Poster upload
    if(isset($_FILES["poster"]) && $_FILES["poster"]["size"] > 0) {
        $imageTempName = uploadImage("temp", $_FILES["poster"], "poster", 0);
    } elseif(!empty($newitem["posterURL"]) && $newitem["posterURL"] != "http://image.tmdb.org/t/p/original") {
        $imageTempName = uploadImage("temp", $newitem["posterURL"], "poster", 1);
    }
    //Fanart upload
    if(isset($_FILES["fanart"]) && $_FILES["fanart"]["size"] > 0) {
        $imageTempNameFan = uploadImage("temp", $_FILES["fanart"], "fanart", 0);
    } elseif(!empty($newitem["fanartURL"]) && $newitem["fanartURL"] != "http://image.tmdb.org/t/p/original") {
        $imageTempNameFan = uploadImage("temp", $newitem["fanartURL"], "fanart", 1);
    }
    unset($newitem["submit"],$newitem["a"],$newitem["posterURL"],$newitem["fanartURL"],$newitem["imagenURL"]);
    $idItem = "";
    if($action == "a") {
        unset($newitem[$id]);
    }
    switch($id) {
        case "idcine":
            if($action == "a") {
                $object = new Cines($newitem,$db);
            }
            $path = PATH_CINE_CINES;
            break;
        case "idconcierto":
            if($action == "a") {
                $object = new Conciertos($newitem,$db);
            }
            $path = PATH_CONCIERTO_POSTER;
            $pathFan = PATH_CONCIERTO_FANART;
            break;
        case "idcurso":
            if($action == "a") {
                $object = new Cursos($newitem,$db);
            }
            $path = PATH_CURSOS;
            break;
        case "iddeporte":
            if($action == "a") {
                $object = new Deportes($newitem,$db);
            }
            $path = PATH_DEPORTES;
            break;
        case "identradas":
            if($action == "a") {
                $object = new Entradas($newitem,$db);
            }
            $path = PATH_ENTRADAS;
            break;
        case "ideventos":
            if($action == "a") {
                $object = new Eventos($newitem,$db);
            }
            $path = PATH_EVENTO_POSTER;
            $pathFan = PATH_EVENTO_FANART;
            break;
        case "idexposicion":
            if($action == "a") {
                $object = new Exposiciones($newitem,$db);
            }
            $path = PATH_EXPO_POSTER;
            $pathFan = PATH_EXPO_FANART;
            break;
        case "idpelicula":
            if($action == "a") {
                $object = new Peliculas($newitem,$db);
            }
            $path = PATH_CINE_POSTER;
            $pathFan = PATH_CINE_FANART;
            break;
        case "idlocal":
            if($action == "a") {
                $object = new Locales($newitem,$db);
            }
            $path = PATH_LOCALES;
            break;
        case "idmuseo":
            if($action == "a") {
                $object = new Museos($newitem,$db);
            }
            $path = PATH_MUSEOS;
            break;
        case "idobrateatro":
            if($action == "a") {
                $object = new Obras($newitem,$db);
            }
            $path = PATH_OBRA_POSTER;
            $pathFan = PATH_OBRA_FANART;
            break;
        case "idpabellon":
            if($action == "a") {
                $object = new Pabellones($newitem,$db);
            }
            $path = PATH_PABELLONES;
            break;
        case "idpromocion":
            if($action == "a") {
                $object = new Promociones($newitem,$db);
            }
            $path = PATH_CINE_PROMOS;
            break;
        case "idteatro":
            if($action == "a") {
                $object = new Teatros($newitem,$db);
            }
            $path = PATH_TEATROS;
            break;
    }
    if($action == "a") {
        $result = $object->createAction();
        if($result) {
            $newitem[$id] = $result;
            $idItem = $result;
            //Subido poster
            if(isset($imageTempName)) {
                $fparts = pathinfo($imageTempName);
                if(!rename(PATH_TEMP.$imageTempName,BASE_URI.$path.$result.".".$fparts["extension"])) {
                    throw new Exception("Error al copiar el poster", 1);
                } elseif(!rename(PATH_TEMP."t_".$imageTempName,BASE_URI.$path."thumbs/".$result.".".$fparts["extension"])) {
                    throw new Exception("Error al copiar el thumbnail del poster", 1);
                } elseif(!rename(PATH_TEMP."o_".$imageTempName,BASE_URI.$path."original/".$result.".".$fparts["extension"])) {
                    throw new Exception("Error al copiar la imagen de tamaño original del poster", 1);
                } else {
                    $newitem["poster"] = $path.$result.".".$fparts["extension"];
                }
            }
            //Subido Fanart
            if(isset($imageTempNameFan)) {
                $fparts = pathinfo($imageTempNameFan);
                if(!rename(PATH_TEMP.$imageTempNameFan,BASE_URI.$pathFan.$result.".".$fparts["extension"])) {
                    throw new Exception("Error al copiar el fanart", 1);
                } else {
                    $newitem["fanart"] = $pathFan.$result.".".$fparts["extension"];
                }
            }
            //Subida imagen
            if(isset($imageTempNameImg)) {
                $fparts = pathinfo($imageTempNameImg);
                if(!rename(PATH_TEMP.$imageTempNameImg,BASE_URI.$path.$result.".".$fparts["extension"])) {
                    throw new Exception("Error al copiar la foto", 1);
                } elseif(!rename(PATH_TEMP."o_".$imageTempNameImg,BASE_URI.$path."original/".$result.".".$fparts["extension"])) {
                    throw new Exception("Error al copiar la imagen de tamaño original", 1);
                } else {
                    $newitem["imagen"] = $path.$result.".".$fparts["extension"];
                }
            }

            if((isset($newitem["poster"]) && !empty($newitem["poster"])) || (isset($newitem["fanart"]) && !empty($newitem["fanart"])) || (isset($newitem["imagen"]) && !empty($newitem["imagen"]))) {
                unset($object);
                switch($id) {
                    case "idcine":
                        $object = new Cines($newitem,$db);
                        break;
                    case "idconcierto":
                        $object = new Conciertos($newitem,$db);
                        break;
                    case "idcurso":
                        $object = new Cursos($newitem,$db);
                        break;
                    case "iddeporte":
                        $object = new Deportes($newitem,$db);
                        break;
                    case "identradas":
                        $object = new Entradas($newitem,$db);
                        break;
                    case "ideventos":
                        $object = new Eventos($newitem,$db);
                        break;
                    case "idexposicion":
                        $object = new Exposiciones($newitem,$db);
                        break;
                    case "idpelicula":
                        $object = new Peliculas($newitem,$db);
                        break;
                    case "idlocal":
                        $object = new Locales($newitem,$db);
                        break;
                    case "idmuseo":
                        $object = new Museos($newitem,$db);
                        break;
                    case "idobrateatro":
                        $object = new Obras($newitem,$db);
                        break;
                    case "idpabellon":
                        $object = new Pabellones($newitem,$db);
                        break;
                    case "idpromocion":
                        $object = new Promociones($newitem,$db);
                        break;
                    case "idteatro":
                        $object = new Teatros($newitem,$db);
                        break;
                }
                $result = $object->updateAction();
            }
        }
    } elseif($action == "m") {
        //Subido nuevo poster
        if(isset($imageTempName)) {
            deleteAllExtImages(BASE_URI.$path.$newitem[$id]);
            deleteAllExtImages(BASE_URI.$path."thumbs/".$newitem[$id]);
            deleteAllExtImages(BASE_URI.$path."original/".$newitem[$id]);
            $fparts = pathinfo($imageTempName);
            if(!rename(PATH_TEMP.$imageTempName,BASE_URI.$path.$newitem[$id].".".$fparts["extension"])) {
                throw new Exception("Error al copiar el poster", 1);
            } elseif(!rename(PATH_TEMP."t_".$imageTempName,BASE_URI.$path."thumbs/".$newitem[$id].".".$fparts["extension"])) {
                throw new Exception("Error al copiar el thumbnail del poster", 1);
            } elseif(!rename(PATH_TEMP."o_".$imageTempName,BASE_URI.$path."original/".$newitem[$id].".".$fparts["extension"])) {
                throw new Exception("Error al copiar la imagen de tamaño original del poster", 1);
            } else {
                $newitem["poster"] = $path.$newitem[$id].".".$fparts["extension"];
            }
        }
        //Subido nuevo Fanart
        if(isset($imageTempNameFan)) {
            deleteAllExtImages(BASE_URI.$pathFan.$newitem[$id]);
            $fparts = pathinfo($imageTempNameFan);
            if(!rename(PATH_TEMP.$imageTempNameFan,BASE_URI.$pathFan.$newitem[$id].".".$fparts["extension"])) {
                throw new Exception("Error al copiar el fanart", 1);
            } else {
                $newitem["fanart"] = $pathFan.$newitem[$id].".".$fparts["extension"];
            }
        }
        //Subida nueva imagen
        if(isset($imageTempNameImg)) {
            deleteAllExtImages(BASE_URI.$path.$newitem[$id]);
            deleteAllExtImages(BASE_URI.$path."original/".$newitem[$id]);
            $fparts = pathinfo($imageTempNameImg);
            if(!rename(PATH_TEMP.$imageTempNameImg,BASE_URI.$path.$newitem[$id].".".$fparts["extension"])) {
                throw new Exception("Error al copiar la foto", 1);
            } elseif(!rename(PATH_TEMP."o_".$imageTempNameImg,BASE_URI.$path."original/".$newitem[$id].".".$fparts["extension"])) {
                throw new Exception("Error al copiar la imagen de tamaño original", 1);
            } else {
                $newitem["imagen"] = $path.$newitem[$id].".".$fparts["extension"];
            }
        }
        $idItem = $newitem[$id];
        switch($id) {
            case "idcine":
                $object = new Cines($newitem,$db);
                break;
            case "idconcierto":
                $object = new Conciertos($newitem,$db);
                break;
            case "idcurso":
                $object = new Cursos($newitem,$db);
                break;
            case "iddeporte":
                $object = new Deportes($newitem,$db);
                break;
            case "identradas":
                $object = new Entradas($newitem,$db);
                break;
            case "ideventos":
                $object = new Eventos($newitem,$db);
                break;
            case "idexposicion":
                $object = new Exposiciones($newitem,$db);
                break;
            case "idpelicula":
                $object = new Peliculas($newitem,$db);
                break;
            case "idlocal":
                $object = new Locales($newitem,$db);
                break;
            case "idmuseo":
                $object = new Museos($newitem,$db);
                break;
            case "idobrateatro":
                $object = new Obras($newitem,$db);
                break;
            case "idpabellon":
                $object = new Pabellones($newitem,$db);
                break;
            case "idpromocion":
                $object = new Promociones($newitem,$db);
                break;
            case "idteatro":
                $object = new Teatros($newitem,$db);
                break;
        }
        $result = $object->updateAction();
    }
    return $idItem;
}

?>

