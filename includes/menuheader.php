    <nav id="filters">
        <img src="img/interface/home-50.png" alt="Menu toggle" id='menutoggle' width='50' height='50' />
        <span id='header-logo'>
            <a href='main'>
            	<img src="img/interface/logoBlanco.png" alt="Logo" />
                <!--<?php echo PAGENAME; ?> <span class='version'><?php echo VERSION; ?></span>-->
            </a>
        </span>
        <div id='sectionTitle'><h1><?php echo $menuTit; ?></h1></div>
        <?php
        $pagesWithMenu = array("main","lugares","cartelera","conciertos","obrasteatro","exposiciones","deportes","infantil","formacion","eventos");
        if(in_array($p,$pagesWithMenu)) {
            $displayDSel = array("","");
            if(isset($_COOKIE["density"])) {
                if($_COOKIE["density"] == 1) {
                    $displayDSel[1] = "class='current'";
                } else {
                    $displayDSel[0] = "class='current'";
                }
            } else {
                $displayDSel[0] = "class='current'";
            }
            $displaySel = array("","","");
            if(isset($_COOKIE["display"])) {
                if($_COOKIE["display"] == 1) {
                    $displaySel[1] = "class='current'";
                } else if($_COOKIE["display"] == 2) {
                    $displaySel[2] = "class='current'";
                } else {
                    $displaySel[0] = "class='current'";
                }
            } else {
                $displaySel[0] = "class='current'";
            }
        ?>
        <ul id='header-options'>
            <li id='view-config'>
                <img src='img/interface/view-config-50.png' alt='Menú de visualización' width='50' height='50'/>
                <ul class="dropdown-menu">
                    <!--<li>
                        <select>
                            <?php
                                /*$options = array("ayuntamientos" => 1);
                                $g = new GeneralController($options,$db);
                                $allAyunta = $g->readAction();
                                foreach($allAyunta as $a) {
                                    echo "<option>".$a["localidad"]." (".$a["total"].")</option>";
                                }*/
                            ?>
                        </select>
                    </li>-->
                    <li>Densidad:
                        <ul id='view-density'>
                            <li id='density-normal' <?php echo $displayDSel[0]; ?>>Normal</li>
                            <li id='density-compact' <?php echo $displayDSel[1]; ?>>Compacta</li>
                        </ul>
                    </li>
                    <li>Ver:
                        <ul id='view-style'>
                            <li id='card' <?php echo $displaySel[0]; ?>>
                                <img src='img/interface/view-config-card.png' alt='Vista en tarjetas' width='18' height='15'/> Vista en tarjetas
                            </li>
                            <li id='list' <?php echo $displaySel[1]; ?>>
                                <img src='img/interface/view-config-list.png' alt='Vista en lista' width='18' height='15'/> Vista en lista
                            </li>
                            <?php
                            if($p == "main") {
                            /*?>
                            <li id='type' <?php echo $displaySel[2]; ?>>
                                <img src='img/interface/view-config-type.png' alt='Vista en categorías' width='18' height='15'/> Vista en categorías
                            </li>
							<?php */ ?>
                            <li id='map' <?php echo $displaySel[1]; ?>>
                                <img src='img/interface/view-config-map.png' alt='Vista en mapa' width='18' height='15'/> Vista en mapa
                            </li>
                            <?php
                            }
                            ?>
                        </ul>
                    </li>
                    <?php
                    if($p == "main") {
                    ?>
                    <li>Filtro:
                        <ul id='view-filter'>
                            <li>
                                <input type='checkbox' name='film' id='film' class="css-checkbox" <?php echo !isset($_COOKIE["filter-film"]) || $_COOKIE["filter-film"] == 1?'checked="checked"':''; ?>/>
                                <label for="film" class="css-label">
                                    <div id='cartelera'></div>
                                </label>
                            </li>
                            <li>
                                <input type='checkbox' name='concert' id='concert' class="css-checkbox"  <?php echo !isset($_COOKIE["filter-concert"]) || $_COOKIE["filter-concert"] == 1?'checked="checked"':''; ?>/>
                                <label for="concert" class="css-label">
                                    <div id='conciertos'></div>
                                </label>
                            </li>
                            <li>
                                <input type='checkbox' name='theater' id='theater' class="css-checkbox"  <?php echo !isset($_COOKIE["filter-theater"]) || $_COOKIE["filter-theater"] == 1?'checked="checked"':''; ?>/>
                                <label for="theater" class="css-label">
                                    <div id='obrasteatro'></div>
                                </label>
                            </li>
                            <li>
                                <input type='checkbox' name='expo' id='expo' class="css-checkbox"  <?php echo !isset($_COOKIE["filter-expo"]) || $_COOKIE["filter-expo"] == 1?'checked="checked"':''; ?>/>
                                <label for="expo" class="css-label">
                                    <div id='exposiciones'></div>
                                </label>
                            </li>
                            <li>
                                <input type='checkbox' name='sport' id='sport' class="css-checkbox"  <?php echo !isset($_COOKIE["filter-sport"]) || $_COOKIE["filter-sport"] == 1?'checked="checked"':''; ?>/>
                                <label for="sport" class="css-label">
                                    <div id='deportes'></div>
                                </label>
                            </li>
                            <li>
                                <input type='checkbox' name='course' id='course' class="css-checkbox"  <?php echo !isset($_COOKIE["filter-course"]) || $_COOKIE["filter-course"] == 1?'checked="checked"':''; ?>/>
                                <label for="course" class="css-label">
                                    <div id='formacion'></div>
                                </label>
                            </li>
                            <li>
                                <input type='checkbox' name='event' id='event' class="css-checkbox"  <?php echo !isset($_COOKIE["filter-event"]) || $_COOKIE["filter-event"] == 1?'checked="checked"':''; ?>/>
                                <label for="event" class="css-label">
                                    <div id='eventos'></div>
                                </label>
                            </li>
                        </ul>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            </li>
            <li id='search'>
                <img src='img/interface/calendar.png' alt='Menú de busqueda' width='50' height='50'/>
                <ul class="dropdown-menu">
                    <li>
                        <!--<div id="public_search_div">
                            <input type='text' id='public_search' name='public_search' placeholder='Buscar...' value=''/>
                            <span id='result_search'></span>
                        </div>-->
                        <span class='nota'>* Eventos ordenados por hora de comienzo.</span>
                        <div id="datepicker"></div>
                    </li>
                </ul>
            </li>
        </ul>
        <?php
        }
        ?>
    </nav>