<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $cancel = "index.php?p=adminlistentradas";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "identradas");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newentradas">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        if(isset($id) && !empty($id)) {
            $options = array("identradas" => $id);
            $entradas = new Entradas($options,$db);
            $d = $entradas->readAction();
            $d = $d[0];
            if($d["publicado"] == 0) {
                $d["publicado"] = "";
            } elseif($d["publicado"] == 1) {
                $d["publicado"] = "checked";
            }
            $act = "m";
        } else {
            $d = array(
                "identradas" => "",
                "nombre" => "",
                "url" => "",
                "email" => "",
                "comision" => "",
                "telefono" => "",
                "direccion" => "",
                "localidad" => "",
                "provincia" => "",
                "codigopostal" => "",
                "coordenadas" => "",
                "web" => "",
                "imagen" => "",
                "publicado" => ""
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}

if(!empty($d["coordenadas"])) {
    $d["coordenadas"] = trim($d["coordenadas"],"\(\)");
    $coord = explode(",",$d["coordenadas"]);
} else {
    $coord = array("","");
}
?>
    <script>
        $(document).ready(function() {
            initialize();
            <?php
            if(!empty($d["coordenadas"])) {
                echo "setPoints('".$coord[0]."','".$coord[1]."');";
            }
            ?>
        });
    </script>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo $msg["errormsg"];
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar venta de entradas ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo venta de entradas ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newentradas" id="newentradas" class="form" action="index.php?p=adminformentradas" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="identradas" value="<?php echo $d["identradas"]; ?>"/>
                <div>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" value="<?php echo $d["nombre"]; ?>" />
                </div>
                <div>
                    <label for="form-url">URL</label>
                    <input type="url" name="url" id="form-url" class="campo" value="<?php echo $d["url"]; ?>" />
                </div>
                <div>
                    <label for="form-email">E-Mail</label>
                    <input type="email" name="email" id="form-email" class="campo" value="<?php echo $d["email"]; ?>" />
                </div>
                <div>
                    <label for="form-comision">Comisión</label>
                    <input type="text" name="comision" id="form-comision" class="campo" value="<?php echo $d["comision"]; ?>" />
                </div>
                <div>
                    <label for="form-telefono">Teléfono</label>
                    <input type="text" name="telefono" id="form-telefono" class="campo" value="<?php echo $d["telefono"]; ?>" />
                </div>
                <div>
                    <label for="form-direccion">Dirección</label>
                    <input type="text" name="direccion" id="form-direccion" class="campo" value="<?php echo $d["direccion"]; ?>" />
                </div>
                <div>
                    <label for="form-localidad">Localidad</label>
                    <input type="text" name="localidad" id="form-localidad" class="campo" value="<?php echo $d["localidad"]; ?>" />
                </div>
                <div>
                    <label for="form-provincia">Provincia</label>
                    <input type="text" name="provincia" id="form-provincia" class="campo" value="<?php echo $d["provincia"]; ?>" />
                </div>
                <div>
                    <label for="form-codigopostal">Código Postal</label>
                    <input type="text" name="codigopostal" id="form-codigopostal" class="campo" value="<?php echo $d["codigopostal"]; ?>" />
                </div>
                <div class="field-vacio">
                    <label for="form-publicado">Publicado</label>
                    <span class="checkbox">
                        <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                        <label class="check" for="form-publicado"></label>
                    </span>
                </div>
                <div>
                    <label for="form-imagenURL">Imagen desde URL</label>
                    <input type="url" name="imagenURL" id="form-imagenURL" class="campo" /><br>
                    <label for="form-imagen">Imagen desde escritorio</label>
                    <input type="file" name="imagen" id="form-image" class="campo" />
                    <?php
                    if(!empty($d["imagen"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["imagen"]."?".strtotime("now")."' alt='Imagen del sitio de entradas' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg' onclick='deleteImage(this,".$d["identradas"].",\"".$d["imagen"]."\",\"Entradas\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <div>
                    <label for='form-coordenadas'>Coordenadas</label>
                    <div id="puntosPoly"><?php echo "(".$d["coordenadas"].")"; ?></div><br>
                    <div id="map_canvas"></div>
                    <input type="hidden" name="coordenadas" id="form-coordenadas" value='<?php echo $d["coordenadas"]; ?>'/>
                    <br><br>
                </div>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Venta de entradas creada con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Venta de entradas modificada con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformentradas&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformentradas">Crear una nueva</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>