<?php
/**
 * Clase con metodos para operaciones con promociones
 *
 * @package API
 * @author Trasno  
 */
class PromocionItem {
    protected $db;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }
    
    /**
     * Coge todas las promociones
     *
     * @param string $filter (opcional) filtro de id del cine que tiene esas promociones 
     * @param string $order (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listPromociones($filter = "", $order = "inicio asc") {
        if(!empty($filter)) {
            $where = ", ".BDPREFIX."cines_has_promociones cp where cp.idcine=".$filter." and p.idpromocion=cp.idpromocion";
        } else {
            $where = "";
        }
        $query = sprintf("select p.* from %spromociones p %s order by p.%s", BDPREFIX, $this->db->secure_field($where), $this->db->secure_field($order));
        $r = $this->db->query($query);
        
        $result = array();
        while($promocion = $this->db->fetch($r)) {
            $result[] = $promocion;
        }
        
        return $result;
    }
    
    /**
     * Coge un promocion y todos sus datos (salas, servicios...)
     *
     * @param int $idpromocion id del promocion 
     * @return array|false
     */
     public function dataPromocion($idpromocion) {
        if(!empty($idpromocion)) {
            $query = sprintf("select p.* from %spromociones p where p.idpromocion = %d", BDPREFIX, $this->db->secure_field($idpromocion));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }
     
    /*
     * Eliminar una promocion
     *
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    public function deletePromocion($idpromocion) {
        if(!empty($idpromocion)) {
            $query = sprintf("delete from %spromociones where idpromocion = %d",BDPREFIX, $this->db->secure_field($idpromocion));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %scines_has_promociones where idpromocion = %d",BDPREFIX, $this->db->secure_field($idpromocion));
                $r = $this->db->execute($query);
                if($r) {
                    deleteAllExtImages("img/promos/".$idpromocion);
                    deleteAllExtImages("img/promos/original/".$idpromocion);
                    return true;
                }
            }
        }
        return false;
    }
    
    /*
     * Insertar un promocion
     *
     * @param array $promocion datos de la promocion
     * @param string $promocion['descripcion']
     * @param datetime $promocion['inicio']
     * @param datetime $promocion['fin']
     * @return int
     */
    public function addPromocion($promocion) {
        if(!$this->checkPromocion("descripcion", $promocion["descripcion"])) {
            $fields = "";
            $values = "";
            foreach($promocion as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %spromociones (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addPromocion] Error en la query: ".$query, 1);    
            }
        } else {
            throw new Exception("[addPromocion] Ya existe la promocion.", 1);
        }
    }
    
    /*
     * Actualiza los campos de una promocion
     *
     * @param array $promocion datos de la promocion
     * @param int $promocion['idpromocion'] identificador de la promocion
     * @param string $promocion['descripcion']
     * @param datetime $promocion['inicio']
     * @param datetime $promocion['fin']
     * @return boolean
     */
    public function updatePromocion($promocion) {
        if($this->checkPromocion("idpromocion", $promocion["idpromocion"])) {
            $fields = "";
            foreach($promocion as $key => $value) {
                if($key != "idpromocion") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %spromociones set %s where idpromocion = %d", BDPREFIX, $fields, $this->db->secure_field($promocion["idpromocion"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updatePromocion] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updatePromocion] No existe la promocion.", 1);
        }
    }
    
    /*
     * Publicar promociones para que sean visibles
     *
     * @param int $ids ids de las promociones
     * @return boolean
     */
    public function publishPromocion($ids) {
        $query = sprintf("update %spromociones set publicado=1 where idpromocion in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishPromocion] Error en la query: ".$query, 1);    
        }
    }
    
    /*
     * Despublicar promociones para que no sean visibles
     *
     * @param int $ids ids de las promociones
     * @return boolean
     */
    public function unpublishPromocion($ids) {
        $query = sprintf("update %spromociones set publicado=0 where idpromocion in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishPromocion] Error en la query: ".$query, 1);    
        }
    }
    
    /*
     * Comprueba si existe la promocion
     * 
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkPromocion($field, $value) {
        $query = sprintf("select p.* from %spromociones p where p.%s like '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);
        
        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END