<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $entradas = "";
    $cancel = "index.php?p=adminlisttheater";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "idteatro");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newteatro">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        $entrada = new Entradas("",$db);
        $allEntradas = $entrada->readAction();
        if(isset($id) && !empty($id)) {
            $options = array("idteatro" => $id);
            $teatro = new Teatros($options,$db);
            $d = $teatro->readAction();
            $d = $d[0];
            if($d["publicado"] == 0) {
                $d["publicado"] = "";
            } elseif($d["publicado"] == 1) {
                $d["publicado"] = "checked";
            }
            $act = "m";

            foreach($d["entradas"] as $k => $ent) {
               $entradas = $entradas."{id: '".$ent["identradas"]."'},";
            }
        } else {
            $d = array(
                "idteatro" => "",
                "nombre" => "",
                "direccion" => "",
                "localidad" => "",
                "provincia" => "",
                "codigopostal" => "",
                "coordenadas" => "",
                "telefono" => "",
                "email" => "",
                "web" => "",
                "twitter" => "",
                "facebook" => "",
                "google" => "",
                "otrasocial" => "",
                "notas" => "",
                "descripcion" => "",
                "imagen" => "",
                "menciones" => "",
                "publicado" => "",
                "tipolocal" => ""
                );
            $servicios = array(
                "minusvalidos" => "",
                "hd" => "",
                "3d" => "",
                "parking" => "",
                "carnetjoven" => "",
                "carnetestudiante" => "",
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}

if(!empty($d["coordenadas"])) {
    $d["coordenadas"] = trim($d["coordenadas"],"\(\)");
    $coord = explode(",",$d["coordenadas"]);
} else {
    $coord = array("","");
}
?>
    <script>
        $(document).ready(function() {
            initialize();
            <?php
            if(!empty($d["coordenadas"])) {
                echo "setPoints('".$coord[0]."','".$coord[1]."');";
            }
            ?>
        });
    </script>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo $msg["errormsg"];
                exit;
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar teatro ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo teatro ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newteatro" id="newteatro" class="form" action="index.php?p=adminformtheater" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="idteatro" value="<?php echo $d["idteatro"]; ?>"/>
                <div class='divsmall'>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" value="<?php echo htmlspecialchars($d["nombre"]); ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-direccion">Dirección</label>
                    <input type="text" name="direccion" id="form-direccion" class="campo" value="<?php echo $d["direccion"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-localidad">Localidad</label>
                    <input type="text" name="localidad" id="form-localidad" class="campo" value="<?php echo $d["localidad"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-provincia">Provincia</label>
                    <input type="text" name="provincia" id="form-provincia" class="campo" value="<?php echo $d["provincia"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-codigopostal">Código Postal</label>
                    <input type="text" name="codigopostal" id="form-codigopostal" class="campo" value="<?php echo $d["codigopostal"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-telefono">Teléfono</label>
                    <input type="text" name="telefono" id="form-telefono" class="campo" value="<?php echo $d["telefono"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-email">E-Mail</label>
                    <input type="email" name="email" id="form-email" class="campo" value="<?php echo $d["email"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-web">Página web</label>
                    <input type="url" name="web" id="form-web" class="campo" value="<?php echo $d["web"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-twitter">Twitter</label>
                    <input type="url" name="twitter" id="form-twitter" class="campo" value="<?php echo $d["twitter"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-facebook">Facebook</label>
                    <input type="url" name="facebook" id="form-facebook" class="campo" value="<?php echo $d["facebook"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-google">Google+</label>
                    <input type="url" name="google" id="form-google" class="campo" value="<?php echo $d["google"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-otrasocial">Otra web social</label>
                    <input type="url" name="otrasocial" id="form-otrasocial" class="campo" value="<?php echo $d["otrasocial"]; ?>" />
                </div>
                <div class='divbig'>
                    <label for="form-descripcion">Descripción</label>
                    <textarea name="descripcion" id="form-descripcion" class="campo"><?php echo $d["descripcion"]; ?></textarea>
                    <br><br>
                </div>
                <div class='divbig'>
                    <label for="form-notas">Notas</label>
                    <textarea name="notas" id="form-notas" class="campo"><?php echo $d["notas"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-menciones">Menciones / Agradecimientos / Créditos</label>
                    <textarea name="menciones" id="form-menciones" class="campo"><?php echo $d["menciones"]; ?></textarea>
                    <br><br>
                </div>
                <div class='divbig'>
                    <div style='display: inline-block;'>
                        <label for="form-publicado">Publicado</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                            <label class="check" for="form-publicado"></label>
                        </span>
                    </div>
                    <div style="padding: 10px 20px;">
                        Votos: <?php echo isset($d["numvotos"]) && !empty($d["numvotos"])?$d["numvotos"]:0 ?>
                        <br>Media: <?php echo number_format(isset($d["numvotos"]) && !empty($d["numvotos"])?$d["puntos"]/$d["numvotos"]:0,2); ?>
                    </div>
                </div>
                <div class='divbig'>
                    <label for="form-imagenURL">Imagen desde URL</label>
                    <input type="url" name="imagenURL" id="form-imagenURL" class="campo" />
                    <input type="file" name="imagen" style='display:none;' id="form-imagen" class="campo" />
                    <button type="button" onclick='$("#form-imagen").click();'>Subir</button>
                    <?php
                    if(!empty($d["imagen"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["imagen"]."?".strtotime("now")."' alt='Imagen del teatro' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg1' onclick='deleteImage(this,".$d["idteatro"].",\"".$d["imagen"]."\",\"Teatros\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <div class='divbig'>
                    <label for="form-tipolocal">Tipo de local</label>
                    <input type="text" name="tipolocal" id="form-tipolocal" class="campo" value="<?php echo $d["tipolocal"]; ?>" />
                </div>
                <table class='tabla-fechas'>
                <caption style='text-align: left'>Coordenadas</caption>
                <tr>
                    <td colspan='4'>
                        <input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
                        <div id="map_canvas"></div>
                        <input type="hidden" name="coordenadas" id="coordenadastemp" value='<?php echo $d["coordenadas"]; ?>'/>
                    </td>
                    <td colspan='4'>
                        <label for='form-coordenadas'>Coordenadas <div id="puntosPoly"><?php echo "(".$d["coordenadas"].")"; ?></div></label>
                        <button type="button" id='cleanCoord'>Borrar</button>
                    </td>
                </tr>
                </table>
                <br>
                <fieldset>
                    <legend>Venta de entradas</legend>
                    <div ng-repeat="entrada in thing.entradas">
                        <select name="entradas[]" class="campo" ng-model="entrada.id">
                            <?php
                            foreach($allEntradas as $k => $ent) {
                                echo "<option value='".$ent["identradas"]."'>".$ent["nombre"]."</option>";
                            }
                            ?>
                        </select>
                        <button class="campo small" ng-click="removeEntrada(entrada)">&times;</button>
                    </div>
                    <br>
                    <button type="button" ng-click="addEntrada()">Añadir</button><br>
                </fieldset>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Teatro creado con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Teatro modificado con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformtheater&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformtheater">Crear uno nuevo</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>
    <script>
        entradas = [<?php echo $entradas; ?>];
    </script>