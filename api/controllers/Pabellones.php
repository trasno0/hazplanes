<?php
/**
 * Clase controlador de pabellones
 *
 * @package API
 * @author Trasno
 */
class Pabellones {
    private $params;
    private $general;
    private $pabellonItem;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->pabellonItem = new PabellonItem($db);
    }
    
    /**
     * Destructor
     *
     * @return void
     * @author trasno 
     */
    public function __destruct() {
        $this->pabellonItem = null;
    }
    
    /**
     * Crear pabellon
     *
     * @return int
     */
    public function createAction() {
        $pabellon = array();
        foreach($this->params as $key => $value){
            $pabellon[$key] = $value; 
        }
        
        $pabellon["publicado"] = array_key_exists("publicado",$pabellon)? 1:0;
        
        //Creamos el pabellon
        $result = $this->pabellonItem->addPabellon($pabellon);
        
        return $result;
    }
    
    /**
     * Recuperar pabellon(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idpabellon"]) && !empty($this->params["idpabellon"])) {
            $data = $this->pabellonItem->dataPabellon($this->params["idpabellon"]);
        } else {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "",
                "order" => "idpabellon desc"
                );
            if(!empty($this->params)) {
                $options = array_merge($options,$this->params);
            }
            $data = $this->pabellonItem->listPabellones($options);
        }
        
        return $data;
    }
    
    /**
     * Actualizar pabellon
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->pabellonItem->publishPabellon($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->pabellonItem->unpublishPabellon($this->params["ids"]);
            } else {
                $result = false;
            }
        } else {
            $pabellon = array();
            foreach($this->params as $key => $value){
                $pabellon[$key] = $value; 
            }
                    
            unset($pabellon["servicios"],$pabellon["promos"],$pabellon["entradas"],$bdRelPabellonPromo,$promos,$bdRelPabellonEntradas,$entradas);
            $pabellon["publicado"] = array_key_exists("publicado",$pabellon)? 1:0;
            
            //Pabellon
            $result = $this->pabellonItem->updatePabellon($pabellon);
        }
        
        return $result;
    }
    
    /**
     * Eliminar pabellon
     *
     * @return boolean
     * 
     * TODO: eliminar conciertos/obras del pabellon a eliminar
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->pabellonItem->deletePabellon($this->params["id"]);
        }
        
        return $result;
    }
}