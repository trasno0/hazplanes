<?php
/**
 * Clase controlador de teatros
 *
 * @package API
 * @author Trasno
 */
class Teatros {
    private $params;
    private $teatroItem;
    private $general;
    //private $serviciosItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->teatroItem = new TeatroItem($db);
        //$this->serviciosItem = new ServiciosItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->teatroItem = null;
    }

    /**
     * Crear teatro
     *
     * @return int
     */
    public function createAction() {
        $teatro = array();
        foreach($this->params as $key => $value){
            $teatro[$key] = $value;
        }

        //Cogemos los datos de los servicios para utilizarlos una vez creado el teatro
        /*if(isset($teatro["servicios"])) {
            $servicios = $teatro["servicios"];
            foreach($servicios as $k => $s) {
                $servicios[$k] = 1;
            }
        } else {
            $servicios = array();
        }*/

        //Cogemos los datos de las promociones para utilizarlos una vez creado el teatro
        /*if(isset($teatro["promos"])) {
            $promos = $teatro["promos"];
        } else {
            $promos = array();
        }*/

        //Cogemos los datos de las entradas para utilizarlos una vez creado el teatro
        if(isset($teatro["entradas"])) {
            $entradas = $teatro["entradas"];
        } else {
            $entradas = array();
        }

        unset($teatro["servicios"],$teatro["promos"],$teatro["entradas"]);
        $teatro["publicado"] = array_key_exists("publicado",$teatro)? 1:0;

        //Creamos el teatro
        $result = $this->teatroItem->addTeatro($teatro);

        if($result) {
            //Añadir servicios al teatro
            //$servicios["idteatro"] = $result;
            //$this->serviciosItem->addServicios($servicios);
            //Añadir promociones
            //foreach($promos as $k => $promo) {
            //    $this->teatroItem->setPromociones($result, $promo);
            //}
            //Añadir entradas
            foreach($entradas as $k => $entrada) {
                $this->teatroItem->setEntradas($result, $entrada);
            }
        }

        return $result;
    }

    /**
     * Recuperar teatro(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idteatro"]) && !empty($this->params["idteatro"])) {
            $data = $this->teatroItem->dataTeatro($this->params["idteatro"]);
        } else {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "",
                "order" => "nombre"
                );
            if(!empty($this->params)) {
                $options = array_merge($options,$this->params);
            }
            $data = $this->teatroItem->listTeatros($options);
        }

        return $data;
    }

    /**
     * Actualizar teatro
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->teatroItem->publishTeatro($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->teatroItem->unpublishTeatro($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "vote") {
                $result = $this->teatroItem->voteTeatro($this->params["idlugar"],$this->params["points"]);
            } elseif(strtolower($this->params["op"]) == "field") {
                $fields = array();
                foreach($this->params as $key => $value){
                    $fields[$key] = $value;
                }
                unset($fields["op"]);
                $result = $this->teatroItem->updateTeatro($fields);
            } else {
                $result = false;
            }
        } else {
            $teatro = array();
            foreach($this->params as $key => $value){
                $teatro[$key] = $value;
            }

            //Servicios
            /*if(isset($teatro["servicios"])) {
                $servicios = $teatro["servicios"];
                foreach($servicios as $k => $s) {
                    $servicios[$k] = 1;
                }
            } else {
                $servicios = array();
            }
            $servicios["idteatro"] = $teatro["idteatro"];
            $this->serviciosItem->updateServicios($servicios);*/

            //Entradas
            $bdRelTeatroEntradas = $this->teatroItem->getEntradas($teatro["idteatro"]);
            if(isset($teatro["entradas"])) {
                $entradas = $teatro["entradas"];
            } else {
                $entradas = array();
            }
            foreach($entradas as $k => $entrada) {
                $existe = false;
                foreach($bdRelTeatroEntradas as $key => $value) {
                    if($value["identradas"] == $entrada) {
                        $existe = true;
                        unset($bdRelTeatroEntradas[$key]);
                        break;
                    }
                }
                if(!$existe) {
                    $this->teatroItem->setEntradas($teatro["idteatro"], $entrada);
                }
            }
            foreach($bdRelTeatroEntradas as $s) {
                $this->teatroItem->delEntradas($teatro["idteatro"],$s["identradas"]);
            }

            //Promociones
            /*$bdRelTeatroPromo = $this->teatroItem->getPromociones($teatro["idteatro"]);
            if(isset($teatro["promos"])) {
                $promos = $teatro["promos"];
            } else {
                $promos = array();
            }
            foreach($promos as $k => $promo) {
                $existe = false;
                foreach($bdRelTeatroPromo as $key => $value) {
                    if($value["idpromocion"] == $promo) {
                        $existe = true;
                        unset($bdRelTeatroPromo[$key]);
                        break;
                    }
                }
                if(!$existe) {
                    $this->teatroItem->setPromociones($teatro["idteatro"], $promo);
                }
            }
            foreach($bdRelTeatroPromo as $s) {
                $this->teatroItem->delPromociones($teatro["idteatro"],$s["idpromocion"]);
            }*/

            unset($teatro["servicios"],$teatro["promos"],$teatro["entradas"],$bdRelTeatroPromo,$promos,$bdRelTeatroEntradas,$entradas);
            $teatro["publicado"] = array_key_exists("publicado",$teatro)? 1:0;

            //Teatro
            $result = $this->teatroItem->updateTeatro($teatro);
        }

        return $result;
    }

    /**
     * Eliminar teatro
     *
     * @return boolean
     *
     * TODO: eliminar conciertos/obras del teatro a eliminar
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            //$result = $this->serviciosItem->deleteServicios($this->params["idteatro"]);
            $result = $this->teatroItem->delEntradas($this->params["id"]);
            //$result = $this->teatroItem->delPromociones($this->params["idteatro"]);
            if($result) {
                $result = $this->teatroItem->deleteTeatro($this->params["id"]);
            }
        }

        return $result;
    }
}