<?php
/**
 * Clase con metodos para operaciones con teatros
 *
 * @package API
 * @author Trasno
 */
class TeatroItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todos los teatros
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de pelicula.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listTeatros($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf("select t.* from %steatros t %s order by t.%s %s", BDPREFIX, $options["filter"], $this->db->secure_field($options["order"]), $this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($teatro = $this->db->fetch($r)) {
            $result[] = $teatro;
        }
        foreach($result as $k => $v) {
            //$result[$k]["promos"] = $this->getPromociones($teatro["idteatro"]);
            $result[$k]["entradas"] = $this->getEntradas($v["idteatro"]);
        }
        return $result;
    }

    /**
     * Coge un teatro y todos sus datos (salas, servicios...)
     *
     * @param int $idteatro id del teatro
     * @return array|false
     */
     public function dataTeatro($idteatro) {
        if(!empty($idteatro)) {
            $query = sprintf("select t.* from %steatros t where t.idteatro = %d", BDPREFIX, $this->db->secure_field($idteatro));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                $teatro = $this->db->fetch($r);
                //$teatro["promos"] = $this->getPromociones($teatro["idteatro"]);
                $teatro["entradas"] = $this->getEntradas($teatro["idteatro"]);
                return array(0 => $teatro);
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un teatro
     *
     * @param int $idteatro id del teatro
     * @return boolean
     */
    public function deleteTeatro($idteatro) {
        if(!empty($idteatro)) {
            $query = sprintf("delete from %steatros where idteatro = %d",BDPREFIX, $this->db->secure_field($idteatro));
            $r = $this->db->execute($query);
            if($r) {
                deleteAllExtImages("img/teatros/".$idteatro);
                deleteAllExtImages("img/teatros/original/".$idteatro);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar un teatro
     *
     * @param array $teatro datos del teatro
     * @param string $teatro['nombre'] nombre del teatro
     * @param string $teatro['direccion']
     * @param string $teatro['localidad']
     * @param string $teatro['provincia']
     * @param int $teatro['codigopostal']
     * @param string $teatro['coordenadas']
     * @param string $teatro['telefono']
     * @param string $teatro['email']
     * @param string $teatro['web']
     * @param string $teatro['twitter']
     * @param string $teatro['facebook']
     * @param string $teatro['google']
     * @param string $teatro['otrasocial']
     * @param string $teatro['notas']
     * @param string $teatro['descripcion']
     * @param string $teatro['imagen']
     * @return int
     */
    public function addTeatro($teatro) {
        if(!$this->checkTeatro("nombre", $teatro["nombre"])) {
            $fields = "";
            $values = "";
            foreach($teatro as $key => $value) {
                if(!empty($fields)) {
                    $fields = $this->db->secure_field($fields).",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("insert into %steatros (%s, creado) VALUES (%s, '%s')", BDPREFIX, $fields, $values, $now);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addTeatro] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addTeatro] Ya existe el teatro.", 1);
        }
    }

    /*
     * Actualiza los campos de un teatro
     *
     * @param array $teatro datos del teatro
     * @param int $teatro['idteatro'] identificador del teatro
     * @param string $teatro['nombre'] nombre del teatro
     * @param string $teatro['direccion']
     * @param string $teatro['localidad']
     * @param string $teatro['provincia']
     * @param int $teatro['codigopostal']
     * @param string $teatro['coordenadas']
     * @param string $teatro['telefono']
     * @param string $teatro['email']
     * @param string $teatro['web']
     * @param string $teatro['twitter']
     * @param string $teatro['facebook']
     * @param string $teatro['google']
     * @param string $teatro['otrasocial']
     * @param string $teatro['notas']
     * @param string $teatro['descripcion']
     * @param string $teatro['imagen']
     * @return boolean
     */
    public function updateTeatro($teatro) {
        if($this->checkTeatro("idteatro", $teatro["idteatro"])) {
            $fields = "";
            foreach($teatro as $key => $value) {
                if($key != "idteatro") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("update %steatros set %s, actualizado = '%s' where idteatro = %d", BDPREFIX, $fields, $now, $this->db->secure_field($teatro["idteatro"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateTeatro] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateTeatro] No existe el teatro.", 1);
        }
    }

    /*
     * Publicar teatros para que sean visibles
     *
     * @param int $ids ids de los teatros
     * @return boolean
     */
    public function publishTeatro($ids) {
        $query = sprintf("update %steatros set publicado=1 where idteatro in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishTeatro] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar teatros para que no sean visibles
     *
     * @param int $ids ids de los teatros
     * @return boolean
     */
    public function unpublishTeatro($ids) {
        $query = sprintf("update %steatros set publicado=0 where idteatro in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishTeatro] Error en la query: ".$query, 1);
        }
    }

    /*
     * Votar teatros
     *
     * @param int $idlugar id del teatro
     * @param int $points puntos que le dan
     * @return boolean
     */
    public function voteTeatro($idlugar,$points) {
        $query = sprintf("update %steatros set numvotos=numvotos+1, puntos=puntos+%d where idteatro = %d", BDPREFIX, $this->db->secure_field($points), $this->db->secure_field($idlugar));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[voteTeatro] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar un sitio de entradas con un teatro
     *
     * @param int $idteatro id del teatro
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function setEntradas($idteatro, $identradas) {
        if(!$this->checkRelEntradas($idteatro, $identradas)) {
            $query = sprintf("insert into %steatros_has_entradas (idteatro,identradas) VALUES (%d,%d)", BDPREFIX, $this->db->secure_field($idteatro), $this->db->secure_field($identradas));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[setEntradas] Ya existe la relacion entre el teatro y el sitio de entradas.", 1);
        }
    }

    /*
     * Relacionar una promocion con un teatro
     *
     * @param int $idteatro id del teatro
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    public function setPromociones($idteatro, $idpromocion) {
        if(!$this->checkRelPromos($idteatro, $idpromocion)) {
            $query = sprintf("insert into %steatros_has_promociones (idteatro,idpromocion) VALUES (%d,%d)", BDPREFIX, $this->db->secure_field($idteatro), $this->db->secure_field($idpromocion));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setPromociones] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[setPromociones] Ya existe la relacion entre el teatro y la promoción.", 1);
        }
    }

    /*
     * Relacionar un sitio de entradas con un teatro
     *
     * @param int $idteatro id del teatro
     * @return array
     */
    public function getEntradas($idteatro) {
        $query = sprintf("select te.identradas from %steatros_has_entradas te where te.idteatro = %d", BDPREFIX, $this->db->secure_field($idteatro));
        $r = $this->db->query($query);
        if($r) {
            $result = array();
            while($teatro_entradas = $this->db->fetch($r)) {
                $result[] = $teatro_entradas;
            }
            return $result;
        } else {
            throw new Exception("[getEntradas] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar una promocion con un teatro
     *
     * @param int $idteatro id del teatro
     * @return array
     */
    public function getPromociones($idteatro) {
        $query = sprintf("select tp.idpromocion from %steatros_has_promociones tp where tp.idteatro = %d", BDPREFIX, $this->db->secure_field($idteatro));
        $r = $this->db->query($query);
        if($r) {
            $result = array();
            while($teatro_promociones = $this->db->fetch($r)) {
                $result[] = $teatro_promociones;
            }
            return $result;
        } else {
            throw new Exception("[getPromociones] Error en la query: ".$query, 1);
        }
    }

    /*
     * Eliminar la relacion de un sitio de entradas con un teatro
     *
     * @param int $idteatro id del teatro
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function delEntradas($idteatro, $identradas = 0) {
        if(empty($identradas) || $this->checkRelEntradas($idteatro, $identradas)) {
            $query = sprintf("delete from %steatros_has_entradas where idteatro = %d", BDPREFIX, $this->db->secure_field($idteatro));
            if(empty($identradas)) {
                $query .= " and identradas = ".$this->db->secure_field($identradas);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delEntradas] No existe la relacion entre el teatro y el sitio de entradas.", 1);
        }
    }

    /*
     * Eliminar la relacion de una promocion con un teatro
     *
     * @param int $idteatro id del teatro
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    public function delPromociones($idteatro, $idpromocion = 0) {
        if(empty($idpromocion) || $this->checkRelPromos($idteatro, $idpromocion)) {
            $query = sprintf("delete from %steatros_has_promociones where idteatro = %d", BDPREFIX, $this->db->secure_field($idteatro));
            if(empty($identradas)) {
                $query .= " and idpromocion = ".$this->db->secure_field($idpromocion);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delPromociones] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delPromociones] No existe la relacion entre el teatro y la promoción.", 1);
        }
    }

    /*
     * Comprueba si existe el teatro
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkTeatro($field, $value) {
        $query = sprintf("select t.* from %steatros t where t.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion teatro-entradas
     *
     * @param int $idteatro id del teatro
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    private function checkRelEntradas($idteatro, $identradas) {
        $query = sprintf("select te.* from %steatros_has_entradas te where te.idteatro = %d and te.identradas = %d", BDPREFIX, $this->db->secure_field($idteatro), $this->db->secure_field($identradas));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion teatro-promociones
     *
     * @param int $idteatro id del teatro
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    private function checkRelPromos($idteatro, $idpromocion) {
        $query = sprintf("select tp.* from %steatros_has_promociones tp where tp.idteatro = %d and tp.idpromocion = %d", BDPREFIX, $this->db->secure_field($idteatro), $this->db->secure_field($idpromocion));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END