<?php
pageAccessControl();
?>
    <section id='content'>
        <div class="grid">
            <section id="info">
                <h2><?php echo $page_title; ?></h2>
                <div id='infotodo'>
                    <p>
                        Nos encanta poder disfrutar de todo lo que ofrece nuestra ciudad y por eso hemos decidido compartirlo con todos vosotros.
                    </p>
                    <p>
                        Si quieres ponerte en contacto con nosotros, envíanos un correo a <a href="mailto:info@hazplanes.com">info@hazplanes.com</a> o mandanos un mensaje a través de nuestro formulario de contacto o de nuestras páginas en redes sociales
                    </p>
                    <div style='text-align: center; display: block;'>
                        <a href='http://www.google.com/+HazplanesWeb' target='_blank'><img src='img/interface/logogp-29.png' alt='Logo Google+' width='29' height='29'/></a> 
                        <a href='https://www.facebook.com/hazplanesweb' target='_blank'><img src='img/interface/logofb-29.png' alt='Logo Facebook' width='29' height='29'/></a> 
                        <a href='https://twitter.com/HazPlanes' target='_blank'><img src='img/interface/logotw-29.png' alt='Logo Twitter' width='29' height='29'/></a>
                        <br><br>
                        <u>Miembros de honor</u>
                        <p>
                            <!-- Place this tag where you want the widget to render. -->
                            <div class="g-person" data-width="250" data-href="//plus.google.com/u/0/112227619986468336273" data-rel="author"></div> 
                            <div class="g-person" data-width="250" data-href="//plus.google.com/u/0/108739759490724504009" data-rel="author"></div>
                        </p>
                    </div>
                </div>
            </section>
        </div>
    </section>
    