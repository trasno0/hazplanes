<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $cancel = "index.php?p=adminlistcursos";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "idcurso");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newcurso">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        if(isset($id) && !empty($id)) {
            $options = array("idcurso" => $id);
            $curso = new Cursos($options,$db);
            $d = $curso->readAction();
            $d = $d[0];
            $act = "m";
            $d["publicado"] == 0? $d["publicado"] = "" : $d["publicado"] = "checked";
            $d["infantil"] == 0? $d["infantil"] = "" : $d["infantil"] = "checked";
            $d["reserva"] == 0? $d["reserva"] = "" : $d["reserva"] = "checked";
            //Convertimos los horarios de la BD en json para angularjs
            $horarios = horariosToJson($d);
        } else {
            $d = array(
                "idcurso" => "",
                "nombre" => "",
                "descripcion" => "",
                "notas" => "",
                "tipo" => "",
                "web" => "",
                "coordenadas" => "",
                "poster" => "",
                "menciones" => "",
                "publicado" => "checked",
                "infantil" => "",
                "reserva" => "",
                "limitereserva" => ""
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}
?>
    <script>
        $(document).ready(function() {
            initialize();
        });
    </script>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo $msg["errormsg"];
                exit;
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar curso ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo curso ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newcurso" id="newcurso" class="form" action="index.php?p=adminformcurso" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="idcurso" value="<?php echo $d["idcurso"]; ?>"/>
                <div class='divsmall'>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" required="required" value="<?php echo htmlspecialchars($d["nombre"]); ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-tipo">Tipo</label>
                    <input type="text" name="tipo" id="form-tipo" class="campo" value="<?php echo $d["tipo"]; ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-web">Web</label>
                    <input type="text" name="web" id="form-web" class="campo" value="<?php echo $d["web"]; ?>" />
                </div>
                <div class='divbig'>
                    <div style='display: inline-block;'>
                        <label for="form-publicado">Publicado</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                            <label class="check" for="form-publicado"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-infantil">Infantil</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-infantil" name="infantil" <?php echo $d["infantil"]; ?> />
                            <label class="check" for="form-infantil"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-reserva">Reservar</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-reserva" name="reserva" <?php echo $d["reserva"]; ?> />
                            <label class="check" for="form-reserva"></label>
                        </span>
                        <input type='datetime' id='form-limitereserva' name='limitereserva' class="campo field-limitereserva" value="<?php echo $d["limitereserva"]; ?>" />
                    </div>
                </div>
                <div class='divbig'>
                    <label for="form-descripcion">Descripción</label>
                    <textarea name="descripcion" id="form-descripcion" class="campo"><?php echo $d["descripcion"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-notas">Notas</label>
                    <textarea name="notas" id="form-notas" class="campo"><?php echo $d["notas"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-menciones">Menciones / Agradecimientos / Créditos</label>
                    <textarea name="menciones" id="form-menciones" class="campo"><?php echo $d["menciones"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-posterURL">Poster desde URL</label>
                    <input type="url" name="posterURL" id="form-posterURL" class="campo" />
                    <input type="file" style='display:none;' name="poster" id="form-poster" class="campo" />
                    <button type="button" onclick='$("#form-poster").click();'>Subir</button>
                    <?php
                    if(!empty($d["poster"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["poster"]."?".strtotime("now")."' alt='Imagen del curso' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg' onclick='deleteImage(this,".$d["idcurso"].",\"".$d["poster"]."\",\"Cursos\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <br>
                <?php
                formHorariosLocales("curso");
                echo "<br>";
                formHorariosAuditorios("curso");
                echo "<br>";
                formHorariosCines("curso");
                echo "<br>";
                formHorariosMuseos("curso");
                echo "<br>";
                formHorariosLugares("curso");
                ?>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Curso creado con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Curso modificado con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformcurso&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformcurso">Crear uno nuevo</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>
    <script>
        lugares = <?php if(!empty($horarios["horasLugar"])) echo $horarios["horasLugar"]; else echo "[]"; ?>;
        locales = <?php if(!empty($horarios["horasLocal"])) echo $horarios["horasLocal"]; else echo "[]"; ?>;
        cines = <?php if(!empty($horarios["horasCine"])) echo $horarios["horasCine"]; else echo "[]"; ?>;
        auditorios = <?php if(!empty($horarios["horasAuditorio"])) echo $horarios["horasAuditorio"]; else echo "[]"; ?>;
        museos = <?php if(!empty($horarios["horasMuseo"])) echo $horarios["horasMuseo"]; else echo "[]"; ?>;
    </script>