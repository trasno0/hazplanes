<?php
/************************************************************************
 *          Libreria de funciones para manejo de usuarios
 *          Modificado: 06/11/2013
 ************************************************************************/

/*
 * Funcion que comprueba si los datos de login estan en la bd permitiendo acceso y creando una cookie si se ha marcado el campo recordar
 * Parametros:
 *      $email: correo del usuario. Hace las funciones de nombre de usuario.
 *      $pass: contraseña.
 *      $remember: (opcional) 0 = no recordar, 1 = recordar.
 * Salidas:
 *      true/false
 */
function login($email, $pass, $remember = 0) {
    global $db;

    if(!empty($email) && !empty($pass)) {
        $query = sprintf("select iduser, email from %susers where email = '%s' and password = '%s'", BDPREFIX, $db->secure_field($email), sha1($db->secure_field($pass)));
        if($r = $db->query($query)) {
            if($db->count($r) == 1) {
                $u = $db->fetch($r);
                $user_browser = $_SERVER['HTTP_USER_AGENT']; //Obtén el agente de usuario del usuario
                $user_id = preg_replace("/[^0-9]+/", "", $u["iduser"]); //protección XSS ya que podemos imprimir este valor
                $_SESSION['user_id'] = $user_id;
                $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $u["email"]); //protección XSS ya que podemos imprimir este valor
                $_SESSION['username'] = $username;
                if($remember == 1) {
                    setcookie("user", $_SESSION['username'], time()+60*60*24*7, "/"); //7 dias
                }
                return true;
            }
        } else {
            return false;
        }
    }
    return false;
}

/*
 * Funcion que devuelve todos los campos de un usuario
 * Parametros:
 *      $user: array de datos del usuario
 *      $user['idusers']: id del usuario
 * Salidas:
 *      db_fetch($r): array asociativo con todos los campos del usuario encontrado
 */
function userData($user) {
    global $conf;

    if(!empty($user["idusers"])) {
        $query = sprintf("select u.idusers, u.name, u.surname, u.email, u.avatar from %susers u where u.idusers = %d", $conf["bdprefix"], db_secure_field($user["idusers"]));
        $r = db_query($query);
        if(db_count($r) > 0) {
            return db_fetch($r);
        }
    }
    return false;
}

/*
 * Funcion que elimina un usuario
 * Parametros:
 *      $userid: id del usuario
 * Salidas:
 *      $r: true/false
 */
function deleteUser($idusers) {
    global $conf;

    if(!empty($idusers)) {
        $query = sprintf("select avatar from %susers where idusers = %d", $conf["bdprefix"], db_secure_field($idusers));
        $r = db_query($query);
        //Cogemos el nombre del avatar del usuario que se va a eliminar, para eliminar la imagen despues
        $avatarOld = substr(db_result($r,0),0,-4);
        $query = sprintf("delete from %susers where idusers = %d",$conf["bdprefix"], db_secure_field($idusers));
        $r = db_exec($query);
        if($r) {
            if($avatarOld != "no_avatar") {
                deleteImage($avatarOld);
            }
            return true;
        } else {
            return false;
        }
    }
    return false;
}

/*
 * Funcion que lista todos los usuarios con todos sus datos.
 * Parametros:
 *      $filter (opcional): cadena de filtro para nombre de usuario. El nombre de usuario tiene que contener esta cadena.
 *      $order (opcional): campo por el que ordenar el listado.
 * Salidas:
 *      $r: resulset con los usuarios devueltos por la consulta que contienen $filter en su nombre.
 */
function listUsers($filter = "", $order = "name") {
    global $conf;

    $query = sprintf("select c.* from %susers c where c.name like '%%%s%%' order by c.%s", $conf["bdprefix"], db_secure_field($filter), db_secure_field($order));
    $r = db_query($query);
    return $r;
}

/*
 * Funcion que inserta todos los campos de un usuario
 * Parametros:
 *      $user: array de datos del usuario
 *      $user['name']: nombre del usuario
 *      $user['surname']: apellidos del usuario
 *      $user['password']: contraseña del usuario
 *      $user['email']: email del usuario
 *      $user['avatar']: nombre temporal del fichero del avatar
 * Salidas:
 *      $r: true/false or 'exists'
 */
function addUser($user) {
    global $conf;

    $query = sprintf("select * from %susers where email = '%s'", $conf["bdprefix"], db_secure_field($user["email"]));
    $r = db_query($query);
    //Comprobamos si existe un usuario con ese mismo email
    if(db_count($r) == 0) {
        if($user["avatar"] != "no_avatar.jpg") {
            $u = str_replace(".","dot",str_replace("@", "at", $user['email']));
            $ext = substr($user["avatar"],-4);
            $avatar = $u.$ext;
        } else {
            $avatar = "no_avatar.jpg";
        }
        $query = sprintf("insert into %susers (name,surname,password,email,avatar) VALUES ('%s','%s','%s','%s','%s')", $conf["bdprefix"], db_secure_field($user["name"]), db_secure_field($user["surname"]), db_secure_field(md5($user["password"])), db_secure_field($user["email"]), db_secure_field($user["avatar"]));
        $r = db_exec($query);
        if($r) {
            if($user["avatar"] != "no_avatar.jpg") {
                rename(BASE_URI.$conf["path_temp"].$user["avatar"],BASE_URI.$conf["path_avatar"].$u.$ext);
                rename(BASE_URI.$conf["path_temp"]."thumbs/".$user["avatar"],BASE_URI.$conf["path_avatar"]."thumbs/".$u.$ext);
            }
            $id = db_last_id();
            return $id;
        } else {
            return false;
        }
    } else {
        return 'exists';
    }
}

/*
 * Funcion que actualiza todos los campos de un usuario
 * Parametros:
 *      $user: array de datos del usuario
 *      $user['idusers']: id del usuario
 *      $user['name']: nombre del usuario
 *      $user['password']: contraseña del usuario
 *      $user['email']: email del usuario
 * Salidas:
 *      $r: true/false
 */
function updateUser($user) {
    global $conf;

    $query = sprintf("select avatar, email from %susers where idusers = %d", $conf["bdprefix"], db_secure_field($user["idusers"]));
    $r = db_query($query);
    $old = db_fetch($r);
    $query = "update ".$conf["bdprefix"]."users set ";
    $coma = "";
    foreach($user as $key=>$value) {
        if($key != "idusers" && $key != "avatar") {
            $query .= $coma.$key."='".db_secure_field($value)."'";
            $coma = ",";
        }
    }
    if(!empty($user["avatar"])) {
        if(file_exists(BASE_URI.$conf["path_temp"].$user["avatar"])) {
            if($old["email"] != $user["email"]) {
                $u2d = str_replace(".","dot",str_replace("@", "at", $old["email"]));
            } else {
                $u2d = str_replace(".","dot",str_replace("@", "at", $user['email']));
            }
            deleteImage($u2d);
            $u = str_replace(".","dot",str_replace("@", "at", $user['email']));
            $ext = substr($user["avatar"],-4);
            if(file_exists(BASE_URI.$conf["path_temp"].$user["avatar"])) {
                rename(BASE_URI.$conf["path_temp"].$user["avatar"],BASE_URI.$conf["path_avatar"].$u.$ext);
            }
            if(file_exists(BASE_URI.$conf["path_temp"]."thumbs/".$user["avatar"])) {
                rename(BASE_URI.$conf["path_temp"]."thumbs/".$user["avatar"],BASE_URI.$conf["path_avatar"]."thumbs/".$u.$ext);
            }
            $query .= ", avatar='".$u.$ext."'";
        } elseif($old["email"] != $user["email"]) {
            if($old["avatar"] != "no_avatar.jpg") {
                $u = str_replace(".","dot",str_replace("@", "at", $user['email']));
                $ext = substr($old["avatar"],-4);
                if(file_exists(BASE_URI.$conf["path_avatar"].$old["avatar"])) {
                    rename(BASE_URI.$conf["path_avatar"].$old["avatar"],BASE_URI.$conf["path_avatar"].$u.$ext);
                }
                if(file_exists(BASE_URI.$conf["path_avatar"]."thumbs/".$old["avatar"])) {
                    rename(BASE_URI.$conf["path_avatar"]."thumbs/".$old["avatar"],BASE_URI.$conf["path_avatar"]."thumbs/".$u.$ext);
                }
                $query .= ", avatar='".$u.$ext."'";
            }
        } else {
            $query .= ", avatar='no_avatar.jpg'";
        }
    }
    $query .= " where idusers=".$user["idusers"];
    $r = db_exec($query);
    if($r) {
        return true;
    } else {
        return false;
    }
}

function deleteImage($u) {
    if(file_exists(BASE_URI."img/avatar/".$u.".jpg")) {
        unlink(BASE_URI."img/avatar/".$u.".jpg");
        unlink(BASE_URI."img/avatar/thumbs/".$u.".jpg");
    }
    if(file_exists(BASE_URI."img/avatar/".$u.".gif")) {
        unlink(BASE_URI."img/avatar/".$u.".gif");
        unlink(BASE_URI."img/avatar/thumbs/".$u.".gif");
    }
    if(file_exists(BASE_URI."img/avatar/".$u.".png")) {
        unlink(BASE_URI."img/avatar/".$u.".png");
        unlink(BASE_URI."img/avatar/thumbs/".$u.".png");
    }
}

?>
