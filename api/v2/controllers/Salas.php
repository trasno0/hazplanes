<?php
/**
 * Clase controlador de salas
 *
 * @package API
 * @author Trasno
 */
class Salas {
    private $params;
    private $salaItem;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->salaItem = new SalaItem($db);
    }
    
    /**
     * Destructor
     *
     * @return void
     * @author trasno 
     */
    public function __destruct() {
        $this->salaItem = null;
    }
    
    /**
     * Crear sala
     *
     * @return int
     */
    public function createAction() {
        $sala = array();
        foreach($this->params as $key => $value){
            $sala[$key] = $value; 
        }
        
        $result = $this->salaItem->addSala($sala);
        
        return $result;
    }
    
    /**
     * Recuperar sala(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idsala"]) && !empty($this->params["idsala"])) {
            $data = $this->salaItem->dataSala($this->params["idsala"]);
        } else {
            $data = $this->salaItem->listSalas($this->params["idcine"]);
        }
        
        return $data;
    }
    
    /**
     * Actualizar sala
     *
     * @return boolean
     */
    public function updateAction() {
        $sala = array();
        foreach($this->params as $key => $value){
            $sala[$key] = $value; 
        }
        
        $result = $this->salaItem->updateSala($sala);
        
        return $result;
    }
    
    /**
     * Eliminar sala
     *
     * @return boolean
     */
    public function deleteAction() {
        $result = $this->salaItem->deleteSala($this->params["id"]);
        
        return $result;
    }
}