<?php
pageAccessControl();
?>
    <section id='content' class='<?php echo $displayContent; ?>'>
        <div class="grid">
            <section id="main">
                <ul id="celdas">
                    <?php
                    $publi = 0;
                    $countCells = 0;
                    $lugares = array("locales","teatros","cines","museos","pabellones");
                    foreach($lugares as $l) {
                        $options = array(
                            "agenda" => 1
                            );
                        switch($l) {
                            case "locales":
                                $lugar = new Locales($options,$db);
                                $class = "concert";
                                $idlugar = "idlocal";
                                $link = "local/";
                                break;
                            case "teatros":
                                $lugar = new Teatros($options,$db);
                                $class = "theater";
                                $idlugar = "idteatro";
                                $link = "auditorio/";
                                break;
                            case "cines":
                                $lugar = new Cines($options,$db);
                                $class = "film";
                                $idlugar = "idcine";
                                $link = "cine/";
                                break;
                            case "museos":
                                $lugar = new Museos($options,$db);
                                $class = "expo";
                                $idlugar = "idmuseo";
                                $link = "museo/";
                                break;
                            case "pabellones":
                                $lugar = new Pabellones($options,$db);
                                $class = "sport";
                                $idlugar = "idpabellon";
                                $link = "pabellon/";
                                break;
                        }
                        $imgnull = "null-lugar.png";
                        $data = $lugar->readAction();

                        if(count($data) < 1) {
                            echo "<li class='".$display." noclick'>
                                <div class='grid-info no-events'>
                                    <br><br>No hay locales.
                                </div>
                            </li>";
                        } else {
                            foreach($data as $k => $d) {
                                if(isset($d["tipolocal"]) && !empty($d["tipolocal"])) {
                                    $type = str_replace("á", "Á", str_replace("é", "É", str_replace("í", "Í", str_replace("ó", "Ó", str_replace("ú", "Ú", str_replace("ñ", "Ñ", strtoupper($d["tipolocal"])))))));
                                } else {
                                    switch($l) {
                                        case "locales":
                                            $type = "LOCAL";
                                            break;
                                        case "teatros":
                                            $type = "TEATRO";
                                            break;
                                        case "cines":
                                            $type = "CINE";
                                            break;
                                        case "museos":
                                            $type = "MUSEO";
                                            break;
                                        case "pabellones":
                                            $type = "PABELLÓN";
                                            break;
                                    }
                                }
                                $countCells++;
                                if($countCells == 3) {
                                    echo $celdaPubli;
                                    $publi++;
                                }
                                $img = "";
                                if(!empty($d["imagen"])) {
                                    $img = $d["imagen"];
                                }
                                if(empty($img) || !file_exists($img)) {
                                    $img = "img/interface/".$imgnull;
                                }
                                echo "<li class='".$display." ".$class."' style='background: url(".$img.") no-repeat center'>";
                                $infostatus = "";
                                echo "        <div class='grid-info ".$infostatus."'>
                                            <h2><a href='".$link.$d[$idlugar]."-".urlAmigable($d["nombre"])."' class='nombre'>".$d["nombre"]."</a></h2>
                                            <br>
                                            <span class='genre'>".$d["direccion"]."</span>
                                            <br>
                                            <span class='type'>".$type."</span>";
                                echo "  </div>";
                                echo "<span class='score'>";
                                            $stars = ceil(!empty($d["numvotos"])?$d["puntos"]/$d["numvotos"]:0);
                                            for($a = 0; $a < $stars; $a++) {
                                                echo '<img src="img/interface/starF-24.png" alt="" />';
                                            }
                                            for($b = 5; $b > $a; $b--) {
                                                echo '<img src="img/interface/starE-24.png" alt="" />';
                                            }
                                            echo " (<span id='numvotos'>".(!empty($d["numvotos"])?$d["numvotos"]:0)."</span>)";
                                echo "        </span>";
                                echo "</li>";
                            }
                        }
                    }
                    if($publi < 2) {
                        echo $celdaPubli;
                        if($countCells < 2) {
                            echo $celdaPubli;
                        }
                    }
                    ?>
                </ul>
            </section>
        </div>
    </section>