<?php
pageAccessControl();
?>
    <section id='content'>
        <div class="grid">
            <section id="info" style='text-align:center; height: 1500px;'>
                <div class='page-flat'>
                    <h2>Política de cookies</h2>
                    <p>En la web de HazPlanes se utilizan cookies para facilitar la relación de los visitantes con nuestro contenido, permitir elaborar estadísticas sobre las visitas que recibimos y mostrar contenido relacionado con las preferencias mediante el análisis de los hábitos de navegación. En cumplimiento de la Directiva 2009/136/CE, desarrollada en nuestro ordenamiento por el apartado segundo del artículo 22 de la Ley de Servicios de Sociedad de la Información, siguiendo las directrices de la Agencia Española de Protección de Datos, procedemos a informarle detalladamente del uso que se realiza en nuestra web.</p>
                    <p>Se denominan cookies a unos pequeños archivos que se graban en el navegador utilizado por cada visitante de nuestra web para que el servidor pueda recordar la visita de ese usuario con posterioridad cuando vuelva a acceder a nuestros contenidos. Esta información no revela su identidad, ni dato personal alguno, ni accede al contenido almacenado en su PC, pero sí que permite a nuestro sistema identificarle a usted como un usuario determinado que ya visitó la web con anterioridad, visualizó determinadas páginas, etc. Y además permite guardar sus preferencias personales e información técnica como por ejemplo las visitas realizadas o páginas concretas que visite.</p>
                    <p>Si usted no desea que se guarden cookies en su navegador o prefiere recibir una información cada vez que una cookie solicite instalarse, puede configurar sus opciones de navegación para que se haga de esa forma. La mayor parte de los navegadores permiten la gestión de las cookies de 3 formas diferentes:</p>
                    <ul>
                        <li>Las cookies son siempre rechazadas</li>
                        <li>El navegador pregunta si el usuario desea instalar cada cookie</li>
                        <li>Las cookies son siempre aceptadas</li>
                    </ul>
                    <p>Su navegador también puede incluir la posibilidad de seleccionar con detalle las cookies que desea que se instalen en su ordenador. En concreto, el usuario puede normalmente aceptar alguna de las siguientes opciones:</p>
                    <ul>
                        <li>Rechazar las cookies de determinados dominios;</li>
                        <li>Rechazar las cookies de terceros;</li>
                        <li>Aceptar cookies como no persistentes (se eliminan cuando el navegador se cierra);</li>
                        <li>Permitir al servidor crear cookies para un dominio diferente.</li>
                    </ul>
                    <p>Puede encontrar información sobre cómo configurar los navegadores más usados en las siguientes ubicaciones:</p>
                    <ul>
                        <li><a href='http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647' target='_blank'>Chrome</a></li>
                        <li><a href='http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-ininternet-explorer-9' target='_blank'>Explorer</a></li>
                        <li><a href='http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we' target='_blank'>Firefox</a></li>
                        <li><a href='http://support.apple.com/kb/ph5042' target='_blank'>Safari</a></li>
                    </ul>
                    <p>Respecto de las cookies de terceros, es decir aquellas que son ajenas a nuestro sitio web, no podemos hacernos responsables del contenido y veracidad de las políticas de privacidad que ellos incluyen por lo que la información que le ofrecemos es siempre con referencia a la fuente. En caso de dudas diríjase al webmaster del dominio creador de la cookie.</p>
                    <p>A continuación le informamos detalladamente de las cookies que pueden instalarse desde nuestro sitio. En función de su navegación podrán instalarse todas o sólo algunas de ellas.</p>
                    <p>Datos personales recogidos para las siguientes finalidades y utilizando los siguientes servicios:</p>
                    
                    <h3>Google Analytics</h3>
                    <p>Google Analytics es un servicio de análisis web prestado por Google que utiliza los datos recogidos para rastrear y examinar el uso de esta web, para preparar informes de sus actividades y compartirlos con otros servicios de Google.</p>
                    <p>Google también puede utilizar los datos recogidos para contextualizar y personalizar los anuncios de su propia red de publicidad.
    Datos personales recogidos: Cookie y datos de uso.</p>
                    <p>Lugar de tratamiento: EE. UU. | <a href='http://www.google.com/intl/es/policies/privacy/' target='_blank'>Política de privacidad</a> | <a href='http://tools.google.com/dlpage/gaoptout?hl=es' target='_blank'>Opt out</a></p>
    
                    <h3>Google AdSense</h3>
                    <p>Google AdSense es un servicio de publicidad prestado por Google que utiliza la Cookie “Doubleclick”, que rastrea el uso de esta web y el comportamiento del usuario en relación con los anuncios publicitarios, productos y servicios ofertados.</p>
                    <p>Datos Personales recogidos: Cookie y datos de uso.</p>
                    <p>Lugar de tratamiento: EE.UU. – <a href='http://www.google.com/privacy/ads/' target='_blank'>Política de privacidad</a> | <a href='http://google.com/settings/ads/onweb/optout?hl=es' target='_blank'>Opt out</a></p>
    
                    <h3>Botón +1 y widgets sociales de Google+</h3>
                    <p>El botón +1 y los widgets sociales de Google+ son servicios de interacción con la red social Google+.</p>
                    <p>Datos Personales recogidos: Cookie y datos de uso.</p>
                    <p>Lugar de tratamiento: EE.UU. | <a href='http://www.google.com/intl/en/policies/privacy/' target='_blank'>Política de privacidad</a></p>
    
                    <h3>Facebook Social Plugins (botón “Me gusta” y otros widgets de Facebook)</h3>
                    <p>El botón “Me gusta” y los widgets sociales de Facebook son servicios de interacción con la red social Facebook. Estos servicios permiten interactuar con Facebook directamente desde las páginas de esta página web.</p>
                    <p>Las interacciones y la información obtenida por esta web siempre estarán sometidas a la configuración de privacidad del usuario en Facebook.</p>
                    <p>Es posible que aunque los usuarios no utilicen el servicio, éste recoja datos de tráfico web relativos a las páginas en las que estén instalados.</p>
                    <p>Datos personales recogidos: Cookie y datos de uso.</p>
                    <p>Lugar de tratamiento: EE. UU. | <a href='http://www.facebook.com/privacy/explanation.php' target='_blank'>Política de privacidad</a></p>
    
                    <h3>Botón Tweet y widgets sociales de Twitter</h3>
                    <p>El botón Tweet y los widgets sociales de Twitter son servicios de interacción con la red social Twitter.</p>
                    <p>Datos Personales recogidos: Cookie y datos de uso.</p>
                    <p>Lugar de tratamiento: EE.UU. | <a href='http://twitter.com/privacy' target='_blank'>Política de privacidad</a></p>
                </div>
            </section>
        </div>
    </section>