var map;
var marker;
var field = "";
var bound = new google.maps.LatLngBounds();
var boundPublic = new google.maps.LatLngBounds();

function initialize(zoom) {
	if(typeof zoom == 'undefined') {
		zoom = 11;
	}
	var pos;
	var myLatlng = new google.maps.LatLng(42.2181, -8.725);
	var myOptions = {
		zoom: zoom,
		center: myLatlng,
		disableDefaultUI: true,
		scaleControl: true,
		mapTypeControl: true,
		navigationControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	google.maps.event.addListener(map, 'click', function(event) {
		if(marker!=null)
			marker.setMap(null);
		marker = new google.maps.Marker({
			position: event.latLng,
			map: map,
			draggable: true
		});
		marker.setMap(map);
		google.maps.event.addListener(marker, 'drag', function() {
			$("#puntosPoly").html('<div id="punto">'+marker.getPosition()+'</div>');
		});
		google.maps.event.addListener(marker, 'dragend', function() {
			$("#puntosPoly").html('<div id="punto">'+marker.getPosition()+'</div>');
			$("#coordenadastemp").val(marker.getPosition());
		});
		$("#puntosPoly").html('<div id="punto">'+event.latLng+'</div>');
		$("#coordenadastemp").val(marker.getPosition());
	});

	// Create the search box and link it to the UI element.
	var input = /** @type {HTMLInputElement} */(
  		document.getElementById('pac-input'));
  		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	var searchBox = new google.maps.places.SearchBox(
		/** @type {HTMLInputElement} */(input));

	// Listen for the event fired when the user selects an item from the
	// pick list. Retrieve the matching places for that item.
	google.maps.event.addListener(searchBox, 'places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}
		if(marker!=null)
			marker.setMap(null);
		marker = new google.maps.Marker({
			position: places[0].geometry.location,
			map: map,
			draggable: true
		});
		marker.setMap(map);
		google.maps.event.addListener(marker, 'drag', function() {
			$("#puntosPoly").html('<div id="punto">'+marker.getPosition()+'</div>');
		});
		google.maps.event.addListener(marker, 'dragend', function() {
			$("#puntosPoly").html('<div id="punto">'+marker.getPosition()+'</div>');
			$("#coordenadastemp").val(marker.getPosition());
		});
		$("#puntosPoly").html('<div id="punto">'+places[0].geometry.location+'</div>');
		$("#coordenadastemp").val(marker.getPosition());
		var bounds = new google.maps.LatLngBounds();
		bounds.extend(places[0].geometry.location);
		map.fitBounds(bounds);
		google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
			if (this.getZoom() > 17) {
		    	this.setZoom(17);
			}
		});
	});
}

function initView(lat,lng) {
	var myOptions = {
		zoom: 16,
		disableDefaultUI: true,
		scaleControl: true,
		mapTypeControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	var pos = new google.maps.LatLng(lat,lng);
	var marker = new google.maps.Marker({
		position: pos,
		map: map,
		draggable: false,
	});
	marker.setMap(map);
	map.setCenter(marker.getPosition());
}

function initViewClear() {
	var myLatlng = new google.maps.LatLng(42.2181, -8.725);
	var myOptions = {
		zoom: 11,
		center: myLatlng,
		disableDefaultUI: true,
		scaleControl: true,
		mapTypeControl: true,
		navigationControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
		if (this.getZoom() > 16) {
	    	this.setZoom(16);
		}
	});
}

function setPoints(lat,lng) {
	var pos = new google.maps.LatLng(lat,lng);
	marker = new google.maps.Marker({
		position: pos,
		map: map,
		draggable: true
	});
	marker.setMap(map);
	map.setCenter(marker.getPosition());
	google.maps.event.addListener(marker, 'drag', function() {
		$("#puntosPoly").html('<div id="punto">'+marker.getPosition()+'</div>');
	});
	google.maps.event.addListener(marker, 'dragend', function() {
		$("#puntosPoly").html('<div id="punto">'+marker.getPosition()+'</div>');
		$("#coordenadastemp").val(marker.getPosition());
	});
}

function setLugarPoints(lat,lng,index) {
	var pos = new google.maps.LatLng(lat,lng);
	var iconBase = 'https://maps.google.com/mapfiles/kml/paddle/';
	if(typeof index == 'undefined') {
		index = "red-circle";
	}
	var image = {
    	url: iconBase + index + '.png',
    	// This marker is 20 pixels wide by 32 pixels tall.
    	scaledSize: new google.maps.Size(45, 45),
  	};
	marker = new google.maps.Marker({
		position: pos,
		map: map,
		icon: image,
		draggable: false
	});
	marker.setMap(map);
	bound.extend(pos);
	map.fitBounds(bound);
}

function publicInitialize(zoom) {
	if(typeof zoom == 'undefined') {
		zoom = 11;
	}
	var pos;
	var myLatlng = new google.maps.LatLng(42.2181, -8.725);
	var myOptions = {
		zoom: zoom,
		center: myLatlng,
		disableDefaultUI: true,
		scaleControl: true,
		mapTypeControl: true,
		navigationControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	google.maps.event.addListener(map, 'click', function(event) {
		infowindow.close();
	});
}

var infowindow;
var iconBase = 'img/interface/';
var icons = {
  PELICULA: {
    icon: iconBase + 'marker-pelicula.png'
  },
  CONCIERTO: {
    icon: iconBase + 'marker-concierto.png'
  },
  TEATRO: {
    icon: iconBase + 'marker-teatro.png'
  },
  EVENTO: {
    icon: iconBase + 'marker-evento.png'
  },
  EXPOSICIÓN: {
    icon: iconBase + 'marker-exposicion.png'
  },
  DEPORTE: {
    icon: iconBase + 'marker-deporte.png'
  },
  FORMACIÓN: {
    icon: iconBase + 'marker-formacion.png'
  }
};

function setPublicPoints(lat,lng,type,title,info,status) {
	var pos = new google.maps.LatLng(lat,lng);
	// Origins, anchor positions and coordinates of the marker
  	// increase in the X direction to the right and in
  	// the Y direction down.
  	var image = {
    	url: icons[type].icon,
    	// This marker is 20 pixels wide by 32 pixels tall.
    	size: new google.maps.Size(24, 30),
    	// The origin for this image is 0,0.
    	origin: new google.maps.Point(0,0),
    	// The anchor for this image is the base of the flagpole at 0,32.
    	anchor: new google.maps.Point(11, 30)
  	};
	marker = new google.maps.Marker({
		position: pos,
		map: map,
		icon: image,
		title: type + ": " + title,
	});
	google.maps.event.addListener(marker, 'click', (function(marker,info){
        return function() {
        	if(infowindow) infowindow.close();
        	infowindow = new google.maps.InfoWindow({ content: info });
           	infowindow.open(map,marker);
        };
    })(marker,info));
	marker.setMap(map);
	boundPublic.extend(pos);
	map.fitBounds(boundPublic);
}
