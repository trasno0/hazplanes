<?php
pageAccessControl(1);
?>
    <section id='content'>
        <div class="grid">
            <section id='datos'>
                <div class="header-list">
                    <h2>Tipos de deportes <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformtipodeporte'">Nuevo tipo</button></span></h2>
                    <div id="listOptions">
                        <button type="button" c='Tipos_Deporte' id='delete'>Eliminar</button>
                    </div>
                </div>
                <table id="list" class="tabla">
                    <thead>
                        <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                        <th>Nombre</th>
                        <th>Poster</th>
                    </thead>
                    <?php
                    $options = array("read" => "tiposdeporte");
                    $deportes = new Deportes($options,$db);
                    $data = $deportes->readAction();
                    foreach($data as $k => $d) {
                        echo "<tr class='row'>
                            <td><input type='checkbox' name='checkListItem' id='".$d["idtipodeporte"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                            <td><a href='index.php?p=adminformtipodeporte&i=".$d["idtipodeporte"]."'>".$d["nombre"]."</a></td>
                            <td><img src='img/deportes/tipos/".$d["poster"]."' width='50'></td>
                        </tr>";
                    }
                    ?>
                </table>
            </section>
        </div>
    </section>
