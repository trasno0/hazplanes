<?php
/*
 * Scraper de yelmo cines
 * Modificado el 27-10-2014
 */

include_once("../includes/api.inc.php");
include_once("../includes/scrapers.inc.php");

$opts = array('http'=>array('header' => "User-Agent:Mozilla/5.0\r\n"));
$context = stream_context_create($opts);

date_default_timezone_set("Europe/Madrid");

//Yelmo Cines Estrenos
echo "<h1>Yelmo Cines</h1>";
$url = "http://www.yelmocines.es/billboard/upcoming";
$output = file_get_contents_curl($url);
//$output = file_get_contents($url,false,$context);

preg_match_all('/a title=\"(.*?) - Estreno el (.*?)\" href=/s', $output, $estrenos);

$json_total = array();
$count = 0;
$tot = count($estrenos[1]);
echo "<pre>";
//print_r($pelis);
foreach($estrenos[1] as $k => $peli) {
    if(!existPelicula($peli)) {
        $mesesShort = array("ene" => "01","feb" => "02","mar" => "03","abr" => "04","may" => "05","jun" => "06","jul" => "07","ago" => "08","sep" => "09","oct" => "10","nov" => "11","dic" => "12",);
        $f = explode(" ",$estrenos[2][$k]);
        $fecha = $f[0]."-".$mesesShort[substr($f[1],0,3)]."-".$f[2];
        $json_total[] = array(
                "nombre" => $peli,
                "fecha" => $fecha
            );
        $count++;
    }
}

$fp = fopen('../temp/estrenos.json', 'w');
fwrite($fp, json_encode($json_total));
fclose($fp);

echo "Encontrados ".$tot.", cogidos ".$count." eventos";
?>
<h1>Terminado!</h1>