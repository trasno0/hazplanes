<?php
/**
 * Clase controlador de cines
 *
 * @package API
 * @author Trasno
 */
class Cines {
    private $params;
    private $general;
    private $cineItem;
    private $serviciosItem;
    private $salaItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->cineItem = new CineItem($db);
        $this->serviciosItem = new ServiciosItem($db);
        $this->salaItem = new SalaItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->cineItem = null;
    }

    /**
     * Crear cine
     *
     * @return int
     */
    public function createAction() {
        $cine = array();
        foreach($this->params as $key => $value){
            $cine[$key] = $value;
        }

        //Cogemos los datos de los servicios para utilizarlos una vez creado el cine
        if(isset($cine["servicios"])) {
            $servicios = $cine["servicios"];
            foreach($servicios as $k => $s) {
                $servicios[$k] = 1;
            }
        } else {
            $servicios = array();
        }

        //Cogemos los datos de las tarifas
        if(isset($cine["tarifas"])) {
            $tarifas = $cine["tarifas"];
        } else {
            $tarifas = array();
        }

        //Cogemos los datos de las salas para utilizarlos una vez creado el cine
        if(isset($cine["sala"])) {
            $salas = $cine["sala"];
        } else {
            $salas = array();
        }

        //Cogemos los datos de las promociones para utilizarlos una vez creado el cine
        if(isset($cine["promos"])) {
            $promos = $cine["promos"];
        } else {
            $promos = array();
        }

        //Cogemos los datos de las entradas para utilizarlos una vez creado el cine
        if(isset($cine["entradas"])) {
            $entradas = $cine["entradas"];
        } else {
            $entradas = array();
        }

        unset($cine["servicios"],$cine["tarifas"],$cine["sala"],$cine["promos"],$cine["entradas"]);
        $cine["publicado"] = array_key_exists("publicado",$cine)? 1:0;

        //Creamos el cine
        $result = $this->cineItem->addCine($cine);

        if($result) {
            //Añadir servicios al cine
            $servicios["idcine"] = $result;
            $this->serviciosItem->addServicios($servicios);
            //Añadir por lo menos una sala al cine
            foreach($salas as $k => $sala) {
                $sala["idcine"] = $result;
                $this->salaItem->addSala($sala);
            }
            //Añadir tarifas
            $this->cineItem->setTarifas($result, $tarifas);
            //Añadir promociones
            foreach($promos as $k => $promo) {
                $this->cineItem->setPromociones($result, $promo);
            }
            //Añadir entradas
            foreach($entradas as $k => $entrada) {
                $this->cineItem->setEntradas($result, $entrada);
            }
        }

        return $result;
    }

    /**
     * Recuperar cine(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idcine"]) && !empty($this->params["idcine"])) {
            $data = $this->cineItem->dataCine($this->params["idcine"]);
        } else {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "",
                "order" => "nombre",
                );
            if(!empty($this->params)) {
                $options = array_merge($options,$this->params);
            }
            $data = $this->cineItem->listCines($options);
        }

        return $data;
    }

    /**
     * Actualizar cine
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->cineItem->publishCine($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->cineItem->unpublishCine($this->params["ids"]);
            } else {
                $result = false;
            }
        } else {
            $cine = array();
            foreach($this->params as $key => $value){
                $cine[$key] = $value;
            }

            //Servicios
            if(isset($cine["servicios"])) {
                $servicios = $cine["servicios"];
                foreach($servicios as $k => $s) {
                    $servicios[$k] = 1;
                }
            } else {
                $servicios = array();
            }
            $servicios["idcine"] = $cine["idcine"];
            $this->serviciosItem->updateServicios($servicios);

            //Cogemos los datos de las tarifas
            if(isset($cine["tarifas"])) {
                $tarifas = $cine["tarifas"];
            } else {
                $tarifas = array();
            }
            $this->cineItem->setTarifas($cine["idcine"], $tarifas);

            //Salas
            $bdsalas = $this->salaItem->listSalas($cine["idcine"]);
            if(isset($cine["sala"])) {
                $salas = $cine["sala"];
            } else {
                $salas = array();
            }
            foreach($salas as $k => $sala) {
                $salas[$k]["idcine"] = $cine["idcine"];
                if(!array_key_exists("numerada",$salas[$k])) {
                    $salas[$k]["numerada"] = 0;
                } else {
                    $salas[$k]["numerada"] = 1;
                }
                $existe = false;
                foreach($bdsalas as $key => $value) {
                    if($value["idsala"] == $sala["idsala"]) {
                        $existe = true;
                        unset($bdsalas[$key]);
                        break;
                    }
                }
                if($existe) {
                    $this->salaItem->updateSala($salas[$k]);
                } else {
                    $this->salaItem->addSala($salas[$k]);
                }
            }
            foreach($bdsalas as $s) {
                $this->salaItem->deleteSala($s["idsala"]);
            }

            //Entradas
            $bdRelCineEntradas = $this->cineItem->getEntradas($cine["idcine"]);
            if(isset($cine["entradas"])) {
                $entradas = $cine["entradas"];
            } else {
                $entradas = array();
            }
            foreach($entradas as $k => $entrada) {
                $existe = false;
                foreach($bdRelCineEntradas as $key => $value) {
                    if($value["identradas"] == $entrada) {
                        $existe = true;
                        unset($bdRelCineEntradas[$key]);
                        break;
                    }
                }
                if(!$existe) {
                    $this->cineItem->setEntradas($cine["idcine"], $entrada);
                }
            }
            foreach($bdRelCineEntradas as $s) {
                $this->cineItem->delEntradas($cine["idcine"],$s["identradas"]);
            }

            //Promociones
            $bdRelCinePromo = $this->cineItem->getPromociones($cine["idcine"]);
            if(isset($cine["promos"])) {
                $promos = $cine["promos"];
            } else {
                $promos = array();
            }
            foreach($promos as $k => $promo) {
                $existe = false;
                foreach($bdRelCinePromo as $key => $value) {
                    if($value["idpromocion"] == $promo) {
                        $existe = true;
                        unset($bdRelCinePromo[$key]);
                        break;
                    }
                }
                if(!$existe) {
                    $this->cineItem->setPromociones($cine["idcine"], $promo);
                }
            }
            foreach($bdRelCinePromo as $s) {
                $this->cineItem->delPromociones($cine["idcine"],$s["idpromocion"]);
            }

            unset($cine["servicios"], $cine["tarifas"],$cine["sala"],$cine["promos"],$cine["entradas"],$bdsalas,$salas,$bdRelCinePromo,$promos,$bdRelCineEntradas,$entradas);
            $cine["publicado"] = array_key_exists("publicado",$cine)? 1:0;

            //Cine
            $result = $this->cineItem->updateCine($cine);
        }

        return $result;
    }

    /**
     * Eliminar cine
     *
     * @return boolean
     *
     * TODO: eliminar peliculas del cine a eliminar
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->serviciosItem->deleteServicios($this->params["id"]);
            if($result) {
                $result = $this->salaItem->delSalasCine($this->params["id"]);
                if($result) {
                    $result = $this->cineItem->delEntradas($this->params["id"]);
                    if($result) {
                        $result = $this->cineItem->delPromociones($this->params["id"]);
                        if($result) {
                            $result = $this->cineItem->deleteCine($this->params["id"]);
                        }
                    }
                }
            }
        }

        return $result;
    }
}