<?php
pageAccessControl();

try {
    $salas = array();
    $entradas = array();
    $promos = array();

    $diaespectador = array(
        "Ninguno" => "",
        "Lunes" => "",
        "Martes" => "",
        "Miércoles" => "",
        "Jueves" => "",
        "Viernes" => "",
        "Sábado" => "",
        "Domingo" => "",
        );
    $diaespectador[$d["diaespectador"]] = "selected";
    $sala = new Salas($options,$db);
    $s = $sala->readAction();
    $salas = array();
    foreach($s as $k => $sala) {
        $salas[] = array("numero" => $sala["numero"], "capacidad" => $sala["capacidad"], "filas" => $sala["filas"], "numerada" => $sala["numerada"]);
    }

    $entrada = new Entradas($options,$db);
    $entradas = $entrada->readAction();

    $promo = new Promociones($options,$db);
    $promos = $promo->readAction();

    $serv = new Servicios($options,$db);
    $servicios = $serv->readAction();
    $servicios = array_change_key_case($servicios[0], CASE_LOWER);
    foreach($servicios as $key => $value) {
        if($value == 0) {
            $servicios[$key] = "";
        } elseif($value == 1) {
            $servicios[$key] = "checked";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}

if(!empty($d["coordenadas"])) {
    $d["coordenadas"] = trim($d["coordenadas"],"\(\)");
    $coord = explode(",",$d["coordenadas"]);
} else {
    $coord = array("","");
}
?>
    <script>
        $(document).ready(function() {
            initView('<?php echo $coord[0]; ?>','<?php echo $coord[1]; ?>');
        });
    </script>
    <section id='content'>
        <div class="grid">
            <section id='info2'>
                <h2>
                    <?php echo $d["nombre"]; ?>
                    <span class="score right">
                        <?php
                        $stars = ceil(!empty($d["numvotos"])?$d["puntos"]/$d["numvotos"]:0);
                        for($a = 0; $a < $stars; $a++) {
                            echo '<img src="img/interface/starF-24.png" alt="" />';
                        }
                        for($b = 5; $b > $a; $b--) {
                            echo '<img src="img/interface/starE-24.png" alt="" />';
                        }
                        echo " (<span id='numvotos'>".(!empty($d["numvotos"])?$d["numvotos"]:0)."</span>)";
                        ?>
                    </span>
                </h2>
                <div id="info-izq">
                    <?php
                    $ampliar = "";
                    if(!empty($d["imagen"]) && file_exists($d["imagen"])) {
                        $ampliar = "<div class='ampliar'>+</div>";
                        echo "<div id='poster'>";
                        echo $ampliar;
                        echo "  <img src='".$d["imagen"]."' id='posterimg' itemprop='photo' alt='Foto del cine'/>";
                        echo "</div>";
                    } else {
                        $d["imagen"] = "img/interface/null-lugar.png";
                        echo "<div id='poster'>";
                        echo "  <img src='".$d["imagen"]."' id='posterimg' itemprop='photo' alt='Sin foto'/>";
                        echo "</div>";
                    }
                    ?>
                    <br>
                    <div id="map_canvas"></div>
                    <span class="nota">
                        <label for='form-coordenadas'>Coordenadas (Lat,Long)</label><br>
                        <div id="puntosPoly"><?php echo "(".$d["coordenadas"].")"; ?></div>
                    </span>
                    <h3>Puntuar sitio</h3>
                    <span id="vote" class="score">
                        <span style="display:none;" id="i"><?php echo $id; ?>-cines</span>
                        <?php
                        for($a = 1; $a < 6; $a++) {
                            echo '<img src="img/interface/starE-24.png" data-value="'.$a.'" alt="Votar con '.$a.' estrellas" />';
                        }
                        ?>
                    </span>
                    <?php if(!empty($d["menciones"])) { ?>
                        <h3>Créditos / Agradecimientos</h3>
                        <div id='info-credits'>
                            <p style='padding: 0 10px 0;'>
                                <?php echo htmlspecialchars_decode ($d["menciones"]); ?>
                            </p>
                        </div>
                    <?php
                    }
                    echo $publiFicha;
                    ?>
                    <span id='last-update'>
                        Última actualización:
                        <?php
                        if(!empty($d["actualizado"])){
                            echo $d["actualizado"];
                        } else {
                            echo $d["creado"];
                        }
                        ?>
                    </span>
                </div>
                <div id="info-der">
                    <h3>Ficha del cine <span class="ocultar">Ocultar</span></h3>
                    <div id='info-ficha'>
                        <?php
                        $direccion = "";
                        if(!empty($d["direccion"])) {
                            $direccion .= $d["direccion"];
                        }
                        if(!empty($d["codigopostal"])) {
                            if(!empty($direccion)) {
                                $direccion .= " - ";
                            }
                            $direccion .= $d["codigopostal"];
                        }
                        if(!empty($d["localidad"])) {
                            if(!empty($direccion)) {
                                if(!empty($d["codigopostal"])) {
                                    $direccion .= " ";
                                } else {
                                    $direccion .= " - ";
                                }
                            }
                            $direccion .= $d["localidad"];
                        }
                        if(!empty($d["provincia"])) {
                            if(!empty($direccion)) {
                                $direccion .= " (";
                            }
                            $direccion .= $d["provincia"].")";
                        }
                        if(!empty($direccion)) {
                            echo "<div id='direccion'><img src='img/interface/directions-24.png' alt='Dirección'>".$direccion."</div>";
                        }
                        if(!empty($d["telefono"])) {
                            echo "<div id='telefono'><img src='img/interface/phone-24.png' alt='Teléfono'>".$d["telefono"]."</div>";
                        }
                        if(!empty($d["email"])) {
                            echo "<div id='email'><img src='img/interface/email-26.png' alt='Email'>".$d["email"]."</div>";
                        }
                        if(!empty($d["web"])) {
                            echo "<div id='web'><img src='img/interface/link-24.png' alt='Web'><a href='".$d["web"]."' target='_blank'>".$d["web"]."</a></div>";
                        }
                        if(!empty($d["twitter"])) {
                            echo "<div id='twitter'><img src='img/interface/twitter-24.png' alt='Twitter'><a href='".$d["twitter"]."' target='_blank'>".$d["twitter"]."</a></div>";
                        }
                        if(!empty($d["facebook"])) {
                            echo "<div id='facebook'><img src='img/interface/facebook-24.png' alt='Facebook'><a href='".$d["facebook"]."' target='_blank'>".$d["facebook"]."</a></div>";
                        }
                        if(!empty($d["google"])) {
                            echo "<div id='google'><img src='img/interface/google_plus-24.png' alt='Google+'><a href='".$d["google"]."' target='_blank'>".$d["google"]."</a></div>";
                        }
                        if(!empty($d["otrasocial"])) {
                            echo "<div id='otrasocial'><img src='img/interface/link-24.png' alt='Otra red social'><a href='".$d["otrasocial"]."' target='_blank'>".$d["otrasocial"]."</a></div>";
                        }
                        if(!empty($d["diadelespectador"])) {
                            echo "<div id='diaespectador'><img src='img/interface/ticket-26.png' alt='Día del espectador'>Día del espectador: ";
                            echo $d["diadelespectador"]."</div>";
                        }
                        if(!empty($d["notas"])) {
                            echo "<h3>Notas</h3><div id='notas'><p>";
                            echo $d["notas"]."</p></div>";
                        }
                        if(!empty($d["descripcion"])) {
                            echo "<h3>Descripción</h3><div id='descripcion'><p>";
                            echo $d["descripcion"]."</p></div>";
                        }
                        ?>
                    </div>
                    <h3>Servicios <span class="ocultar">Ocultar</span></h3>
                    <div>
                        <table class="tabla">
                            <tr>
                                <th>Minusválidos</th>
                                <th>HD</th>
                                <th>3D</th>
                                <th>Parking</th>
                                <th>Carnet joven</th>
                                <th>Carnet estudiante</th>
                            </tr>
                            <tr style='text-align: center;'>
                                <td>
                                    <input type="checkbox" name="servicios[minusvalidos]" <?php echo $servicios["minusvalidos"]; ?> disabled/>
                                </td>
                                <td>
                                    <input type="checkbox" name="servicios[hd]" <?php echo $servicios["hd"]; ?> disabled/>
                                </td>
                                <td>
                                    <input type="checkbox" name="servicios[3d]" <?php echo $servicios["3d"]; ?> disabled/>
                                </td>
                                <td>
                                    <input type="checkbox" name="servicios[parking]" <?php echo $servicios["parking"]; ?> disabled/>
                                </td>
                                <td>
                                    <input type="checkbox" name="servicios[carnetjoven]" <?php echo $servicios["carnetjoven"]; ?> disabled />
                                </td>
                                <td>
                                    <input type="checkbox" name="servicios[carnetestudiante]" <?php echo $servicios["carnetestudiante"]; ?> disabled/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <h3>Tarifas <span class="ocultar">Ocultar</span></h3>
                    <div>
                        <table class="tabla">
                            <tr>
                                <td>Normal</td>
                                <td><?php echo !empty($d["normal"])? $d["normal"]." €":"--"; ?></td>
                                <td>Normal 3D</td>
                                <td><?php echo !empty($d["normal3d"])? $d["normal3d"]." €":"--"; ?></td>
                            </tr>
                            <tr>
                                <td>Reducida</td>
                                <td><?php echo !empty($d["reducida"])? $d["reducida"]." €":"--"; ?></td>
                                <td>Reducida 3D</td>
                                <td><?php echo !empty($d["reducida3d"])? $d["reducida3d"]." €":"--"; ?></td>
                            </tr>
                            <tr>
                                <td>Día del espectador <?php echo empty($d["diadelespectador"])?"":"(".$d["diadelespectador"].")"; ?></td>
                                <td><?php echo !empty($d["diaespectador"])? $d["diaespectador"]." €":"--"; ?></td>
                                <td>Día del espectador 3D <?php echo empty($d["diadelespectador"])?"":"(".$d["diadelespectador"].")"; ?></td>
                                <td><?php echo !empty($d["diaespectador3d"])? $d["diaespectador3d"]." €":"--"; ?></td>
                            </tr>
                            <tr>
                                <td>Laborables</td>
                                <td><?php echo !empty($d["laborables"])? $d["laborables"]." €":"--"; ?></td>
                                <td>Fin de semana y festivos</td>
                                <td><?php echo !empty($d["findesyfestivos"])? $d["findesyfestivos"]." €":"--"; ?></td>
                            </tr>
                            <tr>
                                <th colspan='4'>Extras</th>
                            </tr>
                            <tr>
                                <td>Gafas</td>
                                <td><?php echo !empty($d["gafas"])? $d["gafas"]." €":"--"; ?></td>
                                <td>Butaca especial</td>
                                <td><?php echo !empty($d["butacaespecial"])? $d["butacaespecial"]." €":"--"; ?></td>
                            </tr>
                        </table>
                    </div>
                    <h3>Información de interés <span class="ocultar">Ocultar</span></h3>
                    <div>
                        <br>
                        <fieldset>
                            <legend>Salas</legend>
                            <div>
                                <p>
                                    <table class='tabla'>
                                        <tr>
                                            <th>Número</th>
                                            <th>Capacidad</th>
                                            <th>Filas</th>
                                            <th>Numerada</th>
                                        </tr>
                                    <?php
                                    foreach($salas as $sala) {
                                        ?>
                                        <tr>
                                            <td><?php echo $sala["numero"]; ?></td>
                                            <td><?php echo empty($sala["capacidad"])?"--":$sala["capacidad"]; ?></td>
                                            <td><?php echo empty($sala["filas"])?"--":$sala["filas"]; ?></td>
                                            <td><?php echo $sala["numerada"]?"Si":"No"; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </table>
                                </p>
                            </div>
                        </fieldset>
                        <br>
                        <fieldset>
                            <legend>Venta de entradas</legend>
                            <div ng-repeat="entrada in thing.entradas">
                                <p>
                                <?php
                                foreach($entradas as $k => $ent) {
                                    echo "<a href='".$ent["url"]."' target='_blank'>".$ent["nombre"]."</a>";
                                }
                                ?>
                                </p>
                            </div>
                        </fieldset>
                    </div>
                    <h3>Próximos eventos <span class="ocultar">Ocultar</span></h3>
                    <div id='info-eventos-prox'>
                        <div class="grid">
                        <?php
                        lugarNextEvents($d, "cine", "idcine", false);
                        ?>
                        </div>
                    </div>
                    <h3>Eventos pasados (24 últimos) <span class="ocultar">Ocultar</span></h3>
                    <div id='info-eventos-pasados'>
                        <div class="grid">
                        <?php
                        lugarNextEvents($d, "cine", "idcine", true);
                        ?>
                        </div>
                    </div>
                <div>
            </section>
        </div>
    </section>
