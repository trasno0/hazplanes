<?php
/**
 * Clase controlador de exposiciones
 *
 * @package API
 * @author Trasno
 */
class Exposiciones {
    private $params;
    private $general;
    private $exposicionItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->exposicionItem = new ExposicionItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->exposicionItem = null;
    }

    /**
     * Crear exposicion
     *
     * @return int
     */
    public function createAction() {
        $exposicion = array();
        foreach($this->params as $key => $value){
            $exposicion[$key] = $value;
        }

        $horarios = array();
        $tipohorarios = array("museos","locales","auditorios","lugar");
        foreach($tipohorarios as $t) {
            if(isset($concierto[$t])) {
                $horarios[$t] = $exposicion[$t];
            }
            unset($exposicion[$t]);
        }

        $exposicion["publicado"] = array_key_exists("publicado",$exposicion)? 1:0;
        $exposicion["infantil"] = array_key_exists("infantil",$exposicion)? 1:0;
        $exposicion["reserva"] = array_key_exists("reserva",$exposicion)? 1:0;

        $result = $this->exposicionItem->addExposicion($exposicion);

        if($result) {
            //Insertar los horarios
            foreach($horarios as $tipo => $horario) {
                if(isset($horario[0][0]["fecha"])) {
                    switch($tipo) {
                        case "locales":
                            $idtipo = "idlocal";
                            $addtipo = "local";
                            break;
                        case "auditorios":
                            $idtipo = "idteatro";
                            $addtipo = "teatro";
                            break;
                        case "museos":
                            $idtipo = "idmuseo";
                            $addtipo = "museo";
                            break;
                        case "lugar":
                            $idtipo = "";
                            $addtipo = "lugar";
                            break;
                    }
                    foreach($horario as $key => $local) {
                        foreach($local as $k => $hora) {
                            if(isset($local[0][$idtipo])) {
                                $id = $local[0][$idtipo];
                            } else {
                                $id = "";
                            }
                            $pase = array(
                                "idlugar" => $id,
                                "idexposicion" => $result,
                                "inicio" => $hora["inicio"],
                                "fin" => $hora["fin"],
                                "precio" => $hora["precio"],
                                "permanente" => array_key_exists("permanente",$hora)? 1:0,
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0
                                );
                            if($tipo == "lugar") {
                                $pase["coordenadas"] = $hora["coordenadas"];
                                $pase["lugar"] = $hora["lugar"];
                            }
                            $this->exposicionItem->addHora($pase,$addtipo);
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Recuperar exposicion(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["agenda"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "lc.inicio >= CURDATE()",
                "order" => "inicio, nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->exposicionItem->listExposiciones($options);
        } else {
            if(isset($this->params["idexposicion"]) && !empty($this->params["idexposicion"])) {
                $data = $this->exposicionItem->dataExposicion($this->params["idexposicion"]);
                $horas = $this->exposicionItem->listHoras($data[0]);
                $data[0]["horasMuseo"] = $horas["horasMuseo"];
                $data[0]["horasLocal"] = $horas["horasLocal"];
                $data[0]["horasAuditorio"] = $horas["horasAuditorio"];
                $data[0]["horasLugar"] = $horas["horasLugar"];
            } else {
                $options = array(
                    "limit" => 0,
                    "start" => 0,
                    "filter" => "",
                    "order" => "idexposicion",
                    );
                if(!empty($this->params)) {
                    $options = array_merge($options,$this->params);
                }
                $data = $this->exposicionItem->listExposiciones($options);
            }
        }

        return $data;
    }

    /**
     * Actualizar exposicion
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            //Publicar o no publicar
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->exposicionItem->publishExposicion($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->exposicionItem->unpublishExposicion($this->params["ids"]);
            } else {
                $result = false;
            }
        } else {
            $exposicion = array();
            foreach($this->params as $key => $value){
                $exposicion[$key] = $value;
            }

            //Horarios museos
            $bdhorarios = $this->exposicionItem->listHoras(array("idexposicion" => $exposicion["idexposicion"]));
            $tipohorarios = array("horasLocal", "horasAuditorio", "horasMuseo", "horasLugar");
            foreach($tipohorarios as $th) {
                $bdhoras = $bdhorarios[$th];
                switch($th) {
                    case "horasLocal":
                        $formfield = "locales";
                        $idtipo = "idlocal";
                        $addtipo = "local";
                        break;
                    case "horasAuditorio":
                        $formfield = "auditorios";
                        $idtipo = "idteatro";
                        $addtipo = "teatro";
                        break;
                    case "horasMuseo":
                        $formfield = "museos";
                        $idtipo = "idmuseo";
                        $addtipo = "museo";
                        break;
                    case "horasLugar":
                        $formfield = "lugar";
                        $idtipo = "";
                        $addtipo = "lugar";
                        break;
                }
                if(isset($exposicion[$formfield])) {
                    $horarios = $exposicion[$formfield];
                } else {
                    $horarios = array();
                }

                foreach($horarios as $key => $local) {
                    foreach($local as $k => $hora) {
                        if(isset($local[0][$idtipo])) {
                            $idlugar = $local[0][$idtipo];
                        } else {
                            $idlugar = "";
                        }
                        if(isset($hora["inicio"])) {
                            $pase = array(
                                "idlugar" => $idlugar,
                                "idexposicion" => $exposicion["idexposicion"],
                                "inicio" => $hora["inicio"],
                                "fin" => $hora["fin"],
                                "precio" => $hora["precio"],
                                "permanente" => array_key_exists("permanente",$hora)? 1:0,
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0
                                );
                            if($addtipo == "lugar") {
                                $pase["coordenadas"] = $hora["coordenadas"];
                                $pase["lugar"] = $hora["lugar"];
                            }
                            $existe = false;
                            //Comprobamos si existe en la BD para modificarlo, en vez de darlo de alta de nuevo
                            foreach($bdhoras as $ke => $val) {
                                if(isset($val[$idtipo])) {
                                    if($val[$idtipo] == $pase["idlugar"] && $val["idexposicion"] == $pase["idexposicion"]) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                } else {
                                    if($val["idexposicion"] == $pase["idexposicion"]) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                }
                            }
                            if($existe) {
                                $this->exposicionItem->updateHora($pase,$addtipo);
                            } else {
                                $this->exposicionItem->addHora($pase,$addtipo);
                            }
                        }
                    }
                }
                foreach($bdhoras as $delHora) {
                    if(isset($delHora[$idtipo])) {
                        $delHora["idlugar"] = $delHora[$idtipo];
                    }
                    $this->exposicionItem->deleteHora($delHora,$addtipo);
                }
                unset($exposicion[$formfield]);
            }

            $exposicion["publicado"] = array_key_exists("publicado",$exposicion)? 1:0;
            $exposicion["infantil"] = array_key_exists("infantil",$exposicion)? 1:0;
            $exposicion["reserva"] = array_key_exists("reserva",$exposicion)? 1:0;

            $result = $this->exposicionItem->updateExposicion($exposicion);
        }

        return $result;
    }

    /**
     * Eliminar exposicion
     *
     * @return boolean
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->exposicionItem->deleteExposicion($this->params["id"]);
        }

        return $result;
    }
}
