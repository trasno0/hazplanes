<?php
/**
 * Clase controlador de peliculas
 *
 * @package API
 * @author Trasno
 */
class Peliculas {
    private $params;
    private $general;
    private $peliculaItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->peliculaItem = new PeliculaItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->peliculaItem = null;
    }

    /**
     * Crear pelicula
     *
     * @return int
     */
    public function createAction() {
        $pelicula = array();
        foreach($this->params as $key => $value){
            $pelicula[$key] = $value;
        }

        $horarios = array();
        $tipohorarios = array("locales","auditorios","cines","lugar","museos");
        foreach($tipohorarios as $t) {
            if(isset($pelicula[$t])) {
                $horarios[$t] = $pelicula[$t];
            }
            unset($pelicula[$t]);
        }
        unset($pelicula["from"]);

        $pelicula["publicado"] = array_key_exists("publicado",$pelicula)? 1:0;
        $pelicula["infantil"] = array_key_exists("infantil",$pelicula)? 1:0;
        $pelicula["reserva"] = array_key_exists("reserva",$pelicula)? 1:0;

        $result = $this->peliculaItem->addPelicula($pelicula);

        if($result) {
            //Insertar los horarios
            foreach($horarios as $tipo => $horario) {
                if(isset($horario[0][0]["fecha"])) {
                    switch($tipo) {
                        case "locales":
                            $idtipo = "idlocal";
                            $addtipo = "local";
                            break;
                        case "auditorios":
                            $idtipo = "idteatro";
                            $addtipo = "teatro";
                            break;
                        case "cines":
                            $idtipo = "idcine";
                            $addtipo = "cine";
                            break;
                        case "lugar":
                            $idtipo = "";
                            $addtipo = "lugar";
                            break;
                        case "museos":
                            $idtipo = "idmuseo";
                            $addtipo = "museo";
                            break;
                    }
                    foreach($horario as $key => $local) {
                        foreach($local as $k => $hora) {
                            if(isset($local[0][$idtipo])) {
                                $id = $local[0][$idtipo];
                            } else {
                                $id = "";
                            }
                            $pase = array(
                                "idlugar" => $id,
                                "idpelicula" => $result,
                                "fecha" => $hora["fecha"],
                                "precio" => $hora["precio"],
                                "3d" => array_key_exists("3d",$hora)? 1:0,
                                "vo" => array_key_exists("vo",$hora)? 1:0,
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                );
                            if($tipo == "cines") {
                                $pase["idsala"] = $hora["idsala"];
                            } else {
                                $pase["precioanticipada"] = $hora["precioanticipada"];
                            }
                            if($tipo == "lugar") {
                                $pase["coordenadas"] = trim($hora["coordenadas"]);
                                $pase["lugar"] = trim($hora["lugar"]);
                                $pase["localidad"] = trim($hora["localidad"]);
                            } else {
                                $pase["urlcompra"] = trim($hora["urlcompra"]);
                            }
                            $this->peliculaItem->addHora($pase,$addtipo);
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Recuperar pelicula(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["agenda"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "fecha >= NOW()",
                "order" => "fecha, nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->peliculaItem->listAgendaPeliculas($options);
        } else {
            if(isset($this->params["idpelicula"]) && !empty($this->params["idpelicula"])) {
                $data = $this->peliculaItem->dataPelicula($this->params["idpelicula"]);
                $horas = $this->peliculaItem->listHoras($data[0]);
                $data[0]["horasCine"] = $horas["horasCine"];
                $data[0]["horasLocal"] = $horas["horasLocal"];
                $data[0]["horasAuditorio"] = $horas["horasAuditorio"];
                $data[0]["horasLugar"] = $horas["horasLugar"];
                $data[0]["horasMuseo"] = $horas["horasMuseo"];
            } else {
                $options = array(
                    "limit" => 0,
                    "start" => 0,
                    "filter" => "",
                    "order" => "fechaestreno desc"
                    );
                if(!empty($this->params)) {
                    $options = array_merge($options,$this->params);
                }
                $data = $this->peliculaItem->listPeliculas($options);
            }
        }

        return $data;
    }

    /**
     * Actualizar pelicula
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            //Publicar o no publicar
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->peliculaItem->publishPelicula($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->peliculaItem->unpublishPelicula($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "field") {
                $fields = array();
                foreach($this->params as $key => $value){
                    $fields[$key] = $value;
                }
                unset($fields["op"]);
                $result = $this->peliculaItem->updatePelicula($fields);
            } else {
                $result = false;
            }
        } else {
            $pelicula = array();
            foreach($this->params as $key => $value){
                $pelicula[$key] = $value;
            }
            $from = "";
            if(isset($pelicula["from"])) {
                $from = 1;
                unset($pelicula["from"]);
            }


            //Horarios
            $bdhorarios = $this->peliculaItem->listHoras(array("idpelicula" => $pelicula["idpelicula"]));
            $tipohorarios = array("horasLocal", "horasAuditorio", "horasCine", "horasLugar", "horasMuseo");
            foreach($tipohorarios as $th) {
                $bdhoras = $bdhorarios[$th];
                switch($th) {
                    case "horasCine":
                        $formfield = "cines";
                        $idtipo = "idcine";
                        $addtipo = "cine";
                        break;
                    case "horasLocal":
                        $formfield = "locales";
                        $idtipo = "idlocal";
                        $addtipo = "local";
                        break;
                    case "horasAuditorio":
                        $formfield = "auditorios";
                        $idtipo = "idteatro";
                        $addtipo = "teatro";
                        break;
                    case "horasLugar":
                        $formfield = "lugar";
                        $idtipo = "";
                        $addtipo = "lugar";
                        break;
                    case "horasMuseo":
                        $formfield = "museos";
                        $idtipo = "idmuseo";
                        $addtipo = "museo";
                        break;
                }
                if(isset($pelicula[$formfield])) {
                    $horarios = $pelicula[$formfield];
                } else {
                    $horarios = array();
                }
                $idlugares = array();
                foreach($horarios as $key => $local) {
                    foreach($local as $k => $hora) {
                        if(isset($local[0][$idtipo])) {
                            $id = $local[0][$idtipo];
                        } else {
                            $id = "";
                        }
                        array_push($idlugares,$id);
                        if(isset($hora["fecha"])) {
                            $pase = array(
                                "idlugar" => $id,
                                "idpelicula" => $pelicula["idpelicula"],
                                "fecha" => date("Y-m-d H:i",strtotime($hora["fecha"])),
                                "precio" => $hora["precio"],
                                "3d" => array_key_exists("3d",$hora)? 1:0,
                                "vo" => array_key_exists("vo",$hora)? 1:0,
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                );
                            if($addtipo == "cine") {
                                $pase["idsala"] = $hora["idsala"];
                            } else {
                                $pase["precioanticipada"] = $hora["precioanticipada"];
                            }
                            if($addtipo == "lugar") {
                                $pase["coordenadas"] = trim($hora["coordenadas"]);
                                $pase["lugar"] = trim($hora["lugar"]);
                                $pase["localidad"] = trim($hora["localidad"]);
                            } else {
                                $pase["urlcompra"] = trim($hora["urlcompra"]);
                            }
                            $existe = false;
                            //Comprobamos si existe en la BD para modificarlo, en vez de darlo de alta de nuevo
                            foreach($bdhoras as $ke => $val) {
                                if(isset($val[$idtipo])) {
                                    if($val[$idtipo] == $pase["idlugar"] && $val["idpelicula"] == $pase["idpelicula"] && date("Y-m-d H:i",strtotime($val["fecha"])) == $pase["fecha"]) {
                                        if(isset($pase["idsala"]) && $val["idsala"] == $pase["idsala"]) {
                                            $existe = true;
                                            unset($bdhoras[$ke]);
                                            break;
                                        } else if(!isset($pase["idsala"])) {
                                            $existe = true;
                                            unset($bdhoras[$ke]);
                                            break;
                                        }
                                    }
                                } else {
                                    if($val["idpelicula"] == $pase["idpelicula"] && date("Y-m-d H:i",strtotime($val["fecha"])) == date("Y-m-d H:i",strtotime($pase["fecha"]))) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                }
                            }
                            if($existe) {
                                $this->peliculaItem->updateHora($pase,$addtipo);
                            } else {
                                $this->peliculaItem->addHora($pase,$addtipo);
                            }
                        }
                    }
                }
                foreach($bdhoras as $delHora) {
                    if(isset($delHora[$idtipo])) {
                        $delHora["idlugar"] = $delHora[$idtipo];
                    }
                    if(in_array($delHora["idlugar"], $idlugares) || $from == 1) {
                        $this->peliculaItem->deleteHora($delHora,$addtipo);
                    }
                }
                unset($pelicula[$formfield]);
            }

            $pelicula["publicado"] = array_key_exists("publicado",$pelicula)? 1:0;
            $pelicula["infantil"] = array_key_exists("infantil",$pelicula)? 1:0;
            $pelicula["reserva"] = array_key_exists("reserva",$pelicula)? 1:0;

            $result = $this->peliculaItem->updatePelicula($pelicula);
        }

        return $result;
    }

    /**
     * Eliminar pelicula
     *
     * @return boolean
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->peliculaItem->deletePelicula($this->params["id"]);
        }

        return $result;
    }
}