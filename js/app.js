angular.module('app', []);

function Controller($scope, $http, $sce) {
	api = "api/v2/";

	$scope.cancel = function(url) {
        window.location.href = url;
    };

	if(typeof(salas) === "undefined") salas = [];
	if(typeof(promos) === "undefined") promos = [];
	if(typeof(entradas) === "undefined") entradas = [];
	if(typeof(cines) === "undefined") cines = [];
	if(typeof(cinesScraper) === "undefined") cinesScraper = [];
	if(typeof(locales) === "undefined") locales = [];
	if(typeof(auditorios) === "undefined") auditorios = [];
	if(typeof(museos) === "undefined") museos = [];
	if(typeof(pabellones) === "undefined") pabellones = [];
	if(typeof(lugares) === "undefined") lugares = [];
	if(typeof(search_db) === "undefined") search_db = "";

	//Lugares
	$scope.lugares = lugares;
	$scope.addDiaLugar = function() {
        $scope.lugares.push({});
    };
    $scope.removeDiaLugar = function(lugar) {
        $scope.lugares.splice($scope.lugares.indexOf(lugar), 1);
    };
	//Fin lugares

	$scope.thing = {
        salas: salas,
        promos: promos,
        entradas: entradas
   	};

    $scope.addSala = function() {
        $scope.thing.salas.push({});
    };
    $scope.addPromo = function() {
        $scope.thing.promos.push({});
    };
    $scope.addEntrada = function() {
        $scope.thing.entradas.push({});
    };

    $scope.removeSala = function(sala) {
        $scope.thing.salas.splice($scope.thing.salas.indexOf(sala), 1);
    };
    $scope.removePromo = function(promo) {
        $scope.thing.promos.splice($scope.thing.promos.indexOf(promo), 1);
    };
    $scope.removeEntrada = function(entrada) {
        $scope.thing.entradas.splice($scope.thing.entradas.indexOf(entrada), 1);
    };

    //Recuperar el numero de salas segun el cine seleccionado (solo para cines)
    $scope.refreshSalas = function(cine,c) {
        $http({
            method: 'POST',
            url: api + 'index.php',
            data: $.param({ controller: "Salas", action: "read", idcine: cine.idcine }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(dat) {
            $scope.cines[$scope.cines.indexOf(cine)].child[$scope.cines[$scope.cines.indexOf(cine)].child.length-1].salas = angular.fromJson(dat.data);
        });
    };

    //Cines
    $scope.cines = cines;
    $scope.cinesScraper = cinesScraper;

    $scope.addCine = function() {
        $scope.cines.push({child: []});
    };
    $scope.addHoraCine = function(cine,hora) {
        $scope.cines[$scope.cines.indexOf(cine)].child.push({salas: {}, horas: {}});
        $scope.refreshSalas(cine,hora);
    };
    $scope.removeCine = function(cine) {
        $scope.cines.splice($scope.cines.indexOf(cine), 1);
    };
    $scope.removeHoraCine = function(cine, hora) {
        $scope.cines[$scope.cines.indexOf(cine)].child.splice($scope.cines[$scope.cines.indexOf(cine)].child.indexOf(hora), 1);
    };

	//Locales
    $scope.locales = locales;
    $scope.addLocal = function() {
        $scope.locales.push({child: []});
        $scope.addHoraLocal($scope.locales[$scope.locales.length-1]);
    };
    $scope.addHoraLocal = function(local) {
        $scope.locales[$scope.locales.indexOf(local)].child.push({horas: {}});
    };
    $scope.removeLocal = function(local) {
        $scope.locales.splice($scope.locales.indexOf(local), 1);
    };
    $scope.removeHoraLocal = function(local, hora) {
        $scope.locales[$scope.locales.indexOf(local)].child.splice($scope.locales[$scope.locales.indexOf(local)].child.indexOf(hora), 1);
    };

    //Auditorios
    $scope.auditorios = auditorios;
    $scope.addAuditorio = function() {
        $scope.auditorios.push({child: []});
        $scope.addHoraAuditorio($scope.auditorios[$scope.auditorios.length-1]);
    };
    $scope.addHoraAuditorio = function(auditorio) {
        $scope.auditorios[$scope.auditorios.indexOf(auditorio)].child.push({horas: {}});
    };
    $scope.removeAuditorio = function(auditorio) {
        $scope.auditorios.splice($scope.auditorios.indexOf(auditorio), 1);
    };
    $scope.removeHoraAuditorio = function(auditorio, hora) {
        $scope.auditorios[$scope.auditorios.indexOf(auditorio)].child.splice($scope.auditorios[$scope.auditorios.indexOf(auditorio)].child.indexOf(hora), 1);
   	};

    //Museos
    $scope.museos = museos;

    $scope.addMuseo = function() {
        $scope.museos.push({child: []});
        $scope.addHoraMuseo($scope.museos[$scope.museos.length-1]);
    };
    $scope.addHoraMuseo = function(museo) {
        $scope.museos[$scope.museos.indexOf(museo)].child.push({horas: {}});
    };
    $scope.removeMuseo = function(museo) {
        $scope.museos.splice($scope.museos.indexOf(museo), 1);
    };
    $scope.removeHoraMuseo = function(museo, hora) {
        $scope.museos[$scope.museos.indexOf(museo)].child.splice($scope.museos[$scope.museos.indexOf(museo)].child.indexOf(hora), 1);
    };

    //Pabellones
    $scope.pabellones = pabellones;

    $scope.addPabellon = function() {
        $scope.pabellones.push({child: []});
        $scope.addHoraPabellon($scope.pabellones[$scope.pabellones.length-1]);
    };
    $scope.addHoraPabellon = function(pabellon) {
        $scope.pabellones[$scope.pabellones.indexOf(pabellon)].child.push({horas: {}});
    };
    $scope.removePabellon = function(pabellon) {
        $scope.pabellones.splice($scope.pabellones.indexOf(pabellon), 1);
    };
    $scope.removeHoraPabellon = function(pabellon, hora) {
        $scope.pabellones[$scope.pabellones.indexOf(pabellon)].child.splice($scope.pabellones[$scope.pabellones.indexOf(pabellon)].child.indexOf(hora), 1);
    };

	//Scraper
	//Lanzar scraper
    $scope.scraperLaunch = function(origen) {
    	$scope.scraper_launch_result = $sce.trustAsHtml('<div id="facebookG"><div id="blockG_1" class="facebook_blockG"></div><div id="blockG_2" class="facebook_blockG"></div><div id="blockG_3" class="facebook_blockG"></div></div>');
    	switch(origen) {
    		case 'servinova':
    			url = 'scrapers/scraperServinova.php';
    			break;
			case 'yelmo':
    			url = 'scrapers/scraperYelmo.php';
    			break;
			case 'plazae':
    			url = 'scrapers/scraperPlazae.php';
    			break;
			case 'granvia':
    			url = 'scrapers/scraperGranvia.php';
    			break;
			case 'multinorte':
    			url = 'scrapers/scraperMulticines.php';
    			break;
			case 'yelmoestrenos':
				url = 'scrapers/scraperYelmoEstrenos.php';
				break;
			default:
				$scope.scraper_launch_result = $sce.trustAsHtml("Error del scraper. Origen no definido");
				exit;
    	}
    	$http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(dat) {
        	$scope.scraper_launch_result = $sce.trustAsHtml(dat);
        }).
        error(function(data, status, headers, config) {
        	$scope.scraper_launch_result = $sce.trustAsHtml("Error del scraper");
    	});
    };

	$scope.action = "a";
	$scope.search_db = search_db;
	$scope.search_tmdb = search_db;
	$scope.search_fa = search_db;
	//Recuperar info de bd
    $scope.searchdb = function() {
    	$scope.search_db_result = "Buscando...";
    	$http({
            method: 'POST',
            url: 'includes/scraper_search_db.php',
            data: $.param({ titulo: $scope.search_db }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(dat) {
        	if(dat.debug != "")
        		console.log(dat.debug);
        	if(dat.success == "ok") {
	        	dataDB = dat.data;
	        	posDB = 0;
				$scope.db = dataDB[0];
	        	if(dataDB.length > 0) {
	            	$scope.search_db_result = "Hay " + dataDB.length + " coincidencias";
	            	$scope.action = "m";
	            	$scope.idpelicula = dataDB[0].idpelicula;
	            } else {
	            	$scope.search_db_result = "No se ha encontrado";
	            	$scope.action = "a";
	            }
           	} else {
	           	console.log(dat);
           	}
        });
    };

    $scope.nextDB = function() {
    	if(posDB < dataDB.length-1) {
        	posDB++;
    		$scope.db = dataDB[posDB];
    		$scope.idpelicula = dataDB[posDB].idpelicula;
        }
    };
    $scope.prevDB = function() {
    	if(posDB > 0) {
        	posDB--;
    		$scope.db = dataDB[posDB];
    		$scope.idpelicula = dataDB[posDB].idpelicula;
        }
    };
    //Fin recuperar info de bd
    //Recuperar info de tmbd
    $scope.searchtmdb = function() {
    	$scope.search_tmdb_result = "Buscando...";
    	$http({
            method: 'POST',
            url: 'includes/scraper_search_tmdb.php',
            data: $.param({ titulo: $scope.search_tmdb }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(dat) {
        	if(dat.debug != "")
        		console.log(dat.debug);
        	if(dat.success == "ok") {
        		dataTMDB = dat.data;
        		posTMDB = 0;
				$scope.tmdb = dataTMDB[0];
        		if(dataTMDB.length > 0) {
	            	$scope.search_tmdb_result = "Hay " + dataTMDB.length + " coincidencias";
            	} else {
	            	$scope.search_tmdb_result = "No se ha encontrado";
            	}
           	} else {
           		$scope.search_tmdb_result = "Error";
	           	console.log(dat);
           	}
        });
    };

    $scope.nextTMDB = function() {
    	if(posTMDB < dataTMDB.length-1) {
        	posTMDB++;
    		$scope.tmdb = dataTMDB[posTMDB];
        }
    };
    $scope.prevTMDB = function() {
    	if(posTMDB > 0) {
        	posTMDB--;
    		$scope.tmdb = dataTMDB[posTMDB];
        }
    };
    //Fin recuperar info de tmbd
    //Recuperar info de fa
    $scope.searchfa = function() {
    	$scope.search_fa_result = "Buscando...";
    	$http({
            method: 'POST',
            url: 'includes/scraper_search_fa.php',
            data: $.param({ titulo: $scope.search_fa }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(dat) {
        	if(dat.debug != "" && typeof dat.debug !== 'undefined') {
        		console.log(dat.debug);
        	}
        	if(dat.success == "ok") {
        		dataFA = dat.data;
        		posFA = 0;
				$scope.fa = dataFA[0];
        		if(dataFA.length > 0) {
	            	$scope.search_fa_result = "Hay " + dataFA.length + " coincidencias";
            	} else {
	            	$scope.search_fa_result = "No se ha encontrado";
            	}
           	} else {
           		$scope.search_fa_result = "Error";
	           	console.log(dat);
           	}
        });
    };

    $scope.nextFA = function() {
    	if(posFA < dataFA.length-1) {
        	posFA++;
    		$scope.fa = dataFA[posFA];
        }
    };
    $scope.prevFA = function() {
    	if(posFA > 0) {
        	posFA--;
    		$scope.fa = dataFA[posFA];
        }
    };
    //Fin recuperar info de fa
    //Fin scraper
}

posDB = 0;
dataDB = "";
posTMDB = 0;
dataTMDB = "";
posFA = 0;
dataFA = "";