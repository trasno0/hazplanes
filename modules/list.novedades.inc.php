<?php
pageAccessControl();
?>
    <section id='content'>
        <div class="grid">
            <section id="main">
                <ul id="celdas">
                    <?php
                    $tipoNovedad = array("creado", "actualizado");
                    $limit = 25;
                    if(isset($id) && !empty($id)) {
                        if($id == 1) {
                            $id = "creado";
                        } else {
                            $id = "actualizado";
                        }
                        $tipoNovedad = array($id);
                        $limit = 50;

                    }
                    $textoUltimas = "Últimos ".$limit." eventos";
                    foreach($tipoNovedad as $tipo) {
                        $options = array(
                            "novedades" => $tipo,
                            "limite" => $limit
                            );
                        $general = new GeneralController($options,$db);
                        $data = $general->readAction();
                        if($tipo == "creado") {
                            echo "<li class='linea fecha'>Novedades <span class='nota'>(".$textoUltimas.")</span><span style='float: right;'><a type='application/rss+xml' href='rssnovedades.php'><img src='img/interface/icon-rss.png' alt='RSS de novedades'></a></span></li>";
                        } else {
                            echo '</ul>
                            </div>
                            <div class="grid">
                                <ul id="celdas">';
                            echo "<li class='linea fecha'>Actualizaciones <span class='nota'>(".$textoUltimas.")</span></li>";
                        }
                        $publi = 0;
                        $countCells = 0;
                        $ids = array();
                        foreach($data as $k => $d) {
                            $countCells++;
                            if($countCells == 9) {
                                echo $celdaPubli;
                                $publi++;
                            }
                            $ids[] = $d["nombre"];
                            $img = "";
                            $type = $d["tipoevento"];
                            if($type == "deportes") {
                                if(!empty($d["genero"])) {
                                    $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["genero"]);
                                    $tipodeporte = new Deportes($options2,$db);
                                    $d2 = $tipodeporte->readAction();
                                    if(!empty($d2)) {
                                        $d2 = $d2[0];
                                        $d["genero"] = $d2["nombre"];
                                    } else {
                                        $d["genero"] = "Sin especificar";
                                    }
                                } else {
                                    $d["genero"] = "Sin especificar";
                                }
                            }
                            $imgnull = "";
                            switch($type) {
                                case "peliculas":
                                    $class = "film";
                                    $imgnull = "null-film.png";
                                    $type = "PELICULA";
                                    $link = "pelicula/";
                                    break;
                                case "conciertos":
                                    $class = "concert";
                                    $imgnull = "null-music.png";
                                    $type = "CONCIERTO";
                                    $link = "concierto/";
                                    break;
                                case "obrasteatro":
                                    $class = "theater";
                                    $imgnull = "null-theatre.png";
                                    $type = "TEATRO";
                                    $link = "obra/";
                                    break;
                                case "eventos":
                                    $class = "event";
                                    $imgnull = "null-calendar.png";
                                    $type = "EVENTO";
                                    $link = "evento/";
                                    break;
                                case "exposiciones":
                                    $class = "expo";
                                    $imgnull = "null-museum.png";
                                    $type = "EXPOSICIÓN";
                                    $link = "exposicion/";
                                    break;
                                case "deportes":
                                    $class = "sport";
                                    $imgnull = "null-sports.png";
                                    $type = "DEPORTE";
                                    $link = "competicion/";
                                    break;
                                case "formacion":
                                case "cursos":
                                    $class = "course";
                                    $imgnull = "null-cursos.png";
                                    $type = "FORMACIÓN";
                                    $link = "curso/";
                                    break;
                            }
                            if(empty($img) || !file_exists($img)) {
                                $img = "img/interface/".$imgnull;
                            }
                            echo "<li class='linea ".$class."'>";
                            $infostatus = "";
                            if($d["cancelado"]) {
                                echo "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
                                $infostatus = "cancelado";
                            } else {
                               if($d["agotado"]) {
                                   echo "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
                                   $infostatus = "agotado";
                               } else {
                                   if($d["infantil"]) {
                                       echo "<div class='ribbon-wrapper'><div class='ribbon-infantil'>NIÑOS</div></div>";
                                       $infostatus = "infantil";
                                   }
                               }
                            }
                            if($tipo == "creado") {
                                $actualizado = tiempo_transcurrido($d["creado"]);
                            } else {
                                $actualizado = tiempo_transcurrido($d["actualizado"]);
                            }
                            echo "        <div class='grid-info ".$infostatus."'>
                                        <a href='".$link.$d["id"]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a>
                                        <br>
                                        <span class='genre'>".$d["genero"]."</span>
                                        <br>
                                        <span class='hour'>".$actualizado."</span>
                                        <span class='type'>".$type."</span>
                                    </div>
                                </li>";
                        }
                        if(count($tipoNovedad) > 1) {
                            echo "<li class='linea noclick'><a href='".BASE_URL."novedades/".$tipo."'>Ver más…</a></li>";
                        } else {
                            if(count($data) == 0) {
                                echo "<li class='linea noclick'>No hay eventos en este apartado</li>";
                            }
                        }
                    }
                    ?>
                </ul>
            </section>
        </div>
    </section>
