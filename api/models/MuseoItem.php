<?php
/**
 * Clase con metodos para operaciones con museos
 *
 * @package API
 * @author Trasno
 */
class MuseoItem {
    protected $db;
    private $general;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
        $this->general = new General($db);
    }

    /**
     * Coge todos los museos
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de pelicula.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listMuseos($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf("select m.* from %smuseos m %s order by m.%s %s", BDPREFIX, $options["filter"], $this->db->secure_field($options["order"]), $this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($museo = $this->db->fetch($r)) {
            $result[] = $museo;
        }
        foreach($result as $k => $v) {
            //$result[$k]["promos"] = $this->getPromociones($museo["idmuseo"]);
            $options["idlugar"] = $v["idmuseo"];
            $options["tipolugar"] = "museo";
            $result[$k]["horario"] = $this->general->listHorarios($options);
        }
        return $result;
    }

    /**
     * Coge un museo y todos sus datos (salas, servicios...)
     *
     * @param int $idmuseo id del museo
     * @return array|false
     */
     public function dataMuseo($idmuseo) {
        if(!empty($idmuseo)) {
            $query = sprintf("select m.* from %smuseos m where m.idmuseo = %d", BDPREFIX, $this->db->secure_field($idmuseo));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                $museo = $this->db->fetch($r);
                //$museo["promos"] = $this->getPromociones($museo["idmuseo"]);
                $options["idlugar"] = $museo["idmuseo"];
                $options["tipolugar"] = "museo";
                $museo["horario"] = $this->general->listHorarios($options);
                return array(0 => $museo);
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un museo
     *
     * @param int $idmuseo id del museo
     * @return boolean
     */
    public function deleteMuseo($idmuseo) {
        if(!empty($idmuseo)) {
            $query = sprintf("delete from %smuseos where idmuseo = %d",BDPREFIX, $this->db->secure_field($idmuseo));
            $r = $this->db->execute($query);
            if($r) {
                deleteAllExtImages("img/museos/".$idmuseo);
                deleteAllExtImages("img/museos/original/".$idmuseo);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar un museo
     *
     * @param array $museo datos del museo
     * @param string $museo['nombre'] nombre del museo
     * @param string $museo['direccion']
     * @param string $museo['museoidad']
     * @param string $museo['provincia']
     * @param int $museo['codigopostal']
     * @param string $museo['coordenadas']
     * @param string $museo['telefono']
     * @param string $museo['email']
     * @param string $museo['web']
     * @param string $museo['twitter']
     * @param string $museo['facebook']
     * @param string $museo['google']
     * @param string $museo['otrasocial']
     * @param string $museo['notas']
     * @param string $museo['descripcion']
     * @param string $museo['imagen']
     * @return int
     */
    public function addMuseo($museo) {
        if(!$this->checkMuseo("nombre", $museo["nombre"])) {
            $fields = "";
            $values = "";
            foreach($museo as $key => $value) {
                if(!empty($fields)) {
                    $fields = $this->db->secure_field($fields).",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %smuseos (%s, creado) VALUES (%s, NOW())", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addMuseo] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addMuseo] Ya existe el museo.", 1);
        }
    }

    /*
     * Actualiza los campos de un museo
     *
     * @param array $museo datos del museo
     * @param int $museo['idmuseo'] identificador del museo
     * @param string $museo['nombre'] nombre del museo
     * @param string $museo['direccion']
     * @param string $museo['museoidad']
     * @param string $museo['provincia']
     * @param int $museo['codigopostal']
     * @param string $museo['coordenadas']
     * @param string $museo['telefono']
     * @param string $museo['email']
     * @param string $museo['web']
     * @param string $museo['twitter']
     * @param string $museo['facebook']
     * @param string $museo['google']
     * @param string $museo['otrasocial']
     * @param string $museo['notas']
     * @param string $museo['descripcion']
     * @param string $museo['imagen']
     * @return boolean
     */
    public function updateMuseo($museo) {
        if($this->checkMuseo("idmuseo", $museo["idmuseo"])) {
            $fields = "";
            foreach($museo as $key => $value) {
                if($key != "idmuseo") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %smuseos set %s, actualizado = NOW() where idmuseo = %d", BDPREFIX, $fields, $this->db->secure_field($museo["idmuseo"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateMuseo] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateMuseo] No existe el museo.", 1);
        }
    }

    /*
     * Publicar museos para que sean visibles
     *
     * @param int $ids ids de los museos
     * @return boolean
     */
    public function publishMuseo($ids) {
        $query = sprintf("update %smuseos set publicado=1 where idmuseo in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishMuseo] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar museos para que no sean visibles
     *
     * @param int $ids ids de los museos
     * @return boolean
     */
    public function unpublishMuseo($ids) {
        $query = sprintf("update %smuseos set publicado=0 where idmuseo in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishMuseo] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar un sitio de entradas con un museo
     *
     * @param int $idmuseo id del museo
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function setEntradas($idmuseo, $identradas) {
        if(!$this->checkRelEntradas($idmuseo, $identradas)) {
            $query = sprintf("insert into %smuseos_has_entradas (idmuseo,identradas) VALUES (%d,%d)", BDPREFIX, $this->db->secure_field($idmuseo), $this->db->secure_field($identradas));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[setEntradas] Ya existe la relacion entre el museo y el sitio de entradas.", 1);
        }
    }

    /*
     * Relacionar una promocion con un museo
     *
     * @param int $idmuseo id del museo
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    public function setPromociones($idmuseo, $idpromocion) {
        if(!$this->checkRelPromos($idmuseo, $idpromocion)) {
            $query = sprintf("insert into %smuseos_has_promociones (idmuseo,idpromocion) VALUES (%d,%d)", BDPREFIX, $this->db->secure_field($idmuseo), $this->db->secure_field($idpromocion));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setPromociones] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[setPromociones] Ya existe la relacion entre el museo y la promoción.", 1);
        }
    }

    /*
     * Relacionar un sitio de entradas con un museo
     *
     * @param int $idmuseo id del museo
     * @return array
     */
    public function getEntradas($idmuseo) {
        $query = sprintf("select le.identradas from %smuseos_has_entradas le where le.idmuseo = %d", BDPREFIX, $this->db->secure_field($idmuseo));
        $r = $this->db->query($query);
        if($r) {
            $result = array();
            while($museo_entradas = $this->db->fetch($r)) {
                $result[] = $museo_entradas;
            }
            return $result;
        } else {
            throw new Exception("[getEntradas] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar una promocion con un museo
     *
     * @param int $idmuseo id del museo
     * @return array
     */
    public function getPromociones($idmuseo) {
        $query = sprintf("select lp.idpromocion from %smuseos_has_promociones lp where lp.idmuseo = %d", BDPREFIX, $this->db->secure_field($idmuseo));
        $r = $this->db->query($query);
        if($r) {
            $result = array();
            while($museo_promociones = $this->db->fetch($r)) {
                $result[] = $museo_promociones;
            }
            return $result;
        } else {
            throw new Exception("[getPromociones] Error en la query: ".$query, 1);
        }
    }

    /*
     * Eliminar la relacion de un sitio de entradas con un museo
     *
     * @param int $idmuseo id del museo
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function delEntradas($idmuseo, $identradas = 0) {
        if(empty($identradas) || $this->checkRelEntradas($idmuseo, $identradas)) {
            $query = sprintf("delete from %smuseos_has_entradas where idmuseo = %d", BDPREFIX, $this->db->secure_field($idmuseo));
            if(empty($identradas)) {
                $query .= " and identradas = ".$this->db->secure_field($identradas);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delEntradas] No existe la relacion entre el museo y el sitio de entradas.", 1);
        }
    }

    /*
     * Eliminar la relacion de una promocion con un museo
     *
     * @param int $idmuseo id del museo
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    public function delPromociones($idmuseo, $idpromocion = 0) {
        if(empty($idpromocion) || $this->checkRelPromos($idmuseo, $idpromocion)) {
            $query = sprintf("delete from %smuseos_has_promociones where idmuseo = %d", BDPREFIX, $this->db->secure_field($idmuseo));
            if(empty($identradas)) {
                $query .= " and idpromocion = ".$this->db->secure_field($idpromocion);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delPromociones] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delPromociones] No existe la relacion entre el museo y la promoción.", 1);
        }
    }

    /*
     * Comprueba si existe el museo
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkMuseo($field, $value) {
        $query = sprintf("select l.* from %smuseos l where l.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion museo-entradas
     *
     * @param int $idmuseo id del museo
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    private function checkRelEntradas($idmuseo, $identradas) {
        $query = sprintf("select le.* from %smuseos_has_entradas le where le.idmuseo = %d and le.identradas = %d", BDPREFIX, $this->db->secure_field($idmuseo), $this->db->secure_field($identradas));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion museo-promociones
     *
     * @param int $idmuseo id del museo
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    private function checkRelPromos($idmuseo, $idpromocion) {
        $query = sprintf("select lp.* from %smuseos_has_promociones lp where lp.idmuseo = %d and lp.idpromocion = %d", BDPREFIX, $this->db->secure_field($idmuseo), $this->db->secure_field($idpromocion));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END
