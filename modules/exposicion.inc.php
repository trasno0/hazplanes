<?php
pageAccessControl();

$style = "info2";
if(isset($d["fanart"]) && !empty($d["fanart"])) {
    echo "<style>
        html {
            background: black url(".$d["fanart"].") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>";
	$style = "info";
}
?>
    <section id='content'>
        <div class="grid">
            <section id='<?php echo $style; ?>' itemscope itemtype="http://data-vocabulary.org/Event">
                <h2>
                    <?php
                    echo "<span itemprop='name'>".$d["nombre"]."</span><span itemprop='summary' style='display:none;'>".$d["nombre"]."</span>";
                    if($d["infantil"] == 1) {
                        echo "<span class='h2-infantil'>Para niños</span>";
                    }
                    ?>
                    <span itemprop="eventType" style='display:none;'>Exposición</span>
                </h2>
                <div id="info-izq">
                    <?php
                    if(isset($d["reserva"]) && !empty($d["reserva"])) {
                        if(!empty($d["limitereserva"]) && $d["limitereserva"] > "2000-01-01") {
                            echo "<div id='reserva'>Reservar antes del ".date("d-m-Y H:i",strtotime($d["limitereserva"]))."</div>";
                        } else {
                            echo "<div id='reserva'>Reserva necesaria</div>";
                        }
                    }
                    $ampliar = "";
                    if(empty($d["poster"]) || !file_exists($d["poster"])) {
                        $d["poster"] = "img/interface/null-museum.png";
                    } else {
                        $ampliar = "<div class='ampliar'>+</div>";
                    }
                    echo "<div id='poster'>";
                    echo $ampliar;
                    echo "  <img src='".$d["poster"]."' id='posterimg' itemprop='photo' alt='Poster de la exposición'/>";
                    echo "</div>";
                    echo "<br>";
                    if(!empty($d["menciones"])) { ?>
                        <h3>Créditos / Agradecimientos</h3>
                        <div id='info-credits'>
                            <p style='padding: 0 10px 0;'>
                                <?php echo htmlspecialchars_decode ($d["menciones"]); ?>
                            </p>
                        </div>
                    <?php
                    }
                    echo $publiFicha;
                    ?>
                    <span id='last-update'>
                        Última actualización:
                        <?php
                        if(!empty($d["actualizado"])){
                            echo $d["actualizado"];
                        } else {
                            echo $d["creado"];
                        }
                        ?>
                    </span>
                </div>
                <div id="info-der">
                    <h3>Ficha técnica <span class="ocultar">Ocultar</span></h3>
                    <div id='info-ficha'>
                        <?php
                        if(!empty($d["genero"])) {
                            echo "<div id='genero'><h4>Género</h4>";
                            echo $d["genero"]."</div>";
                        }
                        if(!empty($d["web"])) {
                            echo "<div id='web'><h4>Página web</h4> <a href='".$d["web"]."' target='_blank'>".$d["web"]."</a></div>";
                        }
                        ?>
                        <div class='share-links'>
                            <!-- Place this tag where you want the share button to render. -->
                            <div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php echo $ogurl; ?>"></div>
                            <div class="fb-share-button" data-href="<?php echo $ogurl; ?>" data-type="button_count"></div>
                            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $ogurl; ?>"  data-via="HazPlanes" data-lang="es">Twittear</a>
                        </div>
                        <?php
                        if(!empty($d["descripcion"])) {
                            echo "<h3>Descripción</h3><div id='descripcion'>";
                            echo "<p itemprop='description'>".htmlspecialchars_decode($d["descripcion"])."</p>";
                            echo "</div>";
                        }
                        if(!empty($d["notas"])) {
                        ?>
                        <h3>Notas</h3>
                        <div id='notas'>
                            <?php echo "<p>".htmlspecialchars_decode($d["notas"])."</p>"; ?>
                        </div>
                        <?php } ?>
                    </div>
                    <br>
                    <h3>Expuesto en</h3>
                    <div id='info-ficha'>
                    <?php
                    $tipohorarios = array("horasMuseo", "horasLocal", "horasAuditorio", "horasLugar");
                    foreach($tipohorarios as $th) {
                        switch($th) {
                            case "horasLocal":
                                $formfield = "locales";
                                $idtipo = "idlocal";
                                $url = "local";
                                break;
                            case "horasAuditorio":
                                $formfield = "auditorios";
                                $idtipo = "idteatro";
                                $url = "teatro";
                                break;
                            case "horasMuseo":
                                $formfield = "museos";
                                $idtipo = "idmuseo";
                                $url = "museo";
                                break;
                            case "horasLugar":
                                $formfield = "lugar";
                                $idtipo = "";
                                break;
                        }
                        foreach($d[$th] as $data) {
                            if(isset($data)) {
                                ?>
                                <div class='celllocal' itemprop='location'><span itemprop='name'>
                                    <?php
                                    if($th != "horasLugar") {
                                        echo $data["nombre"]." ";
                                        echo "<a href='".$url."/".$data[$idtipo]."-".urlAmigable($data["nombre"])."' class='nota'>+ info</a>";
                                    } else {
                                        echo $data["lugar"];
                                    } ?>
                                </div>
                                <?php
                                if(isset($data["permanente"]) && !$data["permanente"]) {
                                    echo "<div id='inicio'><h4>Fecha de inicio</h4>";
                                    echo "<time itemprop='startDate' datetime='".$data["inicio"]."'>".date("d-m-Y",strtotime($data["inicio"]))."</time></div>";
                                    echo "<div id='fin'><h4>Fecha de finalización</h4>";
                                    echo "<time itemprop='endDate' datetime='".$data["fin"]."'>".date("d-m-Y",strtotime($data["fin"]))."</time></div>";
                                } else {
                                    echo "<div><time itemprop='startDate' datetime='".date("Y-m-d", strtotime("now"))."'><h4>Exposicion permanente</h4></time></div>";
                                }
                                if($th == "horasMuseo") {
                                ?>
                                <div id="info-horas">
                                    <div id="horarios">
                                        <ul>
                                            <?php
                                            $options = array("idmuseo" => $data[$idtipo]);
                                            $museo = new Museos($options,$db);
                                            $d2 = $museo->readAction();
                                            $d2 = $d2[0];
                                            //Horarios
                                            $cerrados = explode(",",$d2["horario"][0]["cerradosueltos"]);
                                            unset($d2["horario"][0]["cerrado"],$d2["horario"][0]["cerradosueltos"],$d2["horario"][0]["idhorario"],$d2["horario"][0]["idlugar"],$d2["horario"][0]["tipolugar"]);
                                            foreach($d2["horario"][0] as $dia => $horario) {
                                                if($dia == "miercoles") {
                                                    $dia = "miércoles";
                                                } elseif($dia == "sabado") {
                                                    $dia = "sábado";
                                                }
                                                $day = $dia;
                                                $active = "";
                                                if(strtoupper($dias[date("N",strtotime("now"))-1]) == strtoupper($dia)) {
                                                    $dia = $dia." (Hoy)";
                                                    $active = "active";
                                                }
                                                echo "<li class='info-horas-day ".$active."'>
                                                        <div class='dayname'>".ucfirst($dia)."</div>";
                                                if(!empty($horario)) {
                                                    $jornada = explode(" y ",$horario);
                                                    if(isset($jornada[1])) {
                                                        $horario = $jornada[0]."<hr>".$jornada[1];
                                                    } else {
                                                        $horario = $jornada[0];
                                                    }
                                                }
                                                echo "  <div class='dayhoras'>";
                                                if(!empty($horario)) {
                                                    echo $horario;
                                                } else {
                                                    $numdia = array("Lunes" => 1,"Martes" => 2,"Miércoles" => 3,"Jueves" => 4,"Viernes" => 5,"Sábado" => 6,"Domingo" => 7);
                                                    if(in_array($numdia[ucfirst($day)], $cerrados)) {
                                                        echo "Cerrado";
                                                    } else {
                                                        echo "Sin determinar";
                                                    }
                                                }
                                                echo "</div>";
                                                echo "</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <?php
                                }
                            }
                        }
                    }
                    ?>
                    <?php
                    if($th == "horasLugar") {
                        infoMapLugares($d);
                    }
                    ?>
                    </div>
                </div>
            </section>
        </div>
    </section>