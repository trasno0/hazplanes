<?php
include_once("api.inc.php");
include("../scrapers/tmdb_v3.php");

$apikey="6bc2d4775411094abbccdcec00ac5bbe";
$language = 'es';

$tmdb_V3 = new TMDBv3($apikey,$language);

# Url de las imagenes
$urlImages = $tmdb_V3->getImageURL();

$debug = "";

if (isset($_POST['titulo'])) { // Forms
    $titulo = $_POST['titulo'];
} else {
    $titulo = "";
}

if(!empty($titulo)) {

    $title = str_replace("á","a",str_replace("é","e",str_replace("í","i",str_replace("ó","o",str_replace("ú","u",strtolower($titulo))))));

    //Recoger informacion de la pelicula en TMDB
    //Buscar Pelicula
    $buscar = $tmdb_V3->searchMovie($title,$language);
    $peliDB = array();
    foreach($buscar["results"] as $k => $value) {
        //ID de Pelicula
        $idMovie = $value["id"];
        $peliDB[$k]["ficha"] = "http://www.themoviedb.org/movie/".$idMovie;
        $peliDB[$k]["fichatmdb"] = "http://www.themoviedb.org/movie/".$idMovie;

        //Actores
        $pelinfo = $tmdb_V3->movieCast($idMovie);
        $peliDB[$k]["actores"] = $pelinfo["cast"];
        $peliDB[$k]["director"] = $pelinfo["director"];

        //Trailers
        $pelinfo = $tmdb_V3->movieTrailer($idMovie);
        if(isset($pelinfo["youtube"][0])) {
            $peliDB[$k]["trailer"] = "http://www.youtube.com/watch?v=".$pelinfo["youtube"][0]["source"];
        }

        //Detalles de la pelicula
        $pelinfo = $tmdb_V3->movieDetail($idMovie);
        if(!empty($pelinfo)) {
            $peliDB[$k]["fichaimdb"] = "http://www.imdb.com/title/".$pelinfo["imdb_id"];
            $peliDB[$k]["nombre"] = $pelinfo["title"];
            $peliDB[$k]["original"] = $pelinfo["original_title"];
            $peliDB[$k]["web"] = $pelinfo["homepage"];
            $peliDB[$k]["duracion"] = (int)$pelinfo["runtime"];
            $generos = array();
            foreach($pelinfo["genres"] as $genero) {
                $generos[] = $genero["name"];
            }
            $peliDB[$k]["genero"] = implode(", ",$generos);
            $paises = array();
            foreach($pelinfo["production_countries"] as $country) {
                $c = "";
                switch($country["iso_3166_1"]) {
                    case "ES":
                        $c = "España";
                        break;
                    case "US":
                        $c = "Estados Unidos";
                        break;
                    case "KR":
                        $c = "Corea del Sur";
                        break;
                    case "GB":
                        $c = "Reino Unido";
                        break;
                    case "PT":
                        $c = "Portugal";
                        break;
                    case "FR":
                        $c = "Francia";
                        break;
                    case "DE":
                        $c = "Alemania";
                        break;
                    case "IT":
                        $c = "Italia";
                        break;
                    case "IE":
                        $c = "Irlanda";
                        break;
                    case "SE":
                        $c = "Suecia";
                        break;
                    case "RU":
                        $c = "Rusia";
                        break;
                    case "VE":
                        $c = "Venezuela";
                        break;
                    case "AR":
                        $c = "Argentina";
                        break;
                    case "CA":
                        $c = "Canadá";
                        break;
                    case "DK":
                        $c = "Dinamarca";
                        break;
                    case "PL":
                        $c = "Polonia";
                        break;
                    case "JP":
                        $c = "Japón";
                        break;
                    case "IS":
                        $c = "Islandia";
                        break;
                    case "KH":
                        $c = "Camboya";
                        break;
                    case "BR":
                        $c = "Brasil";
                        break;
                    case "CH":
                        $c = "Suiza";
                        break;
                    case "AU":
                        $c = "Australia";
                        break;
                    case "IN":
                        $c = "India";
                        break;
                    case "IL":
                        $c = "Israel";
                        break;
                    case "HU":
                        $c = "Hungría";
                        break;
                    case "RO":
                        $c = "Rumanía";
                        break;
                    case "GR":
                        $c = "Grecia";
                        break;
                    case "BE":
                        $c = "Bélgica";
                        break;
                    case "CY":
                        $c = "Chipre";
                        break;
                    case "MT":
                        $c = "Malta";
                        break;
                    case "PS":
                        $c = "Palestina";
                        break;
                    case "CH":
                        $c = "China";
                        break;
                    case "HK":
                        $c = "Hong Kong";
                        break;
                    case "CL":
                        $c = "Chile";
                        break;
                    case "TR":
                        $c = "Turquía";
                        break;
                    case "NZ":
                        $c = "Nueva Zelanda";
                        break;
                    case "UY":
                        $c = "Uruguay";
                        break;
                    case "BG":
                        $c = "Bulgaria";
                        break;
                    case "AT":
                        $c = "Austria";
                        break;
                    case "NO":
                        $c = "Noruega";
                        break;
                    default:
                        $c = $country["iso_3166_1"];
                }
                if(!empty($c)) {
                    $paises[] = $c;
                }
            }
            $peliDB[$k]["nacionalidad"] = implode(", ",$paises);
            $release = $tmdb_V3->movieRelease($idMovie);
            if(isset($release["date"])) {
                $peliDB[$k]["fechaestreno"] = $release["date"];
            }
            if(isset($release["age"])) {
                $peliDB[$k]["edad"] = (int)$release["age"];
            }
            $peliDB[$k]["sinopsis"] = $pelinfo["overview"];
            $peliDB[$k]["fanart"] = $urlImages.$pelinfo["backdrop_path"];
            $peliDB[$k]["poster"] = $urlImages.$pelinfo["poster_path"];
            $peliDB[$k]["fanart2"] = $urlImages.$pelinfo["backdrop_path"];
            $peliDB[$k]["poster2"] = $urlImages.$pelinfo["poster_path"];
        }
    }
    //Fin recoger info TMDB
    $msg = array("success" => "ok","data" => $peliDB, "debug" => $debug);
} else {
    $msg = array("success" => "ko","data" => "Título vacío", "debug" => $debug);
}

echo json_encode($msg);
?>