<?php
pageAccessControl();
?>
    <section id='content'>
        <div class="grid">
            <section id="main">
                <h2>Oops, el elemento que intentas ver no existe.</h2>
                <p>Ponte en contacto con nosotros si sabes que existe pero no puedes verlo.</p>
                <p><a href='javascript:history.back()'>Volver a la página anterior</a></p>
            </section>
        </div>
    </section>