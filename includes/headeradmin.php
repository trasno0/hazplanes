<?php
// Check for a $page_title value:
if (!isset($page_title)) $page_title = 'HazPlanes';

include_once("admin.forms.inc.php");

$menuSel = array();
$menuSel = array_pad($menuSel,13,"");
switch(substr($p,0,11)) {
    case "adminlistci":
    case "adminformci":
    case "adminlistfi":
    case "adminformfi":
    case "adminlistpr":
    case "adminformpr":
    case "adminlisten":
    case "adminformen":
        $menuSel[1] = "class='current'";
        $menuTit = "Cartelera";
        break;
    case "adminlistco":
    case "adminformco":
    case "adminlistlo":
    case "adminformlo":
    case "adminlistth":
    case "adminformth":
        $menuSel[2] = "class='current'";
        $menuTit = "Conciertos";
        break;
    case "adminlistte":
    case "adminformob":
        $menuSel[3] = "class='current'";
        $menuTit = "Teatro";
        break;
    case "adminlistev":
    case "adminformev":
    case "adminlistlu":
    case "adminformlu":
        $menuSel[4] = "class='current'";
        $menuTit = "Eventos";
        break;
    case "adminlistex":
    case "adminformex":
    case "adminlistmu":
    case "adminformmu":
        $menuSel[5] = "class='current'";
        $menuTit = "Exposiciones";
        break;
    case "adminlistde":
    case "adminformde":
    case "adminlistpa":
    case "adminformpa":
        $menuSel[9] = "class='current'";
        $menuTit = "Deportes";
        break;
    case "adminlistcu":
    case "adminformcu":
        $menuSel[12] = "class='current'";
        $menuTit = "Cursos";
        break;
    case "adminupload":
        $menuSel[6] = "class='current'";
        $menuTit = "Recomendaciones";
        break;
    case "admincontac":
        $menuSel[7] = "class='current'";
        $menuTit = "Contacto";
        break;
    case "adminabout":
        $menuSel[8] = "class='current'";
        $menuTit = "Acerca de...";
        break;
    default:
        $menuSel[0] = "class='current'";
        $menuTit = "Inicio";
        break;
}

?>
<!doctype html>
<html>
<head>
    <meta name="author" content="Aper" />
    <meta charset="utf-8" />
    <meta name="robots" content="none">

    <title><?php echo $page_title; ?></title>

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="styles/styles.css" />
    <link rel='stylesheet' type='text/css' href='styles/admin.css' />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="js/admin.js"></script>
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDatF3wwcD0PS5IIKl4E1t8XQVpfEfbiQs&sensor=false&libraries=places"></script>-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
    <script type="text/javascript" src="js/maps.js"></script>
    <?php if($p != "adminscraperjsontobd" && $p != "adminscraperwebtojson" && $p != "adminscraperjsonestrenostobd") { ?>
    <script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        plugins: "code link paste",
        toolbar: "undo redo removeformat | alignleft aligncenter alignright alignjustify | bold italic underline | fontsizeselect | bullist numlist outdent indent | code link unlink",
        width: 395,
        height: 150,
        menubar: false,
        statusbar: false,
        forced_root_block : "",
        force_br_newlines : true,
        force_p_newlines : false,
        target_list: [
            {title: 'Nueva pestaña', value: '_blank'},
            {title: 'Ninguno', value: ''}
        ],
        paste_auto_cleanup_on_paste : true,
        paste_remove_styles: true,
        paste_remove_styles_if_webkit: true,
        paste_strip_class_attributes: true,
        paste_preprocess : function(pl, o) {
          o.content = strip_tags( o.content,'<a><p><br><li><ul>' );
          // remove all tags => plain text
          //o.content = strip_tags( o.content,'' );
        }
     });

    function strip_tags (str, allowed_tags){
        var key = '', allowed = false;
        var matches = [];    var allowed_array = [];
        var allowed_tag = ''; var i = 0; var k = ''; var html = '';
        var replacer = function (search, replace, str) {
            return str.split(search).join(replace);
        };
        // Build allowes tags associative array
        if (allowed_tags) {
            allowed_array = allowed_tags.match(/([a-zA-Z0-9]+)/gi);
        }
        str += '';
        // Match tags
        matches = str.match(/(<\/?[\S][^>]*>)/gi);
        // Go through all HTML tags
        for (key in matches) {
            if (isNaN(key)) { // IE7 Hack
                continue;
            }
            // Save HTML tag
            html = matches[key].toString();
            // Is tag not in allowed list? Remove from str!
            allowed = false;
            // Go through all allowed tags
            for (k in allowed_array) {            // Init
                allowed_tag = allowed_array[k];
                i = -1;
                if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+'>');}
                if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+' ');}
                if (i != 0) { i = html.toLowerCase().indexOf('</'+allowed_tag)   ;}
                // Determine
                if (i == 0) {                allowed = true;
                    break;
                }
            }
            if (!allowed) {
                str = replacer(html, "", str); // Custom replace. No regexing
            }
        }
        return str;
    }
    </script>
    <?php } ?>
</head>
<body>
    <?php include("menulateraladmin.php") ?>
    <?php include("menuheaderadmin.php") ?>
    <!-- End of header. -->
