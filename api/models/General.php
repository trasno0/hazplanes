<?php
/**
 * Clase con metodos para operaciones comunes a varios modelos
 *
 * @package API
 * @author Trasno
 */
class General {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }
    
    /**
     * Coge todos los eventos del dia actual
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de usuario. El nombre de usuario tiene que contener esta cadena.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listAgendaHoy($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $expoFilter = "1";
        if(strpos($options["filter"],"between") === -1) {
            $expoFilter = "((inicio <= CURDATE() + INTERVAL 1 DAY and fin >= CURDATE() + INTERVAL 1 DAY) or permanente = 1) and WEEKDAY(NOW() + INTERVAL 1 DAY)+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
        } else {
            $between = explode(" ",$options["filter"]);
            if($between[3] == "now()" || $between[6] == "now()") {
                $expoFilter = "((inicio <= CURDATE() and fin >= CURDATE()) or permanente = 1) and WEEKDAY(NOW())+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
            } elseif($between[1] == "(fecha") {
                $expoFilter = "((inicio <= ".$between[3]."' and fin >= ".$between[3]."') or permanente = 1) and WEEKDAY(".$between[3]."')+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
            }
        }
        if($expoFilter == 1) {
            if(strpos($options["filter"],"infantil") !== -1) {
                $expoFilter .= " and infantil = 1";
            } else {
                $expoFilter .= " and infantil = 0";
            }
        }
        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, tipoevento from (
        
        select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slocales_has_conciertos order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %steatros_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %scines_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slugares_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %smuseos_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        
        select p.idpelicula, nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas' from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %scines_has_peliculas order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas' from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slocales_has_peliculas order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas' from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %steatros_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas' from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slugares_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas' from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %smuseos_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by p.idpelicula union
        
        select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro' from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slocales_has_obrasteatro order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro' from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %steatros_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro' from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %scines_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro' from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slugares_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro' from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %smuseos_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        
        select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos' from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %slocales_has_eventos le order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos' from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia from %steatros_has_eventos te order by fecha asc) as te where te.ideventos = e.ideventos %s group by te.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos' from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia from %scines_has_eventos te order by fecha asc) as te where te.ideventos = e.ideventos %s group by te.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos' from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %slugares_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos' from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %smuseos_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %slocales_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %steatros_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %slugares_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %scines_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %smuseos_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %spabellones_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
                
        select e.idcurso, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'formacion' from %scursos e, (select le.idcurso, fecha, agotado, cancelado, todoeldia from %slocales_has_cursos le order by fecha asc) as le where le.idcurso = e.idcurso %s group by le.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'formacion' from %scursos e, (select te.idcurso, fecha, agotado, cancelado, todoeldia from %steatros_has_cursos te order by fecha asc) as te where te.idcurso = e.idcurso %s group by te.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion' from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %slugares_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion' from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %scines_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion' from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %smuseos_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones' from %sexposiciones e, %smuseos_has_exposiciones me where me.idexposicion = e.idexposicion and %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones' from %sexposiciones e, %slocales_has_exposiciones me where me.idexposicion = e.idexposicion and %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones' from %sexposiciones e, %steatros_has_exposiciones me where me.idexposicion = e.idexposicion and %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones' from %sexposiciones e, %slugares_has_exposiciones me where me.idexposicion = e.idexposicion and %s and publicado = 1
        
        ) as consulta group by nombre, date(fecha)
        order by %s %s", 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $expoFilter, 
        BDPREFIX, BDPREFIX, str_replace("idmuseo", "idlocal", $expoFilter),
        BDPREFIX, BDPREFIX, str_replace("idmuseo", "idteatro", $expoFilter),
        BDPREFIX, BDPREFIX, str_replace(" and idlugar=me.idmuseo", "", $expoFilter),
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        //echo $query."</br>";
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge todos los eventos de un lugar
     *
     * @param array $options
     * @param int $options["idlugar"] id de lugar.
     * @param int $options["tipolugar"] id de lugar.
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listNextEvents($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $expoFilter = "";
        if(strpos($options["filter"],"fecha >= ") === false) {
            $expoFilter = " and fin < CURDATE() and permanente <> 1";
        } else {
            $expoFilter = " and (fin >= CURDATE() or permanente = 1)";
        }
        switch($options["tipolugar"]) {
            case "local":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slocales_has_conciertos where idlocal = %d order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slocales_has_peliculas where idlocal = %d order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slocales_has_obrasteatro where idlocal = %d order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %slocales_has_eventos le where idlocal = %d order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %slocales_has_deportes lc where idlocal = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %slocales_has_cursos lc where idlocal = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso union
                select e.idexposicion, nombre, genero, poster, DATE_FORMAT(fin,'%%d %%b %%Y 00:00'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %slocales_has_exposiciones me where me.idlocal = %d and me.idexposicion = e.idexposicion %s and publicado = 1
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $expoFilter);
                break;
            case "cine":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %scines_has_conciertos where idcine = %d order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %scines_has_peliculas where idcine = %d order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s group by date(cp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %scines_has_obrasteatro where idcine = %d order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %scines_has_eventos lue where idcine = %d order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %scines_has_deportes lc where idcine = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %scines_has_cursos lc where idcine = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"]);
                break;
            case "teatro":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %steatros_has_conciertos where idteatro = %d order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %steatros_has_peliculas where idteatro = %d order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %steatros_has_obrasteatro where idteatro = %d order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %steatros_has_eventos le where idteatro = %d order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %steatros_has_deportes lc where idteatro = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %steatros_has_cursos lc where idteatro = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso union
                select e.idexposicion, nombre, genero, poster, DATE_FORMAT(fin,'%%d %%b %%Y 00:00'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %steatros_has_exposiciones me where me.idteatro = %d and me.idexposicion = e.idexposicion %s and publicado = 1
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $expoFilter);
                break;
            case "museo":
                $subquery = sprintf("
                select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %smuseos_has_conciertos where idmuseo = %d order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
                select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %smuseos_has_peliculas where idmuseo = %d order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
                select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %smuseos_has_obrasteatro where idmuseo = %d order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
                select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %smuseos_has_eventos le where idmuseo = %d order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %smuseos_has_deportes lc where idmuseo = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte union
                select c.idcurso, nombre, tipo, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos c, (select lc.idcurso, fecha, agotado, cancelado, todoeldia from %smuseos_has_cursos lc where idmuseo = %d order by fecha asc) as lc where lc.idcurso = c.idcurso %s group by lc.fecha, c.idcurso union
                select e.idexposicion, nombre, genero, poster, DATE_FORMAT(fin,'%%d %%b %%Y 00:00'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %smuseos_has_exposiciones me where me.idmuseo = %d and me.idexposicion = e.idexposicion %s and publicado = 1
                ",
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"],
                BDPREFIX, BDPREFIX, $options["idlugar"], $expoFilter);
                break;
            case "pabellon":
                $subquery = sprintf("
                select c.iddeporte as id, c.nombre, td.idtipodeporte as genero, c.poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'deportes' as tipoevento, creado, actualizado from %sdeportes c LEFT JOIN %stipos_deporte td on td.idtipodeporte = c.tipo, (select lc.iddeporte, fecha, agotado, cancelado, todoeldia from %spabellones_has_deportes lc where idpabellon = %d order by fecha asc) as lc where lc.iddeporte = c.iddeporte %s group by lc.fecha, c.iddeporte
                ",
                BDPREFIX, BDPREFIX, BDPREFIX, $options["idlugar"], $options["filter"]);
                break;
        }
        
        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, tipoevento, creado, actualizado from (%s) as consulta order by %s %s", 
         
        $subquery,
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        
        //echo $query."</br>";
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }
    
    /**
     * Coge todas las novedades o actualizaciones de eventos
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listNovedades($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, tipoevento, creado, actualizado from (
        
        select c.idconcierto as id, nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slocales_has_conciertos order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %steatros_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %scines_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %slugares_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', creado, actualizado from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia from %smuseos_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        
        select p.idpelicula, nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %scines_has_peliculas order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s group by date(cp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slocales_has_peliculas order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %steatros_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by date(tp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slugares_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by date(tp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', creado, actualizado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %smuseos_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by date(tp.fecha), p.idpelicula union
        
        select t.idobrateatro, nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slocales_has_obrasteatro order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %steatros_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %scines_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %slugares_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', creado, actualizado from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia from %smuseos_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        
        select e.ideventos, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia from %slocales_has_eventos le order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia from %steatros_has_eventos te order by fecha asc) as te where te.ideventos = e.ideventos %s group by te.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %slugares_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %scines_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        select e.ideventos, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', creado, actualizado from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia from %smuseos_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %slocales_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %steatros_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %slugares_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %scines_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %smuseos_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', creado, actualizado from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia from %spabellones_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        
        select e.idcurso, nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos e, (select le.idcurso, fecha, agotado, cancelado, todoeldia from %slocales_has_cursos le order by fecha asc) as le where le.idcurso = e.idcurso %s group by le.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos e, (select te.idcurso, fecha, agotado, cancelado, todoeldia from %steatros_has_cursos te order by fecha asc) as te where te.idcurso = e.idcurso %s group by te.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %slugares_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %scines_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', creado, actualizado from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia from %smuseos_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %smuseos_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %slocales_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %steatros_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1 union
        select e.idexposicion, nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', creado, actualizado from %sexposiciones e, %slugares_has_exposiciones me where me.idexposicion = e.idexposicion %s and publicado = 1
        
        ) as consulta group by date(actualizado), nombre
        order by %s %s", 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"],
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        //echo $query."</br>";
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge todos los eventos de hoy y sus coordenadas
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de usuario. El nombre de usuario tiene que contener esta cadena.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listAgendaHoyCoord($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $expoFilter = "1";
        if(strpos($options["filter"],"between") === -1) {
            $expoFilter = "((inicio <= CURDATE() + INTERVAL 1 DAY and fin >= CURDATE() + INTERVAL 1 DAY) or permanente = 1) and WEEKDAY(NOW() + INTERVAL 1 DAY)+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
        } else {
            $between = explode(" ",$options["filter"]);
            if($between[3] == "now()" || $between[6] == "now()") {
                $expoFilter = "((inicio <= CURDATE() and fin >= CURDATE()) or permanente = 1) and WEEKDAY(NOW())+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
            } elseif($between[1] == "(fecha") {
                $expoFilter = "((inicio <= ".$between[3]."' and fin >= ".$between[3]."') or permanente = 1) and WEEKDAY(".$between[3]."')+1 not in (select cerrado from horarios where tipolugar='museo' and idlugar=me.idmuseo)";
            }
        }
        if($expoFilter == 1) {
            if(strpos($options["filter"],"infantil") !== -1) {
                $expoFilter .= " and infantil = 1";
            } else {
                $expoFilter .= " and infantil = 0";
            }
        }
        $query = sprintf("select id, nombre, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, tipoevento, coordenadas from (
        
        select c.idconcierto as id, c.nombre, genero, poster, lc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos' as tipoevento, coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_conciertos lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lc where lc.idconcierto = c.idconcierto %s group by lc.fecha, c.idconcierto union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_conciertos lo, %steatros l where lo.idteatro = l.idteatro order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_conciertos lo, %scines l where lo.idcine = l.idcine order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_conciertos order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        select c.idconcierto, c.nombre, genero, poster, tc.fecha, infantil, agotado, cancelado, todoeldia, 'conciertos', coordenadas from %sconciertos c, (select idconcierto, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_conciertos lo, %smuseos l where lo.idmuseo = l.idmuseo order by fecha asc) as tc where tc.idconcierto = c.idconcierto %s group by tc.fecha, c.idconcierto union
        
        select p.idpelicula, p.nombre, genero, poster, cp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_peliculas lo, %scines l where lo.idcine = l.idcine order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, p.nombre, genero, poster, lp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_peliculas lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, p.nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_peliculas lo, %steatros l where lo.idteatro = l.idteatro order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, p.nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by p.idpelicula union
        select p.idpelicula, p.nombre, genero, poster, tp.fecha, infantil, agotado, cancelado, todoeldia, 'peliculas', coordenadas from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_peliculas lo, %smuseos l where lo.idmuseo = l.idmuseo order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by p.idpelicula union
        
        select t.idobrateatro, t.nombre, genero, poster, lt.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_obrasteatro lc, %slocales l where lc.idlocal = l.idlocal order by fecha asc) as lt where lt.idobrateatro = t.idobrateatro %s group by lt.fecha, t.idobrateatro union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_obrasteatro lo, %steatros l where lo.idteatro = l.idteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_obrasteatro lo, %scines l where lo.idcine = l.idcine order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_obrasteatro order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        select t.idobrateatro, t.nombre, genero, poster, at.fecha, infantil, agotado, cancelado, todoeldia, 'obrasteatro', coordenadas from %sobrasteatro t, (select idobrateatro, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_obrasteatro lo, %smuseos l where lo.idmuseo = l.idmuseo order by fecha asc) as at where at.idobrateatro = t.idobrateatro %s group by at.fecha, t.idobrateatro union
        
        select e.ideventos, e.nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', coordenadas from %seventos e, (select le.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_eventos le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.ideventos = e.ideventos %s group by le.fecha, e.ideventos union
        select e.ideventos, e.nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', coordenadas from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_eventos te, %steatros l where te.idteatro = l.idteatro order by fecha asc) as te where te.ideventos = e.ideventos %s group by te.fecha, e.ideventos union
        select e.ideventos, e.nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', coordenadas from %seventos e, (select te.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_eventos te, %scines l where te.idcine = l.idcine order by fecha asc) as te where te.ideventos = e.ideventos %s group by te.fecha, e.ideventos union
        select e.ideventos, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', coordenadas from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_eventos lue order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        select e.ideventos, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'eventos', coordenadas from %seventos e, (select lue.ideventos, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_eventos lue, %smuseos l where lue.idmuseo = l.idmuseo order by fecha asc) as lue where lue.ideventos = e.ideventos %s group by lue.fecha, e.ideventos union
        
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_deportes le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_deportes le, %steatros l where le.idteatro = l.idteatro order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_deportes le order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_deportes le, %scines l where le.idcine = l.idcine order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_deportes le, %smuseos l where le.idmuseo = l.idmuseo order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
        select e.iddeporte, e.nombre, td.idtipodeporte as tipo, e.poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'deportes', coordenadas from %sdeportes e LEFT JOIN %stipos_deporte td on td.idtipodeporte = e.tipo, (select le.iddeporte, fecha, agotado, cancelado, todoeldia, coordenadas from %spabellones_has_deportes le, %spabellones l where le.idpabellon = l.idpabellon order by fecha asc) as le where le.iddeporte = e.iddeporte %s group by le.fecha, e.iddeporte union
                
        select e.idcurso, e.nombre, tipo, poster, le.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', coordenadas from %scursos e, (select le.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %slocales_has_cursos le, %slocales l where le.idlocal = l.idlocal order by fecha asc) as le where le.idcurso = e.idcurso %s group by le.fecha, e.idcurso union
        select e.idcurso, e.nombre, tipo, poster, te.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', coordenadas from %scursos e, (select te.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %steatros_has_cursos te, %steatros l where te.idteatro = l.idteatro order by fecha asc) as te where te.idcurso = e.idcurso %s group by te.fecha, e.idcurso union
        select e.idcurso, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', coordenadas from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %slugares_has_cursos lue order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', coordenadas from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %scines_has_cursos lue, %scines l where lue.idcine = l.idcine order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        select e.idcurso, e.nombre, tipo, poster, lue.fecha, infantil, agotado, cancelado, todoeldia, 'formacion', coordenadas from %scursos e, (select lue.idcurso, fecha, agotado, cancelado, todoeldia, coordenadas from %smuseos_has_cursos lue, %smuseos l where lue.idmuseo = l.idmuseo order by fecha asc) as lue where lue.idcurso = e.idcurso %s group by lue.fecha, e.idcurso union
        
        select e.idexposicion, e.nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', coordenadas from %sexposiciones e, %smuseos_has_exposiciones me, %smuseos l where me.idmuseo = l.idmuseo and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', coordenadas from %sexposiciones e, %slocales_has_exposiciones me, %slocales l where me.idlocal = l.idlocal and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', coordenadas from %sexposiciones e, %steatros_has_exposiciones me, %steatros l where me.idteatro = l.idteatro and me.idexposicion = e.idexposicion and %s and e.publicado = 1 union
        select e.idexposicion, e.nombre, genero, poster, concat(CURDATE() + INTERVAL 1 DAY,' 01:59'), infantil, agotado, cancelado, '1', 'exposiciones', coordenadas from %sexposiciones e, %slugares_has_exposiciones me where me.idexposicion = e.idexposicion and %s and e.publicado = 1
        
        ) as consulta group by nombre, date(fecha)
        order by %s %s", 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], 
        BDPREFIX, BDPREFIX, BDPREFIX, $expoFilter, 
        BDPREFIX, BDPREFIX, BDPREFIX, str_replace("idmuseo", "idlocal", $expoFilter),
        BDPREFIX, BDPREFIX, BDPREFIX, str_replace("idmuseo", "idteatro", $expoFilter),
        BDPREFIX, BDPREFIX, str_replace(" and idlugar=me.idmuseo", "", $expoFilter),
        $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        //echo $query."</br>";
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }
    
    /**
     * Coge todos los horarios de apertura de un sitio
     *
     * @param array $options
     * @param int $options["idlugar"] id del lugar seleccionado
     * @param string $options["tipolugar"] tipo del lugar seleccionado (museo, local, cine...)
     * @return array
     */
    public function listHorarios($options) {
        $query = sprintf("select h.* from %shorarios h where h.idlugar = %d and h.tipolugar = '%s'", BDPREFIX, $this->db->secure_field($options["idlugar"]), $this->db->secure_field($options["tipolugar"]));
        $r = $this->db->query($query);

        $result = array();
        while($horarios = $this->db->fetch($r)) {
            $result[] = $horarios;
        }

        return $result;
    }

    /*
     * Eliminar un horario de apertura
     *
     * @param array $options
     * @param int $options["idlugar"] id del lugar a eliminar
     * @param string $options["tipolugar"] tipo del lugar a eliminar (museo, local, cine...)
     * @return boolean
     */
    public function deleteHorario($options) {
        if(!empty($options["idlugar"]) && !empty($options["tipolugar"])) {
            $query = sprintf("delete from %shorarios where idlugar = %d and tipolugar = '%s'",BDPREFIX, $this->db->secure_field($options["idlugar"]), $this->db->secure_field($options["tipolugar"]));
            $r = $this->db->execute($query);
            return true;
        }
        return false;
    }

    /*
     * Insertar un horario de apertura
     *
     * @param array $horario datos del horario
     * @param int $horario['idlugar'] id del lugar que tiene este horario
     * @param string $horario['tipolugar'] tipo del lugar que tiene este horario (museo, local, cine...)
     * @param string $horario['lunes']
     * @param string $horario['martes']
     * @param string $horario['miercoles']
     * @param string $horario['jueves']
     * @param string $horario['viernes']
     * @param string $horario['sabado']
     * @param string $horario['domingo']
     * @param string $horario['cerrado']
     * @param string $horario['cerradosueltos']
     * @return int
     */
    public function addHorario($horario) {
        if(!$this->checkHorario($horario)) {
            $fields = "";
            $values = "";
            foreach($horario as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %shorarios (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addHorario] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHorario] Ya existe el horario.", 1);
        }
    }

    /*
     * Actualiza los campos de una horario de apertura
     *
     * @param array $horario datos del horario
     * @param int $horario['idlugar'] id del lugar que tiene este horario
     * @param string $horario['tipolugar'] tipo del lugar que tiene este horario (museo, local, cine...)
     * @param string $horario['lunes']
     * @param string $horario['martes']
     * @param string $horario['miercoles']
     * @param string $horario['jueves']
     * @param string $horario['viernes']
     * @param string $horario['sabado']
     * @param string $horario['domingo']
     * @param string $horario['cerrado']
     * @param string $horario['cerradosueltos']
     * @return boolean
     */
    public function updateHorario($horario) {
        if($this->checkHorario($horario)) {
            $fields = "";
            $exclude = array("idlugar","idhorario","tipolugar");
            foreach($horario as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %shorarios set %s where idlugar = %d and tipolugar = '%s'", BDPREFIX, $fields, $this->db->secure_field($horario["idlugar"]), $this->db->secure_field($horario["tipolugar"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHorario] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHorario] No existe el horario.", 1);
        }
    }
    
    /*
     * Comprueba si existe el horario de apertura
     *
     * @param array $options
     * @param string $options["idlugar"] id del lugar
     * @param string $options["tipolugar"] tipo del lugar (museo, local, cine...)
     * @return int|false
     */
    private function checkHorario($options) {
        $query = sprintf("select h.idlugar from %shorarios h where h.idlugar = %d and h.tipolugar = '%s'", BDPREFIX, $this->db->secure_field($options["idlugar"]), $this->db->secure_field($options["tipolugar"]));
        $r = $this->db->query($query);
        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
    
    /*
     * Eliminar una imagen de un formulario
     *
     * @param int $id id al que pertenece el elemento
     * @param string $path ruta de la imagen
     * @return boolean
     */
    public function deleteImage($id,$path) {
        if(!empty($id) && !empty($path)) {
            if(strpos($path,"poster") != false) {
                $field = "poster";
            } elseif(strpos($path,"fanart") != false) {
                $field = "fanart";
            } else {
                $field = "imagen";
            }
            $img = explode("/",$path);
            switch($img[1]) {
                case "cines":
                    $table = "cines";
                    $idfield = "idcine";
                    break;
                case "conciertos":
                    $table = "conciertos";
                    $idfield = "idconcierto";
                    break;
                case "cursos":
                    $table = "cursos";
                    $idfield = "idcurso";
                    break;
                case "deportes":
                    $table = "deportes";
                    $idfield = "iddeporte";
                    break;
                case "entradas":
                    $table = "entradas";
                    $idfield = "identradas";
                    break;
                case "eventos":
                    $table = "eventos";
                    $idfield = "idevento";
                    break;
                case "expos":
                    $table = "exposiciones";
                    $idfield = "idexposicion";
                    break;
                case "locales":
                    $table = "locales";
                    $idfield = "idlocal";
                    break;
                case "museos":
                    $table = "museos";
                    $idfield = "idmuseo";
                    break;
                case "obrasteatro":
                    $table = "obrasteatro";
                    $idfield = "idobrateatro";
                    break;
                case "pabellones":
                    $table = "pabellones";
                    $idfield = "idpabellon";
                    break;
                case "peliculas":
                    $table = "peliculas";
                    $idfield = "idpelicula";
                    break;
                case "promos":
                    $table = "promociones";
                    $idfield = "idpromocion";
                    break;
                case "teatros":
                    $table = "teatros";
                    $idfield = "idteatro";
                    break;
            }
            $query = sprintf("update %s set %s = '' where %s = %d and %s = '%s'",BDPREFIX.$table, $field, $idfield, $this->db->secure_field($id), $field, $this->db->secure_field($path));
            $r = $this->db->execute($query);
            if($r) {
                //Eliminamos imagen
                if(!unlink(BASE_URI.$path)) {
                    return false;
                }
                //Si es poster eliminamos la miniatura
                if($field == "poster") {
                    $img[4] = $img[3];
                    $img[3] = "thumbs";
                    $path = implode("/",$img);
                    if(!unlink(BASE_URI.$path)) {
                        return false;
                    }
                }
                //Si es poster o imagen eliminamos el original
                if($field != "fanart") {
                    $img[4] = $img[3];
                    $img[3] = "original";
                    $path = implode("/",$img);
                    if(!unlink(BASE_URI.$path)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
}
// END