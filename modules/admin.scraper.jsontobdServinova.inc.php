<?php
pageAccessControl(1);

try {
    if(isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        unset($newitem["search_db"], $newitem["search_tmdb"], $newitem["search_fa"]);
        $id = formAction($newitem, $action, "idpelicula");
    }
    if($totalPelis < 1) {
        $titulo = "";
        $horas = array();
    }
    $msg['success'] = true;
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}

?>
    <style>
        table { width: 100%; }
        td { vertical-align:top; }
        th { background-color: #ddd; color: black; padding: 10px 0; }
        .form { background: none; margin-top: 0;}
        form label { display:inline-block; width: 200px; height: auto; text-align:left; }
        input, textarea { width: 69%; }
        textarea { height: 120px; }
        button { vertical-align: top; }
        .medium { min-width: 80px; max-width: 150px; }
        .small { min-width: 50px; max-width: 50px; }
        //.checkbox { display: inline-block; width:initial; }
    </style>
    <section id='content' ng-app>
        <div class="grid">
            <section id='datos' ng-controller="Controller">
                <?php
                if(!$msg['success']) {
                    echo "Error: ".$msg['errormsg'];
                }
                ?>
                <h2><?php echo $page_title; ?></h2>
                <form name="newpeliculadb" id="newpeliculadb" class="form" action="index.php?p=adminscraperjsontobdservinova&pag=<?php echo $pag+1; ?>" method="post" enctype="multipart/form-data">
                    <?php
                    echo "<div class='paginacion' style='margin: auto; text-align: center; display: block;'>
                            <table style='width: 300px; margin: auto;'>
                            <tr>
                                <td style='width: 100px;'>";
                    if($pag > 1) {
                        echo '<a href="index.php?p=adminscraperjsontobdservinova&pag='.($pag-1).'"><< Anterior</a>';
                    }
                    echo "</td><td style='width: 100px;'>".$pag." de ".$totalPelis."</td><td style='width: 100px;'>";
                    if($pag < $totalPelis) {
                        echo '<a href="index.php?p=adminscraperjsontobdservinova&pag='.($pag+1).'">Siguiente >></a>';
                    }
                    echo "</td></tr></table></div>";
                    ?>
                    <select name="tipoevento">
                        <option value="pelicula">Película</option>
                        <option value="concierto">Concierto</option>
                        <option value="obrateatro">Obra de teatro</option>
                        <option value="evento">Evento</option>
                        <option value="curso">Curso</option>
                        <option value="deporte">Deporte</option>
                        <option value="exposicion">Exposición</option>
                    </select>
                    <table style=''>
                    <tr>
                    <?php
                    $cols = array("db");
                    foreach($horas as $k => $hora) {
                        if($k == "servinova") {
                            continue;
                        }
                        foreach($hora as $h) {
                    ?>
                        <td>
                            <input type="hidden" name="a" ng-value="action"/>
                            <div>
                                <label for="form-nombre">Nombre</label>
                                <input type="text" name="nombre" id="form-nombre" required='required' class="campo" value='<?php echo $titulo; ?>' />
                            </div>
                            <div>
                                <label for="form-descripcion">Descripción</label>
                                <textarea name="descripcion" id="form-descripcion" class="campo"><?php echo $h["descripcion"]; ?></textarea>
                            </div>
                            <div id='poster'>
                                <label for="form-posterURL">Poster desde URL</label>
                                <input type="text" name="posterURL" id="form-posterURL" class="campo" value="<?php echo $h["image"]; ?>" />
                                <br>
                                <p style='margin: auto; text-align:center;'><img src='<?php echo $h["image"]; ?>' alt='Poster de la película' style='max-width: 300px; max-height: 200px;'/></p>
                            </div>
                            <br><br>
                            <div class='divbig'>
                                <div style='display: inline-block;'>
                                    <label for="form-infantil">Infantil</label>
                                    <span class="checkbox">
                                        <input type="checkbox" id="form-infantil" name="infantil" ng-model='<?php echo $k; ?>.infantil' ng-true-value="1" />
                                        <label class="check" for="form-infantil"></label>
                                    </span>
                                </div><br><br>
                                <div style='display: inline-block;'>
                                    <label for="form-publicado">Publicado</label>
                                    <span class="checkbox">
                                        <input type="checkbox" id="form-publicado" name="publicado" ng-model='<?php echo $k; ?>.publicado' ng-true-value="1" />
                                        <label class="check" for="form-publicado"></label>
                                    </span>
                                </div>
                            </div>
                        </td>
                    <?php }
                    } ?>
                    </tr>
                    </table>
                    <p>
                        <button type="submit" form="newpeliculadb">Guardar</button>
                    </p>
                    <?php
                    //Cogemos los horarios que hay en el JSON
                    $horasJSON = array();
                    foreach($horas as $nombrelugar => $fechas) {
                        foreach($fechas as $fecha => $horitas) {
                            $fecha = date("Y-m-d", $fecha);
                            $diasemana = date("N", strtotime($fecha));
                            foreach($horitas as $key => $hora) {
                                if(strlen($hora["hora"]) > 5) {
                                    $hora["hora"] = substr($hora["hora"],0,-3);
                                }
                                if(strlen($hora["hora"]) < 5) {
                                    $hora["hora"] = "0".$hora["hora"];
                                }
                                $hora["hora"] = str_replace(".",":",$hora["hora"]);
                                $ho[] = array(
                                    "child" => array(
                                        array(
                                            "horas" => array(
                                                "fecha" => $fecha." ".$hora["hora"],
                                                "precio" => $hora["precio"],
                                                "urlcompra" => $hora["url"]
                                            )
                                        )
                                    )
                                );
                            }
                        }
                        $horasJSON = array_merge($horasJSON,$ho);
                    }
                    $horasCineScraper = json_encode($horasJSON);
                    //Fin horarios en JSON
                    ?>
                    <fieldset>
                        <legend>Horarios</legend>
                        <div ng-repeat="cine in cines">
                            <fieldset style="display:block">
                                <legend>Horarios</legend>
                                <br><button type="button" ng-click="addHoraCine(cine)">Añadir hora</button><br>
                                <div ng-repeat="hora in cine.child" style='width: 100%;'>
                                    Fecha: <input type="datetime" required="required" name="cines[{{$parent.$index}}][{{$index}}][fecha]" class="campo medium" ng-model="hora.horas.fecha" />
                                    URL de entradas: <input type="text" name="cines[{{$parent.$index}}][{{$index}}][urlcompra]" class="campo" style='min-width: 160px; width: 160px;' ng-model="hora.horas.urlcompra" />
                                    Precio: <input type="text" name="cines[{{$parent.$index}}][{{$index}}][precio]" class="campo small" ng-model="hora.horas.precio" />
                                    <button class="campo small" ng-click="removeHoraCine(cine,hora)">&times;</button>
                                    <hr>
                                </div>
                                <br><button type="button" ng-click="addHoraCine(cine)">Añadir hora</button><br>
                            </fieldset>
                        </div>
                    </fieldset>
                    <?php
                    echo "<div class='paginacion' style='margin: auto; text-align: center; display: block;'>
                            <table style='width: 300px; margin: auto;'>
                            <tr>
                                <td style='width: 100px;'>";
                    if($pag > 1) {
                        echo '<a href="index.php?p=adminscraperjsontobdservinova&pag='.($pag-1).'"><< Anterior</a>';
                    }
                    echo "</td><td style='width: 100px;'>".$pag." de ".$totalPelis."</td><td style='width: 100px;'>";
                    if($pag < $totalPelis) {
                        echo '<a href="index.php?p=adminscraperjsontobdservinova&pag='.($pag+1).'">Siguiente >></a>';
                    }
                    echo "</td></tr></table></div>";
                    ?>
                    <button type="submit" form="newpeliculadb">Guardar</button>
                </form>
            </section>
        </div>
    </section>
<script>
    /*cinesScraper*/ cines = <?php if(!empty($horasCineScraper)) echo $horasCineScraper; else echo "[]"; ?>;
</script>