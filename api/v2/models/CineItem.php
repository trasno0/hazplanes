<?php
/**
 * Clase con metodos para operaciones con cines
 *
 * @package API
 * @author Trasno
 */
class CineItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todos los cines
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de pelicula.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listCines($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf("select c.*, t.normal, t.reducida, t.diaespectador, t.normal3d, t.reducida3d, t.diaespectador3d, t.gafas, t.butacaespecial, t.laborables, t.findesyfestivos from %scines c left join %starifas t on t.idcine=c.idcine %s order by c.%s %s", BDPREFIX, BDPREFIX, $options["filter"], $this->db->secure_field($options["order"]), $this->db->secure_field($limit));
        //echo $query."<br>";
        $r = $this->db->query($query);

        $result = array();
        while($cine = $this->db->fetch($r)) {
            $result[] = $cine;
        }
        foreach($result as $k => $v) {
            $result[$k]["promos"] = $this->getPromociones($cine["idcine"]);
            $result[$k]["entradas"] = $this->getEntradas($cine["idcine"]);
        }
        return $result;
    }

    /**
     * Coge un cine y todos sus datos (salas, servicios...)
     *
     * @param int $idcine id del cine
     * @return array|false
     */
     public function dataCine($idcine) {
        if(!empty($idcine)) {
            $query = sprintf("select c.*, t.normal, t.reducida, t.diaespectador, t.normal3d, t.reducida3d, t.diaespectador3d, t.gafas, t.butacaespecial, t.laborables, t.findesyfestivos from %scines c left join %starifas t on t.idcine=c.idcine where c.idcine = %d", BDPREFIX, BDPREFIX, $this->db->secure_field($idcine));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                $cine = $this->db->fetch($r);
                $cine["promos"] = $this->getPromociones($cine["idcine"]);
                $cine["entradas"] = $this->getEntradas($cine["idcine"]);
                return array(0 => $cine);
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un cine
     *
     * @param int $idcine id del cine
     * @return boolean
     */
    public function deleteCine($idcine) {
        if(!empty($idcine)) {
            $query = sprintf("delete from %starifas where idcine = %d",BDPREFIX, $this->db->secure_field($idcine));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %scines where idcine = %d",BDPREFIX, $this->db->secure_field($idcine));
            $r = $this->db->execute($query);
            if($r) {
                deleteAllExtImages("img/cines/".$idcine);
                deleteAllExtImages("img/cines/original/".$idcine);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar un cine
     *
     * @param array $cine datos del cine
     * @param string $cine['nombre'] nombre del cine
     * @param string $cine['direccion']
     * @param string $cine['localidad']
     * @param string $cine['provincia']
     * @param int $cine['codigopostal']
     * @param string $cine['coordenadas']
     * @param string $cine['telefono']
     * @param string $cine['email']
     * @param string $cine['web']
     * @param string $cine['twitter']
     * @param string $cine['facebook']
     * @param string $cine['google']
     * @param string $cine['otrasocial']
     * @param int $cine['diaespectador']
     * @param string $cine['notas']
     * @param string $cine['descripcion']
     * @param string $cine['imagen']
     * @return int
     */
    public function addCine($cine) {
        if(!$this->checkCine("nombre", $cine["nombre"])) {
            $fields = "";
            $values = "";
            foreach($cine as $key => $value) {
                if(!empty($fields)) {
                    $fields = $this->db->secure_field($fields).",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("insert into %scines (%s, creado) VALUES (%s, '%s')", BDPREFIX, $fields, $values, $now);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addCine] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addCine] Ya existe el cine.", 1);
        }
    }

    /*
     * Actualiza los campos de un cine
     *
     * @param array $cine datos del cine
     * @param int $cine['idcine'] identificador del cine
     * @param string $cine['nombre'] nombre del cine
     * @param string $cine['direccion']
     * @param string $cine['localidad']
     * @param string $cine['provincia']
     * @param int $cine['codigopostal']
     * @param string $cine['coordenadas']
     * @param string $cine['telefono']
     * @param string $cine['email']
     * @param string $cine['web']
     * @param string $cine['twitter']
     * @param string $cine['facebook']
     * @param string $cine['google']
     * @param string $cine['otrasocial']
     * @param int $cine['diaespectador']
     * @param string $cine['notas']
     * @return boolean
     */
    public function updateCine($cine) {
        if($this->checkCine("idcine", $cine["idcine"])) {
            $fields = "";
            foreach($cine as $key => $value) {
                if($key != "idcine") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("update %scines set %s, actualizado = '%s' where idcine = %d", BDPREFIX, $fields, $now, $this->db->secure_field($cine["idcine"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateCine] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateCine] No existe el cine.", 1);
        }
    }

    /*
     * Publicar cines para que sean visibles
     *
     * @param int $ids ids de los cines
     * @return boolean
     */
    public function publishCine($ids) {
        $query = sprintf("update %scines set publicado=1 where idcine in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishCine] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar cines para que no sean visibles
     *
     * @param int $ids ids de los cines
     * @return boolean
     */
    public function unpublishCine($ids) {
        $query = sprintf("update %scines set publicado=0 where idcine in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishCine] Error en la query: ".$query, 1);
        }
    }

    /*
     * Votar cines
     *
     * @param int $idlugar id del cine
     * @param int $points puntos que le dan
     * @return boolean
     */
    public function voteCine($idlugar,$points) {
        $query = sprintf("update %scines set numvotos=numvotos+1, puntos=puntos+%d where idcine = %d", BDPREFIX, $this->db->secure_field($points), $this->db->secure_field($idlugar));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[voteCine] Error en la query: ".$query, 1);
        }
    }

    /*
     * Insertar tarifas de un cine
     *
     * @param int $idcine id del cine
     * @param array $tarifa tarifas del cine
     * @param int $tarifa['normal']
     * @param int $tarifa['reducida']
     * @param int $tarifa['diaespectador']
     * @param int $tarifa['normal3d']
     * @param int $tarifa['reducida3d']
     * @param int $tarifa['diaespectador3d']
     * @param int $tarifa['gafas']
     * @param int $tarifa['butacaespecial']
     * @return boolean
     */
    public function setTarifas($idcine, $tarifas) {
        if(!$this->checkCineTarifas("idcine", $idcine)) {
            $fields = "idcine";
            $values = $idcine;
            foreach($tarifas as $key => $value) {
                if(!empty($fields)) {
                    $fields = $this->db->secure_field($fields).",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %starifas (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setTarifas] Error en la query: ".$query, 1);
            }
        } else {
            $fields = "";
            foreach($tarifas as $key => $value) {
                if($key != "idcine") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %starifas set %s where idcine = %d", BDPREFIX, $fields, $this->db->secure_field($idcine));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setTarifas] Error en la query: ".$query, 1);
            }
        }
    }

    /*
     * Relacionar un sitio de entradas con un cine
     *
     * @param int $idcine id del cine
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function setEntradas($idcine, $identradas) {
        if(!$this->checkRelEntradas($idcine, $identradas)) {
            $query = sprintf("insert into %scines_has_entradas (idcine,identradas) VALUES (%d,%d)", BDPREFIX, $this->db->secure_field($idcine), $this->db->secure_field($identradas));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[setEntradas] Ya existe la relacion entre el cine y el sitio de entradas.", 1);
        }
    }

    /*
     * Relacionar una promocion con un cine
     *
     * @param int $idcine id del cine
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    public function setPromociones($idcine, $idpromocion) {
        if(!$this->checkRelPromos($idcine, $idpromocion)) {
            $query = sprintf("insert into %scines_has_promociones (idcine,idpromocion) VALUES (%d,%d)", BDPREFIX, $this->db->secure_field($idcine), $this->db->secure_field($idpromocion));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setPromociones] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[setPromociones] Ya existe la relacion entre el cine y la promoción.", 1);
        }
    }

    /*
     * Coger la relacion de un sitio de entradas con un cine
     *
     * @param int $idcine id del cine
     * @return array
     */
    public function getEntradas($idcine) {
        $query = sprintf("select identradas from %scines_has_entradas where idcine = %d", BDPREFIX, $this->db->secure_field($idcine));
        $r = $this->db->query($query);
        if($r) {
            $result = array();
            while($cine_entradas = $this->db->fetch($r)) {
                $result[] = $cine_entradas;
            }
            return $result;
        } else {
            throw new Exception("[getEntradas] Error en la query: ".$query, 1);
        }
    }

    /*
     * Coger la relacion de una promocion con un cine
     *
     * @param int $idcine id del cine
     * @return array
     */
    public function getPromociones($idcine) {
        $query = sprintf("select idpromocion from %scines_has_promociones where idcine = %d", BDPREFIX, $this->db->secure_field($idcine));
        $r = $this->db->query($query);
        if($r) {
            $result = array();
            while($cine_promociones = $this->db->fetch($r)) {
                $result[] = $cine_promociones;
            }
            return $result;
        } else {
            throw new Exception("[getPromociones] Error en la query: ".$query, 1);
        }
    }

    /*
     * Eliminar la relacion de un sitio de entradas con un cine
     *
     * @param int $idcine id del cine
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function delEntradas($idcine, $identradas = 0) {
        if(empty($identradas) || $this->checkRelEntradas($idcine, $identradas)) {
            $query = sprintf("delete from %scines_has_entradas where idcine = %d", BDPREFIX, $this->db->secure_field($idcine));
            if(!empty($identradas)) {
                $query .= " and identradas = ".$this->db->secure_field($identradas);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delEntradas] No existe la relacion entre el cine y el sitio de entradas.", 1);
        }
    }

    /*
     * Eliminar la relacion de una promocion con un cine
     *
     * @param int $idcine id del cine
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    public function delPromociones($idcine, $idpromocion = 0) {
        if(empty($idpromocion) || $this->checkRelPromos($idcine, $idpromocion)) {
            $query = sprintf("delete from %scines_has_promociones where idcine = %d", BDPREFIX, $this->db->secure_field($idcine));
            if(!empty($idpromocion)) {
                $query .= " and idpromocion = ".$this->db->secure_field($idpromocion);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delPromociones] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delPromociones] No existe la relacion entre el cine y la promoción.", 1);
        }
    }

    /*
     * Comprueba si existe el cine
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkCine($field, $value) {
        $query = sprintf("select c.* from %scines c where c.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si el cine tiene ya tarifas
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkCineTarifas($field, $value) {
        $query = sprintf("select t.* from %starifas t where t.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion cine-entradas
     *
     * @param int $idcine id del cine
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    private function checkRelEntradas($idcine, $identradas) {
        $query = sprintf("select ce.* from %scines_has_entradas ce where ce.idcine = %d and ce.identradas = %d", BDPREFIX, $this->db->secure_field($idcine), $this->db->secure_field($identradas));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion cine-promociones
     *
     * @param int $idcine id del cine
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    private function checkRelPromos($idcine, $idpromocion) {
        $query = sprintf("select cp.* from %scines_has_promociones cp where cp.idcine = %d and cp.idpromocion = %d", BDPREFIX, $this->db->secure_field($idcine), $this->db->secure_field($idpromocion));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END