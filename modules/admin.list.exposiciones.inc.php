<?php
pageAccessControl(1);

// Validate what page num to show in list
if(isset($_GET["pag"])) {
    $pag = $_GET["pag"];
} else {
    $pag = 0;
}

//Validate what order to apply to list
if(isset($_GET["order"])) {
    $orde = $_GET["order"];
    if(substr($orde,0,2) == "az") {
        $order = substr($orde,2).", fin desc";
    } else {
        $order = substr($orde,2)." desc, fin desc";
    }
} else {
    $order = "fin desc, nombre";
    $orde = "";
}
$start = $pag * 50;
$options = array("limit" => 50, "start" => $start, "order" => $order);
$exposiciones = new Exposiciones($options,$db);
$data = $exposiciones->readAction();
$cont = count($data);
?>
    <section id='content'>
        <section id='datos'>
            <div class="header-list">
                <h2>Exposiciones <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformexpo'">Nueva exposición</button></span></h2>
                <div id="listOptions">
                    <button type="button" c='Exposiciones' id='publish'>Publicar</button>
                    <button type="button" c='Exposiciones' id='unpublish'>No publicar</button>
                    <button type="button" c='Exposiciones' id='delete'>Eliminar</button>
                </div>
            </div>
            <?php paginacion($pag,$cont,"adminlistexposiciones",$orde); ?>
            <input type="hidden" name="type" id="type" value="exposiciones"/>
            <table id="list" class="tabla">
                <thead>
                    <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                    <th class='listPublishedCell'><a href="index.php?p=adminlistexposiciones&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azpublicado"?"zapublicado":"azpublicado"; ?>">Publicado</a></th>
                    <th><a href="index.php?p=adminlistexposiciones&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aznombre"?"zanombre":"aznombre"; ?>">Nombre</a></th>
                    <th><a href="index.php?p=adminlistexposiciones&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azlugar"?"zalugar":"azlugar"; ?>">Lugar</a></th>
                    <th><a href="index.php?p=adminlistexposiciones&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azgenero"?"zagenero":"azgenero"; ?>">Género</a></th>
                    <th class='col-fecha-list'><a href="index.php?p=adminlistexposiciones&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azfin"?"zafin":"azfin"; ?>">Fin</a></th>
                    <th><a href="index.php?p=adminlistexposiciones&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azpermanente"?"zapermanente":"azpermanente"; ?>">Permanente</a></th>
                    <th><a href="index.php?p=adminlistexposiciones&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azposter"?"zaposter":"azposter"; ?>">Poster</a></th>
                    <th>Enlace</th>
                </thead>
                <tbody id="listrows">
                <?php
                foreach($data as $k => $d) {
                    $link = "exposicion/";
                    $enlace = BASE_URL.$link.$d["idexposicion"]."-".urlAmigable($d["nombre"]);
                    echo "<tr class='row'>
                        <td><input type='checkbox' name='checkListItem' id='".$d["idexposicion"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                        <td>".($d["publicado"]?"Si":"No")."</td>
                        <td><a href='index.php?p=adminformexpo&i=".$d["idexposicion"]."'>".$d["nombre"]."</a></td>
                        <td>".$d["lugar"]."</td>
                        <td>".$d["genero"]."</td>
                        <td>".date("d-m-Y",strtotime($d["fin"]))."</td>
                        <td>".($d["permanente"]?"Si":"No")."</td>
                        <td>".(empty($d["poster"])?"No":"Si")."</td>
                        <td><a href='".$enlace."'>".$enlace."</a></td>
                    </tr>";
                }
                ?>
                </tbody>
            </table>
            <?php paginacion($pag,$cont,"adminlistexposiciones",$orde); ?>
        </section>
    </section>
