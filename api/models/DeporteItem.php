<?php
/**
 * Clase con metodos para operaciones con deportes
 *
 * @package API
 * @author Trasno
 */
class DeporteItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todos los deportes
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de pelicula.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listDeportes($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }

        $query = sprintf(
        "select d.iddeporte, d.tipo, d.nombre, ld.nombre as lugar, td.nombre as genero, d.poster, fecha, infantil, agotado, cancelado, todoeldia, publicado from %sdeportes d LEFT JOIN %stipos_deporte td on td.idtipodeporte = d.tipo left join (
            select iddeporte, l.nombre, fecha, agotado, cancelado, todoeldia from %slocales_has_deportes lhc left join %slocales l on l.idlocal = lhc.idlocal union
            select iddeporte, t.nombre, fecha, agotado, cancelado, todoeldia from %steatros_has_deportes thc left join %steatros t on t.idteatro = thc.idteatro union
            select iddeporte, c.nombre, fecha, agotado, cancelado, todoeldia from %scines_has_deportes chc left join %scines c on c.idcine = chc.idcine union
            select iddeporte, lugar, fecha, agotado, cancelado, todoeldia from %slugares_has_deportes union
            select iddeporte, m.nombre, fecha, agotado, cancelado, todoeldia from %smuseos_has_deportes mhc left join %smuseos m on m.idmuseo = mhc.idmuseo union
            select iddeporte, p.nombre, fecha, agotado, cancelado, todoeldia from %spabellones_has_deportes phc left join %spabellones p on p.idpabellon = phc.idpabellon
        ) as ld on ld.iddeporte = d.iddeporte %s group by date(fecha), d.nombre order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], $options["order"],$this->db->secure_field($limit));
        //echo $query;
        $r = $this->db->query($query);

        $result = array();
        while($deportes = $this->db->fetch($r)) {
            $result[] = $deportes;
        }

        return $result;
    }

    /**
     * Coge un deporte y todos sus datos
     *
     * @param int $iddeporte id de la competicion
     * @return array|false
     */
     public function dataDeportes($iddeporte) {
        if(!empty($iddeporte)) {
            $query = sprintf("select d.*, td.nombre as tiponombre from %sdeportes d LEFT JOIN %stipos_deporte td on td.idtipodeporte = d.tipo where d.iddeporte = %d", BDPREFIX, BDPREFIX, $this->db->secure_field($iddeporte));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un deporte
     *
     * @param int $iddeporte id de la competicion
     * @return boolean
     */
    public function deleteDeportes($iddeporte) {
        if(!empty($iddeporte)) {
            $query = sprintf("delete from %spabellones_has_deportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($iddeporte));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slocales_has_deportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($iddeporte));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %steatros_has_deportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($iddeporte));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %scines_has_deportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($iddeporte));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slugares_has_deportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($iddeporte));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %smuseos_has_deportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($iddeporte));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %sdeportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($iddeporte));
                $r = $this->db->execute($query);
                deleteAllExtImages("img/deportes/poster/".$iddeporte);
                deleteAllExtImages("img/deportes/poster/thumbs/".$iddeporte);
                deleteAllExtImages("img/deportes/poster/original/".$iddeporte);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar una competicion
     *
     * @param array $deportes datos de la competicion
     * @param string $deportes['nombre']
     * @param string $deportes['lugar']
     * @param string $deportes['tipo'] tipo de deporte
     * @param string $deportes['descripcion']
     * @param string $deportes['notas']
     * @param datetime $deportes['fecha']
     * @return int
     */
    public function addDeportes($deportes) {
        if(!$this->checkDeportes("nombre like '".$this->db->secure_field($deportes["nombre"])."'")) {
            $fields = "";
            $values = "";
            foreach($deportes as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %sdeportes (%s, creado) VALUES (%s, NOW())", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addDeportes] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addDeportes] Ya existe la competición.", 1);
        }
    }

    /*
     * Actualiza los campos de una competicion
     *
     * @param array $deportes datos de la competicion
     * @param int $deportes['iddeporte'] identificador de la competicion
     * @param string $deportes['nombre']
     * @param string $deportes['lugar']
     * @param string $deportes['tipo'] tipo de deporte
     * @param string $deportes['descripcion']
     * @param string $deportes['notas']
     * @param datetime $deportes['fecha']
     * @return boolean
     */
    public function updateDeportes($deportes) {
        if($this->checkDeportes("iddeporte = ".$deportes["iddeporte"])) {
            $fields = "";
            foreach($deportes as $key => $value) {
                if($key != "iddeporte") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %sdeportes set %s, actualizado = NOW() where iddeporte = %d", BDPREFIX, $fields, $this->db->secure_field($deportes["iddeporte"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateDeportes] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateDeportes] No existe la competición.", 1);
        }
    }

    /**
     * Coge todos los tipos de deporte
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listTiposDeporte($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(empty($options["filter"])) {
            $options["filter"] = 1;
        }
        $query = sprintf("select td.* from %stipos_deporte td where %s order by %s %s", BDPREFIX, $this->db->secure_field($options["filter"]), $this->db->secure_field($options["order"]), $this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($deportes = $this->db->fetch($r)) {
            $result[] = $deportes;
        }

        return $result;
    }

    /**
     * Coge un tipo de deporte y todos sus datos
     *
     * @param int $idtipodeporte id del tipo de deporte
     * @return array|false
     */
     public function dataTipoDeporte($idtipodeporte) {
        if(!empty($idtipodeporte)) {
            $query = sprintf("select e.* from %stipos_deporte e where e.idtipodeporte = %d", BDPREFIX, $this->db->secure_field($idtipodeporte));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un tipo de deporte
     *
     * @param int $idtipodeporte id del tipo
     * @return boolean
     *
     * TODO: eliminar todos los deportes que dependan del tipo a eliminar
     */
    public function deleteTipoDeportes($idtipodeporte) {
        if(!empty($iddeporte)) {
            /*$query = sprintf("delete from %spabellones_has_deportes where iddeporte = %d",BDPREFIX, $this->db->secure_field($idtipodeporte));
            $r = $this->db->execute($query);
            if($r) {*/
                $query = sprintf("delete from %stipos_deporte where idtipodeporte = %d",BDPREFIX, $this->db->secure_field($idtipodeporte));
                $r = $this->db->execute($query);
                //deleteAllExtImages("img/deportes/poster/".$iddeporte);
                //deleteAllExtImages("img/deportes/poster/thumbs/".$iddeporte);
                return true;
            //}
        }
        return false;
    }

    /*
     * Insertar un tipo de deporte
     *
     * @param array $deportes datos del tipo de deporte
     * @param string $deportes['nombre']
     * @param string $deportes['poster']
     * @return int
     */
    public function addTipoDeporte($deportes) {
        if(!$this->checkTipoDeporte("nombre like '".$this->db->secure_field($deportes["nombre"])."'")) {
            $fields = "";
            $values = "";
            foreach($deportes as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %stipos_deporte (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addTipoDeporte] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addTipoDeporte] Ya existe el tipo de deporte.", 1);
        }
    }

    /*
     * Actualiza los campos de un tipo de deporte
     *
     * @param array $deportes datos del tipo de deporte
     * @param int $deportes['idtipodeporte'] identificador
     * @param string $deportes['nombre']
     * @param string $deportes['poster']
     * @return boolean
     */
    public function updateTipoDeporte($deportes) {
        if($this->checkTipoDeporte("idtipodeporte = ".$deportes["idtipodeporte"])) {
            $fields = "";
            foreach($deportes as $key => $value) {
                if($key != "idtipodeporte") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %stipos_deporte set %s where idtipodeporte = %d", BDPREFIX, $fields, $this->db->secure_field($deportes["idtipodeporte"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateTipoDeporte] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateTipoDeporte] No existe el tipo de deporte.", 1);
        }
    }

    /*
     * Publicar deportes para que sean visibles
     *
     * @param int $ids ids de las webs de deportes
     * @return boolean
     */
    public function publishDeportes($ids) {
        $query = sprintf("update %sdeportes set publicado=1 where iddeporte in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishDeportes] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar deportes para que no sean visibles
     *
     * @param int $ids ids de las webs de deportes
     * @return boolean
     */
    public function unpublishDeportes($ids) {
        $query = sprintf("update %sdeportes set publicado=0 where iddeporte in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishDeportes] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar una competicion con una fecha
     *
     * @param array $deporte
     * @param int $deporte['idlugar']
     * @param int $deporte['iddeporte']
     * @param datetime $pelicula['fecha']
     * @param int $pelicula['duracion']
     * @param string $deporte['urlcompra']
     * @param string $deporte['precio']
     * @param string $deporte['precioadelantada']
     * @return boolean
     */
    public function addHora($deporte, $type) {
        if(!$this->checkRelFechaDeporte($deporte, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_deportes";
                    $idlugar = "idlocal";
                    break;
                case "teatro":
                    $tabla = "teatros_has_deportes";
                    $idlugar = "idteatro";
                    break;
                case "cine":
                    $tabla = "cines_has_deportes";
                    $idlugar = "idcine";
                    break;
                case "lugar":
                    $tabla = "lugares_has_deportes";
                    unset($deporte["idlugar"], $deporte["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_deportes";
                    $idlugar = "idmuseo";
                    break;
                case "pabellon":
                    $tabla = "pabellones_has_deportes";
                    $idlugar = "idpabellon";
                    break;
            }
            $fields = "";
            $values = "";
            foreach($deporte as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                if($key == "idlugar") {
                    $key = $idlugar;
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %s (%s) VALUES (%s)", BDPREFIX.$tabla, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[addHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Coger las horas de una competicion
     *
     * @param array $deporte
     * @param int $deporte['iddeporte']
     * @return array
     */
    public function listHoras($deporte) {
        $result = array("horasLocal" => array(), "horasAuditorio" => array(), "horasCine" => array(), "horasLugar" => array(), "horasMuseo" => array(), "horasPabellon" => array());

        //Locales
        $query = sprintf("select lc.*, l.nombre, l.direccion, l.localidad, l.coordenadas from %slocales_has_deportes lc, %slocales l where lc.iddeporte = %d and l.idlocal = lc.idlocal", BDPREFIX, BDPREFIX, $this->db->secure_field($deporte["iddeporte"]));
        if(isset($deporte["idlugar"]) && !empty($deporte["idlugar"])) {
            $query .= " and lc.idlocal = ".$this->db->secure_field($deporte["idlugar"]);
        }
        $query .= " order by lc.idlocal, lc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLocal"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Auditorios
        $query = sprintf("select tc.*, t.nombre, t.direccion, t.localidad, t.coordenadas from %steatros_has_deportes tc, %steatros t where tc.iddeporte = %d and t.idteatro = tc.idteatro", BDPREFIX, BDPREFIX, $this->db->secure_field($deporte["iddeporte"]));
        if(isset($deporte["idlugar"]) && !empty($deporte["idlugar"])) {
            $query .= " and lc.idteatro = ".$this->db->secure_field($deporte["idlugar"]);
        }
        $query .= " order by tc.idteatro, tc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasAuditorio"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Cines
        $query = sprintf("select cc.*, c.nombre, s.numero, c.direccion, c.localidad, c.coordenadas from %scines_has_deportes cc, %scines c, %ssalas s where cc.iddeporte = %d and c.idcine = cc.idcine and s.idsala = cc.idsala", BDPREFIX, BDPREFIX, BDPREFIX, $this->db->secure_field($deporte["iddeporte"]));
        if(isset($deporte["idsala"]) && !empty($deporte["idsala"])) {
            $query .= " and cc.idsala = ".$this->db->secure_field($deporte["idsala"]);
        }
        if(isset($deporte["idlugar"]) && !empty($deporte["idlugar"])) {
            $query .= " and cc.idcine = ".$this->db->secure_field($deporte["idlugar"]);
        }
        $query .= " order by cc.idcine, cc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasCine"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Lugar
        $query = sprintf("select lc.* from %slugares_has_deportes lc where lc.iddeporte = %d order by lc.fecha", BDPREFIX, $this->db->secure_field($deporte["iddeporte"]));
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLugar"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Museos
        $query = sprintf("select mc.*, m.nombre, m.direccion, m.localidad, m.coordenadas from %smuseos_has_deportes mc, %smuseos m where mc.iddeporte = %d and m.idmuseo = mc.idmuseo", BDPREFIX, BDPREFIX, $this->db->secure_field($deporte["iddeporte"]));
        if(isset($deporte["idlugar"]) && !empty($deporte["idlugar"])) {
            $query .= " and mc.idmuseo = ".$this->db->secure_field($deporte["idlugar"]);
        }
        $query .= " order by mc.idmuseo, mc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasMuseo"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Pabellones
        $query = sprintf("select pd.*, p.nombre, p.direccion, p.localidad, p.coordenadas from %spabellones_has_deportes pd, %spabellones p where pd.iddeporte = %d and p.idpabellon = pd.idpabellon", BDPREFIX, BDPREFIX, $this->db->secure_field($deporte["iddeporte"]));
        if(isset($deporte["idlugar"]) && !empty($deporte["idlugar"])) {
            $query .= " and pd.idpabellon = ".$this->db->secure_field($deporte["idlugar"]);
        }
        $query .= " order by pd.idpabellon, pd.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasPabellon"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        return $result;
    }

    /*
     * Eliminar la relacion de una competicion con una fecha
     *
     * @param array $deporte
     * @param int $deporte['idlugar']
     * @param datetime $deporte['fecha']
     * @return boolean
     */
    public function deleteHora($deporte, $type) {
        if($this->checkRelFechaDeporte($deporte, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_deportes";
                    $idlugar = "idlocal = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_deportes";
                    $idlugar = "idteatro = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_deportes";
                    $idlugar = "idcine = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_deportes";
                    $idlugar = "1";
                    break;
                case "museo":
                    $tabla = "museos_has_deportes";
                    $idlugar = "idmuseo = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "pabellon":
                    $tabla = "pabellones_has_deportes";
                    $idlugar = "idpabellon = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
            }
            $query = sprintf("delete from %s where iddeporte = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($deporte["iddeporte"]), $idlugar, $this->db->secure_field($deporte["fecha"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delHora] No existe la relacion.", 1);
        }
    }

    /*
     * Actualizar una relacion de una competicion con una hora
     *
     * @param array $deporte
     * @param int $deporte['iddeporte']
     * @param datetime $pelicula['fecha']
     * @param int $pelicula['duracion']
     * @param string $deporte['precio']
     * @param string $deporte['lugar']
     * @return boolean
     */
    public function updateHora($deporte, $type) {
        if($this->checkRelFechaDeporte($deporte, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_deportes";
                    $idlugar = "idlocal = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_deportes";
                    $idlugar = "idteatro = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_deportes";
                    $idlugar = "idcine = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_deportes";
                    $idlugar = "1";
                    unset($deporte["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_deportes";
                    $idlugar = "idmuseo = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
                case "pabellon":
                    $tabla = "pabellones_has_deportes";
                    $idlugar = "idpabellon = ".$this->db->secure_field($deporte["idlugar"]);
                    break;
            }
            $fields = "";
            $exclude = array("idlugar","iddeporte","fecha");
            foreach($deporte as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %s set %s where iddeporte = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $fields, $this->db->secure_field($deporte["iddeporte"]), $idlugar, $this->db->secure_field($deporte["fecha"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHora] No existe la relacion.", 1);
        }
    }

    /*
     * Comprueba si existe la competicion
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkDeportes($field) {
        $query = sprintf("select e.* from %sdeportes e where %s", BDPREFIX, $field);
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe el tipo de deporte
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkTipoDeporte($field) {
        $query = sprintf("select e.* from %stipos_deporte e where %s", BDPREFIX, $field);
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion cine-promociones
     *
     * @param int $idcine id del cine
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    private function checkRelFechaDeporte($deporte, $type) {
        switch($type){
            case "local":
                $tabla = "locales_has_deportes";
                $idlugar = "idlocal = ".$this->db->secure_field($deporte["idlugar"]);
                break;
            case "teatro":
                $tabla = "teatros_has_deportes";
                $idlugar = "idteatro = ".$this->db->secure_field($deporte["idlugar"]);
                break;
            case "cine":
                $tabla = "cines_has_deportes";
                $idlugar = "idcine = ".$this->db->secure_field($deporte["idlugar"]);
                break;
            case "lugar":
                $tabla = "lugares_has_deportes";
                $idlugar = "1";
                break;
            case "museo":
                $tabla = "museos_has_deportes";
                $idlugar = "idmuseo = ".$this->db->secure_field($deporte["idlugar"]);
                break;
            case "pabellon":
                $tabla = "pabellones_has_deportes";
                $idlugar = "idpabellon = ".$this->db->secure_field($deporte["idlugar"]);
                break;
        }
        $query = sprintf("select pd.* from %s pd where pd.iddeporte = %d and %s and pd.fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($deporte["iddeporte"]), $idlugar, $this->db->secure_field($deporte["fecha"]));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END