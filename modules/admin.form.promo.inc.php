<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $cancel = "index.php?p=adminlistpromo";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "idpromocion");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newpromo">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        if(isset($id) && !empty($id)) {
            $options = array("idpromocion" => $id);
            $promo = new Promociones($options,$db);
            $d = $promo->readAction();
            $d = $d[0];
            $d["inicio"] = date("Y-m-d",strtotime($d["inicio"]));
            $d["fin"] = date("Y-m-d",strtotime($d["fin"]));
            if($d["publicado"] == 0) {
                $d["publicado"] = "";
            } elseif($d["publicado"] == 1) {
                $d["publicado"] = "checked";
            }
            $act = "m";
        } else {
            $d = array(
                "idpromocion" => "",
                "nombre" => "",
                "descripcion" => "",
                "inicio" => "",
                "fin" => "",
                "imagen" => "",
                "menciones" => "",
                "publicado" => ""
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}

?>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo $msg["errormsg"];
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar promoción ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo promoción ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newpromo" id="newpromo" class="form" action="index.php?p=adminformpromo" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="idpromocion" value="<?php echo $d["idpromocion"]; ?>"/>
                <div>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" value="<?php echo $d["nombre"]; ?>" />
                </div>
                <div>
                    <label for="form-inicio">Inicio</label>
                    <input type="date" name="inicio" id="form-inicio" class="campo" value="<?php echo $d["inicio"]; ?>" />
                </div>
                <div>
                    <label for="form-fin">Fin</label>
                    <input type="date" name="fin" id="form-fin" class="campo" value="<?php echo $d["fin"]; ?>" />
                </div>
                <div class="field-vacio"></div>
                <div>
                    <label for="form-descripcion">Descripción</label>
                    <textarea name="descripcion" id="form-descripcion" class="campo"><?php echo $d["descripcion"]; ?></textarea>
                </div>
                <div>
                    <label for="form-menciones">Menciones / Agradecimientos / Créditos</label>
                    <textarea name="menciones" id="form-menciones" class="campo"><?php echo $d["menciones"]; ?></textarea>
                    <br><br>
                </div>
                <div>
                    <label for="form-imagenURL">Imagen desde URL</label>
                    <input type="url" name="imagenURL" id="form-imagenURL" class="campo" /><br>
                    <label for="form-imagen">Imagen desde escritorio</label>
                    <input type="file" name="imagen" id="form-image" class="campo" />
                    <?php
                    if(!empty($d["imagen"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["imagen"]."?".strtotime("now")."' alt='Imagen de la promoción' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg1' onclick='deleteImage(this,".$d["idpromocion"].",\"".$d["imagen"]."\",\"Promociones\")'>Eliminar</button></p>";
                    }
                    ?>
                </div>
                <div class="field-vacio">
                    <label for="form-publicado">Publicado</label>
                    <span class="checkbox">
                        <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                        <label class="check" for="form-publicado"></label>
                    </span>
                </div>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Promoción creada con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Promoción modificada con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformpromocion&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformpromo">Crear una nueva</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>