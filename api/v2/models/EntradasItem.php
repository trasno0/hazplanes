<?php
/**
 * Clase con metodos para operaciones con webs de entradas
 *
 * @package API
 * @author Trasno  
 */
class EntradasItem {
    protected $db;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }
    
    /**
     * Coge todas las webs de entradas
     *
     * @param string $filter (opcional) filtro de id del cine que tiene esas webs de entradas 
     * @param string $order (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listEntradas($filter = "", $order = "nombre") {
        if(!empty($filter)) {
            $where = ", ".BDPREFIX."cines_has_entradas cp where cp.idcine=".$filter." and e.identradas=cp.identradas";
        } else {
            $where = "";
        }
        $query = sprintf("select e.* from %sentradas e %s order by e.%s", BDPREFIX, $this->db->secure_field($where), $this->db->secure_field($order));
        $r = $this->db->query($query);
        
        $result = array();
        while($entradas = $this->db->fetch($r)) {
            $result[] = $entradas;
        }
        
        return $result;
    }
    
    /**
     * Coge una web de entradas y todos sus datos (salas, servicios...)
     *
     * @param int $identradas id de la web de entradas 
     * @return array|false
     */
     public function dataEntradas($identradas) {
        if(!empty($identradas)) {
            $query = sprintf("select e.* from %sentradas e where e.identradas = %d", BDPREFIX, $this->db->secure_field($identradas));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }
     
    /*
     * Eliminar una web de entradas
     *
     * @param int $identradas id de la web de entradas
     * @return boolean
     */
    public function deleteEntradas($identradas) {
        if(!empty($identradas)) {
            $query = sprintf("delete from %sentradas where identradas = %d",BDPREFIX, $this->db->secure_field($identradas));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %scines_has_entradas where identradas = %d",BDPREFIX, $this->db->secure_field($identradas));
                $r = $this->db->execute($query);
                if($r) {
                    deleteAllExtImages("img/entradas/".$identradas);
                    deleteAllExtImages("img/entradas/original/".$identradas);
                    return true;
                }
            }
        }
        return false;
    }
    
    /*
     * Insertar una web de entradas
     *
     * @param array $entradas datos de la web de entradas
     * @param string $entradas['descripcion']
     * @param datetime $entradas['inicio']
     * @param datetime $entradas['fin']
     * @return int
     */
    public function addEntradas($entradas) {
        if(!$this->checkEntradas("nombre", $entradas["nombre"])) {
            $fields = "";
            $values = "";
            foreach($entradas as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %sentradas (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addEntradas] Error en la query: ".$query, 1);    
            }
        } else {
            throw new Exception("[addEntradas] Ya existe la entradas.", 1);
        }
    }
    
    /*
     * Actualiza los campos de una web de entradas
     *
     * @param array $entradas datos de la web de entradas
     * @param int $entradas['identradas'] identificador de la web de entradas
     * @param string $entradas['descripcion']
     * @param datetime $entradas['inicio']
     * @param datetime $entradas['fin']
     * @return boolean
     */
    public function updateEntradas($entradas) {
        if($this->checkEntradas("identradas", $entradas["identradas"])) {
            $fields = "";
            foreach($entradas as $key => $value) {
                if($key != "identradas") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %sentradas set %s where identradas = %d", BDPREFIX, $fields, $this->db->secure_field($entradas["identradas"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateEntradas] No existe la entradas.", 1);
        }
    }
    
    /*
     * Publicar entradas para que sean visibles
     *
     * @param int $ids ids de las webs de entradas
     * @return boolean
     */
    public function publishEntradas($ids) {
        $query = sprintf("update %sentradas set publicado=1 where identradas in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishEntradas] Error en la query: ".$query, 1);    
        }
    }
    
    /*
     * Despublicar entradas para que no sean visibles
     *
     * @param int $ids ids de las webs de entradas
     * @return boolean
     */
    public function unpublishEntradas($ids) {
        $query = sprintf("update %sentradas set publicado=0 where identradas in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishEntradas] Error en la query: ".$query, 1);    
        }
    }
    
    /*
     * Comprueba si existe la web de entradas
     * 
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkEntradas($field, $value) {
        $query = sprintf("select e.* from %sentradas e where e.%s like '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);
        
        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END