<?php
pageAccessControl(1);

// Validate what page num to show in list
if(isset($_GET["pag"])) {
    $pag = $_GET["pag"];
} else {
    $pag = 0;
}

//Validate what order to apply to list
if(isset($_GET["order"])) {
    $orde = $_GET["order"];
    if(substr($orde,0,2) == "az") {
        $order = substr($orde,2).", fecha desc";
    } else {
        $order = substr($orde,2)." desc, fecha desc";
    }
} else {
    $order = "fecha desc, nombre";
    $orde = "";
}
$start = $pag * 50;
$options = array("limit" => 50, "start" => $start, "order" => $order);
$deportes = new Deportes($options,$db);
$data = $deportes->readAction();
$cont = count($data);
?>
    <section id='content'>
        <section id='datos'>
            <div class="header-list">
                <h2>Competiciones <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformdeporte'">Nueva competición</button></span></h2>
                <div id="listOptions">
                    <button type="button" c='Deportes' id='publish'>Publicar</button>
                    <button type="button" c='Deportes' id='unpublish'>No publicar</button>
                    <button type="button" c='Deportes' id='delete'>Eliminar</button>
                </div>
            </div>
            <?php paginacion($pag,$cont,"adminlistdeportes",$orde); ?>
            <input type="hidden" name="type" id="type" value="deportes"/>
            <table id="list" class="tabla">
                <thead>
                    <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                    <th class='listPublishedCell'><a href="index.php?p=adminlistdeportes&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azpublicado"?"zapublicado":"azpublicado"; ?>">Publicado</a></th>
                    <th><a href="index.php?p=adminlistdeportes&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aznombre"?"zanombre":"aznombre"; ?>">Nombre</a></th>
                    <th><a href="index.php?p=adminlistdeportes&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azlugar"?"zalugar":"azlugar"; ?>">Lugar</a></th>
                    <th><a href="index.php?p=adminlistdeportes&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azgenero"?"zagenero":"azgenero"; ?>">Tipo</a></th>
                    <th class='col-fecha-list'><a href="index.php?p=adminlistdeportes&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azfecha"?"zafecha":"azfecha"; ?>">Fecha</a></th>
                    <th><a href="index.php?p=adminlistdeportes&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azposter"?"zaposter":"azposter"; ?>">Poster</a></th>
                    <th>Enlace</th>
                </thead>
                <tbody id="listrows">
                <?php
                foreach($data as $k => $d) {
                    $tipo = "";
                    if(!empty($d["tiponombre"])) {
                        $tipo = $d["tiponombre"];
                    } else {
                        if(empty($d["genero"])) {
                             $tipo = "Sin especificar";
                        } else {
                             $tipo = $d["genero"];
                        }
                    }
                    $link = "competicion/";
                    $enlace = BASE_URL.$link.$d["iddeporte"]."-".urlAmigable($d["nombre"]);
                    echo "<tr class='row'>
                        <td><input type='checkbox' name='checkListItem' id='".$d["iddeporte"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                        <td>".($d["publicado"]?"Si":"No")."</td>
                        <td><a href='index.php?p=adminformdeporte&i=".$d["iddeporte"]."'>".$d["nombre"]."</a></td>
                        <td>".$d["lugar"]."</td>
                        <td>".$tipo."</td>
                        <td>".(empty($d["fecha"])?"":date("d-m-Y H:i",strtotime($d["fecha"])))."</td>
                        <td>".(empty($d["poster"])?"No":"Si")."</td>
                        <td><a href='".$enlace."'>".$enlace."</a></td>
                    </tr>";
                }
                ?>
                </tbody>
            </table>
            <?php paginacion($pag,$cont,"adminlistdeportes",$orde); ?>
        </section>
    </section>
