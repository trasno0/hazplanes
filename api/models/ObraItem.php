<?php
/**
 * Clase con metodos para operaciones con obras
 *
 * @package API
 * @author Trasno
 */
class ObraItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Listado de obras de teatro
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listObras($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf(
        "select o.idobrateatro, o.nombre, lo.nombre as lugar, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, publicado from %sobrasteatro o left join (
            select idobrateatro, l.nombre, fecha, agotado, cancelado, todoeldia from %slocales_has_obrasteatro lhc left join %slocales l on l.idlocal = lhc.idlocal union
            select idobrateatro, t.nombre, fecha, agotado, cancelado, todoeldia from %steatros_has_obrasteatro thc left join %steatros t on t.idteatro = thc.idteatro union
            select idobrateatro, c.nombre, fecha, agotado, cancelado, todoeldia from %scines_has_obrasteatro chc left join %scines c on c.idcine = chc.idcine union
            select idobrateatro, lugar, fecha, agotado, cancelado, todoeldia from %slugares_has_obrasteatro union
            select idobrateatro, m.nombre, fecha, agotado, cancelado, todoeldia from %smuseos_has_obrasteatro mhc left join %smuseos m on m.idmuseo = mhc.idmuseo
        ) as lo on lo.idobrateatro = o.idobrateatro %s group by date(fecha), o.nombre order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], $options["order"],$this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($obra = $this->db->fetch($r)) {
            $result[] = $obra;
        }

        return $result;
    }

    /**
     * Coge una obra y todos sus datos (locales, horarios, info...)
     *
     * @param int $idobrateatro id de la obra
     * @return array|false
     */
     public function dataObra($idobrateatro) {
        if(!empty($idobrateatro)) {
            $query = sprintf("select ot.* from %sobrasteatro ot where ot.idobrateatro = %d", BDPREFIX, $this->db->secure_field($idobrateatro));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar una obra
     *
     * @param int $idobrateatro id de la obra
     * @return boolean
     */
    public function deleteObra($idobrateatro) {
        if(!empty($idobrateatro)) {
            $query = sprintf("delete from %slocales_has_obrasteatro where idobrateatro = %d",BDPREFIX, $this->db->secure_field($idobrateatro));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %steatros_has_obrasteatro where idobrateatro = %d",BDPREFIX, $this->db->secure_field($idobrateatro));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %scines_has_obrasteatro where idobrateatro = %d",BDPREFIX, $this->db->secure_field($idobrateatro));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slugares_has_obrasteatro where idobrateatro = %d",BDPREFIX, $this->db->secure_field($idobrateatro));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %smuseos_has_obrasteatro where idobrateatro = %d",BDPREFIX, $this->db->secure_field($idobrateatro));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %sobrasteatro where idobrateatro = %d",BDPREFIX, $this->db->secure_field($idobrateatro));
                $r = $this->db->execute($query);
                deleteAllExtImages("img/obrasteatro/poster/".$idobrateatro);
                deleteAllExtImages("img/obrasteatro/poster/thumbs/".$idobrateatro);
                deleteAllExtImages("img/obrasteatro/poster/original/".$idobrateatro);
                deleteAllExtImages("img/obrasteatro/fanart/".$idobrateatro);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar una obra
     *
     * @param array $obra datos de la obra
     * @param string $obra['nombre'] nombre de la obra
     * @param int $obra['duracion'] duracion en minutos
     * @param string $obra['sinopsis']
     * @param string $obra['genero']
     * @param string $obra['video']
     * @param string $obra['poster']
     * @param string $obra['fanart']
     * @return int
     */
    public function addObra($obra) {
        if(!$this->checkObra("nombre", $obra["nombre"])) {
            $fields = "";
            $values = "";
            foreach($obra as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %sobrasteatro (%s, creado) VALUES (%s, NOW())", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addObra] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addObra] Ya existe la obra.", 1);
        }
    }

    /*
     * Actualiza los campos de una obra
     *
     * @param array $obra datos de la obra
     * @param int $obra['idobrateatro'] identificador de la obra
     * @param string $obra['nombre'] nombre de la obra
     * @param int $obra['duracion'] duracion en minutos
     * @param string $obra['sinopsis']
     * @param string $obra['genero']
     * @param string $obra['video']
     * @param string $obra['poster']
     * @param string $obra['fanart']
     * @return boolean
     */
    public function updateObra($obra) {
        if($this->checkObra("idobrateatro", $obra["idobrateatro"])) {
            $fields = "";
            foreach($obra as $key => $value) {
                if($key != "idobrateatro") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %sobrasteatro set %s, actualizado = NOW() where idobrateatro = %d", BDPREFIX, $fields, $this->db->secure_field($obra["idobrateatro"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateObra] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateObra] No existe la obra.", 1);
        }
    }

    /*
     * Publicar obras para que sean visibles
     *
     * @param int $ids ids de los obras
     * @return boolean
     */
    public function publishObra($ids) {
        $query = sprintf("update %sobrasteatro set publicado=1 where idobrateatro in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishObra] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar obras para que no sean visibles
     *
     * @param int $ids ids de los obras
     * @return boolean
     */
    public function unpublishObra($ids) {
        $query = sprintf("update %sobrasteatro set publicado=0 where idobrateatro in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishObra] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar una obra con un local
     *
     * @param array $obra
     * @param int $obra['idlugar']
     * @param int $obra['idobrateatro']
     * @param datetime $obra['fecha']
     * @param string $obra['urlcompra']
     * @param string $obra['precio']
     * @param string $obra['precioadelantada']
     * @return boolean
     */
    public function addHora($obra, $type) {
        if(!$this->checkRelLugarObra($obra,$type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_obrasteatro" ;
                    $idlugar = "idlocal";
                    break;
                case "teatro":
                    $tabla = "teatros_has_obrasteatro" ;
                    $idlugar = "idteatro";
                    break;
                case "cine":
                    $tabla = "cines_has_obrasteatro" ;
                    $idlugar = "idcine";
                    break;
                case "lugar":
                    $tabla = "lugares_has_obrasteatro";
                    unset($obra["idlugar"], $obra["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_obrasteatro" ;
                    $idlugar = "idmuseo";
                    break;
            }
            $fields = "";
            $values = "";
            foreach($obra as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                if($key == "idlugar") {
                    $key = $idlugar;
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %s (%s) VALUES (%s)", BDPREFIX.$tabla, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[addHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Coger las horas de una obra (opcionalmente) con una sala y con un local
     *
     * @param array $obra
     * @param int $obra['idlugar']
     * @param int (opcional) $obra['idobrateatro']
     * @return array
     */
    public function listHoras($obra) {
        $result = array("horasLocal" => array(), "horasAuditorio" => array(), "horasCine" => array(), "horasLugar" => array(), "horasMuseo" => array());

        //Locales
        $query = sprintf("select lt.*, l.nombre, l.direccion, l.localidad, l.coordenadas from %slocales_has_obrasteatro lt, %slocales l where lt.idobrateatro = %d and l.idlocal = lt.idlocal", BDPREFIX, BDPREFIX, $this->db->secure_field($obra["idobrateatro"]));
        if(isset($obra["idlugar"]) && !empty($obra["idlugar"])) {
            $query .= " and lt.idlocal = ".$this->db->secure_field($obra["idlugar"]);
        }
        $query .= " order by lt.idlocal, lt.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLocal"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Auditorios
        $query = sprintf("select tc.*, t.nombre, t.direccion, t.localidad, t.coordenadas from %steatros_has_obrasteatro tc, %steatros t where tc.idobrateatro = %d and t.idteatro = tc.idteatro", BDPREFIX, BDPREFIX, $this->db->secure_field($obra["idobrateatro"]));
        if(isset($obra["idlugar"]) && !empty($obra["idlugar"])) {
            $query .= " and lt.idteatro = ".$this->db->secure_field($obra["idlugar"]);
        }
        $query .= " order by tc.idteatro, tc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasAuditorio"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Cines
        $query = sprintf("select cp.*, c.nombre, s.numero, c.direccion, c.localidad, c.coordenadas from %scines_has_obrasteatro cp, %scines c, %ssalas s where cp.idobrateatro = %d and c.idcine = cp.idcine and s.idsala = cp.idsala", BDPREFIX, BDPREFIX, BDPREFIX, $this->db->secure_field($obra["idobrateatro"]));
        if(isset($obra["idsala"]) && !empty($obra["idsala"])) {
            $query .= " and cp.idsala = ".$this->db->secure_field($obra["idsala"]);
        }
        if(isset($obra["idlugar"]) && !empty($obra["idlugar"])) {
            $query .= " and cp.idcine = ".$this->db->secure_field($obra["idlugar"]);
        }
        $query .= " order by cp.idcine, cp.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasCine"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Lugar
        $query = sprintf("select lt.* from %slugares_has_obrasteatro lt where lt.idobrateatro = %d order by lt.fecha", BDPREFIX, $this->db->secure_field($obra["idobrateatro"]));
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLugar"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Museos
        $query = sprintf("select mt.*, m.nombre, m.direccion, m.localidad, m.coordenadas from %smuseos_has_obrasteatro mt, %smuseos m where mt.idobrateatro = %d and m.idmuseo = mt.idmuseo", BDPREFIX, BDPREFIX, $this->db->secure_field($obra["idobrateatro"]));
        if(isset($obra["idlugar"]) && !empty($obra["idlugar"])) {
            $query .= " and mt.idmuseo = ".$this->db->secure_field($obra["idmuseo"]);
        }
        $query .= " order by mt.idmuseo, mt.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasMuseo"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        return $result;
    }

    /*
     * Eliminar la relacion de una obra con un local
     *
     * @param array $obra
     * @param int $obra['idlugar']
     * @param int $obra['idobrateatro']
     * @param datetime $obra['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function deleteHora($obra,$type) {
        if($this->checkRelLugarObra($obra,$type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_obrasteatro" ;
                    $idlugar = "idlocal = ".$this->db->secure_field($obra["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_obrasteatro" ;
                    $idlugar = "idteatro = ".$this->db->secure_field($obra["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_obrasteatro" ;
                    $idlugar = "idcine = ".$this->db->secure_field($obra["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_obrasteatro" ;
                    $idlugar = "1";
                    break;
                case "museo":
                    $tabla = "museos_has_obrasteatro" ;
                    $idlugar = "idmuseo = ".$this->db->secure_field($obra["idlugar"]);
                    break;
            }
            $query = sprintf("delete from %s where idobrateatro = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($obra["idobrateatro"]), $idlugar, $this->db->secure_field($obra["fecha"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delHora] No existe la relacion.", 1);
        }
    }

    /*
     * Actualizar una relacion de una obra con un local/teatro
     *
     * @param array $obra
     * @param int $obra['idlugar']
     * @param int $obra['idobrateatro']
     * @param datetime $obra['fecha']
     * @param string $obra['urlcompra']
     * @param string $obra['precio']
     * @param string $obra['precioanticipada']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function updateHora($obra, $type) {
        if($this->checkRelLugarObra($obra, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_obrasteatro" ;
                    $idlugar = "idlocal = ".$this->db->secure_field($obra["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_obrasteatro" ;
                    $idlugar = "idteatro = ".$this->db->secure_field($obra["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_obrasteatro" ;
                    $idlugar = "idcine = ".$this->db->secure_field($obra["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_obrasteatro" ;
                    $idlugar = "1";
                    unset($obra["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_obrasteatro" ;
                    $idlugar = "idmuseo = ".$this->db->secure_field($obra["idlugar"]);
                    break;
            }
            $fields = "";
            $exclude = array("idlugar","idobrateatro","fecha");
            foreach($obra as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %s set %s where idobrateatro = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $fields, $this->db->secure_field($obra["idobrateatro"]), $idlugar, $this->db->secure_field($obra["fecha"]));
            if(isset($obra["idsala"])) {
                $query .= " and idsala = ".$this->db->secure_field($obra["idsala"]);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHora] No existe la relacion.", 1);
        }
    }

    /*
     * Comprueba si existe la obra
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkObra($field, $value) {
        $query = sprintf("select ot.* from %sobrasteatro ot where ot.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la obra en un horario y local/teatro determinados
     *
     * @param array $obra
     * @param int $obra['idlugar']
     * @param int $obra['idobrateatro']
     * @param datetime $obra['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return int|false
     */
    private function checkRelLugarObra($obra, $type) {
        switch($type){
            case "local":
                $tabla = "locales_has_obrasteatro";
                $idlugar = "idlocal = ".$this->db->secure_field($obra["idlugar"]);
                break;
            case "teatro":
                $tabla = "teatros_has_obrasteatro";
                $idlugar = "idteatro = ".$this->db->secure_field($obra["idlugar"]);
                break;
            case "cine":
                $tabla = "cines_has_obrasteatro";
                $idlugar = "idcine = ".$this->db->secure_field($obra["idlugar"]);
                break;
            case "lugar":
                $tabla = "lugares_has_obrasteatro";
                $idlugar = "1";
                break;
            case "museo":
                $tabla = "museos_has_obrasteatro" ;
                $idlugar = "idmuseo = ".$this->db->secure_field($obra["idlugar"]);
                break;
        }
        $query = sprintf("select * from %s where idobrateatro = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($obra["idobrateatro"]), $idlugar, $this->db->secure_field($obra["fecha"]));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END