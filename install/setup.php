<?php

function createdatabase($bdserver,$bduser,$bdpass,$bdname,$bdprefix) {
    $dbc = mysqli_connect($bdserver,$bduser,$bdpass,$bdname);
    if (!$dbc) {
		die('No se puede conectar: ' . mysqli_connect_errno());
	}

	$query = "CREATE DATABASE /*!32312 IF NOT EXISTS*/ `".$bdname."` /*!40100 DEFAULT CHARACTER SET utf8 */;";
	if (!mysqli_query($dbc,$query)) {
		die("<div id='error'><br>Error creando la base de datos: ".mysqli_error()."</div>");
	}

	$query = file_get_contents('hazplanes.sql');
    
    $query = str_replace("taskit_", $bdprefix, $query);
    
    $r = mysqli_multi_query($dbc,$query);

	mysqli_close($dbc);
    
    return $r;
}

include("../includes/config.inc.php");

$r = createdatabase(BDSERVER,BDUSER,BDPASS,BDNAME,BDPREFIX);

if($r) {
    echo "Instalado";
} else {
    echo "Hubo un fallo";
}

?>