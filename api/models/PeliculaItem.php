<?php
/**
 * Clase con metodos para operaciones con peliculas
 *
 * @package API
 * @author Trasno
 */
class PeliculaItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todas las peliculas
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de usuario. El nombre de usuario tiene que contener esta cadena.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listAgendaPeliculas($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "and ".$options["filter"];
        }
        $query = sprintf("select idpelicula, nombre, genero, poster, fechaestreno, fecha, infantil, agotado, cancelado, todoeldia, publicado from (

        select p.idpelicula, nombre, genero, poster, fechaestreno, cp.fecha, infantil, agotado, cancelado, todoeldia, publicado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %scines_has_peliculas order by fecha asc) as cp where cp.idpelicula = p.idpelicula %s group by date(cp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, fechaestreno, lp.fecha, infantil, agotado, cancelado, todoeldia, publicado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slocales_has_peliculas order by fecha asc) as lp where lp.idpelicula = p.idpelicula %s group by date(lp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, fechaestreno, tp.fecha, infantil, agotado, cancelado, todoeldia, publicado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %steatros_has_peliculas order by fecha asc) as tp where tp.idpelicula = p.idpelicula %s group by date(tp.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, fechaestreno, lup.fecha, infantil, agotado, cancelado, todoeldia, publicado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %slugares_has_peliculas order by fecha asc) as lup where lup.idpelicula = p.idpelicula %s group by date(lup.fecha), p.idpelicula union
        select p.idpelicula, nombre, genero, poster, fechaestreno, mp.fecha, infantil, agotado, cancelado, todoeldia, publicado from %speliculas p, (select idpelicula, fecha, agotado, cancelado, todoeldia from %smuseos_has_peliculas order by fecha asc) as mp where mp.idpelicula = p.idpelicula %s group by date(mp.fecha), p.idpelicula

        ) as consulta group by date(fecha), nombre
        order by %s %s", BDPREFIX, BDPREFIX, $options["filter"], BDPREFIX, BDPREFIX, $options["filter"], BDPREFIX, BDPREFIX, $options["filter"], BDPREFIX, BDPREFIX, $options["filter"], BDPREFIX, BDPREFIX, $options["filter"], $this->db->secure_field($options["order"]),$this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge todas las peliculas
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de pelicula.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listPeliculas($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf("select p.* from %speliculas p %s order by p.%s %s", BDPREFIX, $options["filter"], $this->db->secure_field($options["order"]), $this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($pelicula = $this->db->fetch($r)) {
            $result[] = $pelicula;
        }

        return $result;
    }

    /**
     * Coge una pelicula y todos sus datos (cines, salas, horarios, info...)
     *
     * @param int $idpelicula id de la pelicula
     * @return array|false
     */
    public function dataPelicula($idpelicula) {
        if(!empty($idpelicula)) {
            $query = sprintf("select p.* from %speliculas p where p.idpelicula = %d", BDPREFIX, $this->db->secure_field($idpelicula));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar una pelicula
     *
     * @param int $idpelicula id de la pelicula
     * @return boolean
     */
    public function deletePelicula($idpelicula) {
        if(!empty($idpelicula)) {
            $query = sprintf("delete from %scines_has_peliculas where idpelicula = %d",BDPREFIX, $this->db->secure_field($idpelicula));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slocales_has_peliculas where idpelicula = %d",BDPREFIX, $this->db->secure_field($idpelicula));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %steatros_has_peliculas where idpelicula = %d",BDPREFIX, $this->db->secure_field($idpelicula));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slugares_has_peliculas where idpelicula = %d",BDPREFIX, $this->db->secure_field($idpelicula));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %smuseos_has_peliculas where idpelicula = %d",BDPREFIX, $this->db->secure_field($idpelicula));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %speliculas where idpelicula = %d",BDPREFIX, $this->db->secure_field($idpelicula));
                $r = $this->db->execute($query);
                deleteAllExtImages("img/peliculas/poster/".$idpelicula);
                deleteAllExtImages("img/peliculas/poster/thumbs/".$idpelicula);
                deleteAllExtImages("img/peliculas/poster/original/".$idpelicula);
                deleteAllExtImages("img/peliculas/fanart/".$idpelicula);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar una pelicula
     *
     * @param array $pelicula datos de la pelicula
     * @param string $pelicula['nombre'] nombre de la pelicula
     * @param string $pelicula['original']
     * @param string $pelicula['director']
     * @param string $pelicula['actores']
     * @param string $pelicula['nacionalidad']
     * @param string $pelicula['distribuidora']
     * @param int $pelicula['edad'] edad minima del publico
     * @param int $pelicula['duracion'] duracion en minutos
     * @param string $pelicula['genero']
     * @param boolean $pelicula['vo']
     * @param string $pelicula['sinopsis']
     * @param string $pelicula['trailer']
     * @param string $pelicula['poster']
     * @param string $pelicula['fanart']
     * @param boolean $pelicula['publicado']
     * @return int
     */
    public function addPelicula($pelicula) {
        if(!$this->checkPelicula("nombre", $pelicula["nombre"])) {
            $fields = "";
            $values = "";
            foreach($pelicula as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %speliculas (%s, creado) VALUES (%s, NOW())", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addPelicula] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addPelicula] Ya existe la pelicula.", 1);
        }
    }

    /*
     * Actualiza los campos de una pelicula
     *
     * @param array $pelicula datos de la pelicula
     * @param int $pelicula['idpelicula'] identificador de la pelicula
     * @param string $pelicula['nombre'] nombre de la pelicula
     * @param string $pelicula['original']
     * @param string $pelicula['director']
     * @param string $pelicula['actores']
     * @param string $pelicula['nacionalidad']
     * @param string $pelicula['distribuidora']
     * @param int $pelicula['edad'] edad minima del publico
     * @param int $pelicula['duracion'] duracion en minutos
     * @param string $pelicula['genero']
     * @param boolean $pelicula['vo']
     * @param string $pelicula['sinopsis']
     * @param string $pelicula['trailer']
     * @param string $pelicula['poster']
     * @param string $pelicula['fanart']
     * @param boolean $pelicula['publicado']
     * @return boolean
     */
    public function updatePelicula($pelicula) {
        if($this->checkPelicula("idpelicula", $pelicula["idpelicula"])) {
            $fields = "";
            foreach($pelicula as $key => $value) {
                if($key != "idpelicula") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %speliculas set %s, actualizado = NOW() where idpelicula = %d", BDPREFIX, $fields, $this->db->secure_field($pelicula["idpelicula"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updatePelicula] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updatePelicula] No existe la pelicula.", 1);
        }
    }

    /*
     * Publicar peliculas para que sean visibles
     *
     * @param int $ids ids de las peliculas
     * @return boolean
     */
    public function publishPelicula($ids) {
        $query = sprintf("update %speliculas set publicado=1 where idpelicula in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishPelicula] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar peliculas para que no sean visibles
     *
     * @param int $ids ids de las peliculas
     * @return boolean
     */
    public function unpublishPelicula($ids) {
        $query = sprintf("update %speliculas set publicado=0 where idpelicula in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishPelicula] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar una pelicula con una sala y con un cine
     *
     * @param array $pelicula
     * @param int $pelicula['idlugar']
     * @param int (opcional) $pelicula['idsala']
     * @param int $pelicula['idpelicula']
     * @param datetime $pelicula['fecha']
     * @param boolean $pelicula['3d']
     * @param string $pelicula['urlcompra']
     * @param string $pelicula['precio']
     * @return boolean
     */
    public function addHora($pelicula, $type) {
        if(!$this->checkRelLugarPelicula($pelicula,$type)) {
            switch($type){
                case "cine":
                    $tabla = "cines_has_peliculas";
                    $idlugar = "idcine";
                    break;
                case "local":
                    $tabla = "locales_has_peliculas";
                    $idlugar = "idlocal";
                    break;
                case "teatro":
                    $tabla = "teatros_has_peliculas";
                    $idlugar = "idteatro";
                    break;
                case "lugar":
                    $tabla = "lugares_has_peliculas";
                    unset($pelicula["idlugar"]);
                    break;
                case "museo":
                    $tabla = "museos_has_peliculas";
                    $idlugar = "idmuseo";
                    break;
            }
            $fields = "";
            $values = "";
            foreach($pelicula as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                if($key == "idlugar") {
                    $key = $idlugar;
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %s (%s) VALUES (%s)", BDPREFIX.$tabla, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[addHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHora] Ya existe la relacion. Fecha: ".$pelicula["fecha"], 1);
        }
    }

    /*
     * Coger las horas de una pelicula (opcionalmente) con una sala y con un cine
     *
     * @param array $pelicula
     * @param int $pelicula['idcine']
     * @param int (opcional) $pelicula['idsala']
     * @param int (opcional) $pelicula['idpelicula']
     * @return array
     */
    public function listHoras($pelicula) {
        $result = array("horasCine" => array(), "horasLocal" => array(), "horasAuditorio" => array(), "horasLugar" => array(), "horasMuseo" => array());

        //Cines
        $query = sprintf("select cp.*, c.nombre, s.numero, c.direccion, c.localidad, c.coordenadas from %scines_has_peliculas cp, %scines c, %ssalas s where cp.idpelicula = %d and c.idcine = cp.idcine and s.idsala = cp.idsala", BDPREFIX, BDPREFIX, BDPREFIX, $this->db->secure_field($pelicula["idpelicula"]));
        if(isset($pelicula["idsala"]) && !empty($pelicula["idsala"])) {
            $query .= " and cp.idsala = ".$this->db->secure_field($pelicula["idsala"]);
        }
        if(isset($pelicula["idlugar"]) && !empty($pelicula["idlugar"])) {
            $query .= " and cp.idcine = ".$this->db->secure_field($pelicula["idlugar"]);
        }
        $query .= " order by cp.idcine, cp.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasCine"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        //Locales
        $query = sprintf("select cp.*, c.nombre, c.direccion, c.localidad, c.coordenadas from %slocales_has_peliculas cp, %slocales c where cp.idpelicula = %d and c.idlocal = cp.idlocal", BDPREFIX, BDPREFIX, $this->db->secure_field($pelicula["idpelicula"]));
        if(isset($pelicula["idlugar"]) && !empty($pelicula["idlugar"])) {
            $query .= " and cp.idlocal = ".$this->db->secure_field($pelicula["idlugar"]);
        }
        $query .= " order by cp.idlocal, cp.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLocal"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        //Teatros
        $query = sprintf("select cp.*, c.nombre, c.direccion, c.localidad, c.coordenadas from %steatros_has_peliculas cp, %steatros c where cp.idpelicula = %d and c.idteatro = cp.idteatro", BDPREFIX, BDPREFIX, $this->db->secure_field($pelicula["idpelicula"]));
        if(isset($pelicula["idlugar"]) && !empty($pelicula["idlugar"])) {
            $query .= " and cp.idteatro = ".$this->db->secure_field($pelicula["idlugar"]);
        }
        $query .= " order by cp.idteatro, cp.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasAuditorio"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        //Lugar
        $query = sprintf("select lt.* from %slugares_has_peliculas lt where lt.idpelicula = %d order by lt.fecha", BDPREFIX, $this->db->secure_field($pelicula["idpelicula"]));
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLugar"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        //Museos
        $query = sprintf("select mp.*, m.nombre, m.direccion, m.localidad, m.coordenadas from %smuseos_has_peliculas mp, %smuseos m where mp.idpelicula = %d and m.idmuseo = mp.idmuseo", BDPREFIX, BDPREFIX, $this->db->secure_field($pelicula["idpelicula"]));
        if(isset($pelicula["idlugar"]) && !empty($pelicula["idlugar"])) {
            $query .= " and mp.idmuseo = ".$this->db->secure_field($pelicula["idlugar"]);
        }
        $query .= " order by mp.idmuseo, mp.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasMuseo"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        return $result;
    }

    /*
     * Eliminar la relacion de una pelicula con una sala y con un cine
     *
     * @param array $pelicula
     * @param int $pelicula['idlugar']
     * @param int (opcional) $pelicula['idsala']
     * @param int $pelicula['idpelicula']
     * @param datetime $pelicula['fecha']
     * @return boolean
     */
    public function deleteHora($pelicula, $type) {
        if($this->checkRelLugarPelicula($pelicula, $type)) {
            switch($type){
                case "cine":
                    $tabla = "cines_has_peliculas";
                    $idlugar = "idcine = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
                case "local":
                    $tabla = "locales_has_peliculas";
                    $idlugar = "idlocal = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_peliculas";
                    $idlugar = "idteatro = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_peliculas";
                    $idlugar = "1";
                    break;
                case "museo":
                    $tabla = "museos_has_peliculas";
                    $idlugar = "idmuseo = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
            }
            $query = sprintf("delete from %s where idpelicula = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($pelicula["idpelicula"]), $idlugar, $this->db->secure_field($pelicula["fecha"]));
            if(isset($pelicula["idsala"])) {
                $query .= " and idsala = ".$this->db->secure_field($pelicula["idsala"]);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delHora] No existe la relacion.", 1);
        }
    }

    /*
     * Actualizar una relacion de una pelicula (con una sala y) con un cine/local/teatro
     *
     * @param array $pelicula
     * @param int $pelicula['idlugar']
     * @param int (opcional) $pelicula['idsala']
     * @param int $pelicula['idpelicula']
     * @param datetime $pelicula['fecha']
     * @param boolean $pelicula['3d']
     * @param string $pelicula['urlcompra']
     * @param string $pelicula['precio']
     * @return boolean
     */
    public function updateHora($pelicula, $type) {
        if($this->checkRelLugarPelicula($pelicula, $type)) {
            switch($type){
                case "cine":
                    $tabla = "cines_has_peliculas";
                    $idlugar = "idcine = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
                case "local":
                    $tabla = "locales_has_peliculas";
                    $idlugar = "idlocal = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_peliculas";
                    $idlugar = "idteatro = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_peliculas";
                    $idlugar = "1";
                    break;
                case "museo":
                    $tabla = "museos_has_peliculas";
                    $idlugar = "idmuseo = ".$this->db->secure_field($pelicula["idlugar"]);
                    break;
            }
            $fields = "";
            $exclude = array("idlugar","idsala","idpelicula","fecha");
            foreach($pelicula as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %s set %s where idpelicula = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $fields, $this->db->secure_field($pelicula["idpelicula"]), $idlugar, $this->db->secure_field($pelicula["fecha"]));

            if(isset($pelicula["idsala"])) {
                $query .= " and idsala = ".$this->db->secure_field($pelicula["idsala"]);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHora] No existe la relacion.", 1);
        }
    }

    /*
     * Comprueba si existe la pelicula
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkPelicula($field, $value) {
        $query = sprintf("select p.* from %speliculas p where p.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la pelicula en un horario, cine/local/teatro (y sala) determinados
     *
     * @param array $pelicula
     * @param int $pelicula['idlugar']
     * @param int (opcional) $pelicula['idsala']
     * @param int $pelicula['idpelicula']
     * @param datetime $pelicula['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return int|false
     */
    public function checkRelLugarPelicula($pelicula, $type) {
        switch($type){
            case "cine":
                $tabla = "cines_has_peliculas";
                $idlugar = "idcine = ".$this->db->secure_field($pelicula["idlugar"]);
                break;
            case "local":
                $tabla = "locales_has_peliculas";
                $idlugar = "idlocal = ".$this->db->secure_field($pelicula["idlugar"]);
                break;
            case "teatro":
                $tabla = "teatros_has_peliculas";
                $idlugar = "idteatro = ".$this->db->secure_field($pelicula["idlugar"]);
                break;
            case "lugar":
                $tabla = "lugares_has_peliculas";
                $idlugar = "1";
                break;
            case "museo":
                $tabla = "museos_has_peliculas";
                $idlugar = "idmuseo = ".$this->db->secure_field($pelicula["idlugar"]);
                break;
        }
        $query = sprintf("select * from %s where idpelicula = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($pelicula["idpelicula"]), $idlugar, $this->db->secure_field($pelicula["fecha"]));
        if(isset($pelicula["idsala"])) {
            $query .= " and idsala = ".$this->db->secure_field($pelicula["idsala"]);
        }
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END