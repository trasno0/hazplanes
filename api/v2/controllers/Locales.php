<?php
/**
 * Clase controlador de locales
 *
 * @package API
 * @author Trasno
 */
class Locales {
    private $params;
    private $general;
    private $localItem;
    //private $serviciosItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->localItem = new LocalItem($db);
        //$this->serviciosItem = new ServiciosItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->localItem = null;
    }

    /**
     * Crear local
     *
     * @return int
     */
    public function createAction() {
        $local = array();
        foreach($this->params as $key => $value){
            $local[$key] = $value;
        }

        //Cogemos los datos de los servicios para utilizarlos una vez creado el local
        /*if(isset($local["servicios"])) {
            $servicios = $local["servicios"];
            foreach($servicios as $k => $s) {
                $servicios[$k] = 1;
            }
        } else {
            $servicios = array();
        }*/

        //Cogemos los datos de las entradas para utilizarlos una vez creado el local
        if(isset($local["entradas"])) {
            $entradas = $local["entradas"];
        } else {
            $entradas = array();
        }

        unset($local["servicios"],$local["promos"],$local["entradas"]);
        $local["publicado"] = array_key_exists("publicado",$local)? 1:0;

        //Creamos el local
        $result = $this->localItem->addLocal($local);

        if($result) {
            //Añadir servicios al local
            //$servicios["idlocal"] = $result;
            //$this->serviciosItem->addServicios($servicios);
            //Añadir entradas
            foreach($entradas as $k => $entrada) {
                $this->localItem->setEntradas($result, $entrada);
            }
        }

        return $result;
    }

    /**
     * Recuperar local(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idlocal"]) && !empty($this->params["idlocal"])) {
            $data = $this->localItem->dataLocal($this->params["idlocal"]);
        } else {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "",
                "order" => "nombre"
                );
            if(!empty($this->params)) {
                $options = array_merge($options,$this->params);
            }
            $data = $this->localItem->listLocales($options);
        }

        return $data;
    }

    /**
     * Actualizar local
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->localItem->publishLocal($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->localItem->unpublishLocal($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "vote") {
                $result = $this->localItem->voteLocal($this->params["idlugar"],$this->params["points"]);
            } elseif(strtolower($this->params["op"]) == "field") {
                $fields = array();
                foreach($this->params as $key => $value){
                    $fields[$key] = $value;
                }
                unset($fields["op"]);
                $result = $this->localItem->updateLocal($fields);
            } else {
                $result = false;
            }
        } else {
            $local = array();
            foreach($this->params as $key => $value){
                $local[$key] = $value;
            }

            //Servicios
            /*if(isset($local["servicios"])) {
                $servicios = $local["servicios"];
                foreach($servicios as $k => $s) {
                    $servicios[$k] = 1;
                }
            } else {
                $servicios = array();
            }
            $servicios["idlocal"] = $local["idlocal"];
            $this->serviciosItem->updateServicios($servicios);*/

            //Entradas
            $bdRelLocalEntradas = $this->localItem->getEntradas($local["idlocal"]);
            if(isset($local["entradas"])) {
                $entradas = $local["entradas"];
            } else {
                $entradas = array();
            }
            foreach($entradas as $k => $entrada) {
                $existe = false;
                foreach($bdRelLocalEntradas as $key => $value) {
                    if($value["identradas"] == $entrada) {
                        $existe = true;
                        unset($bdRelLocalEntradas[$key]);
                        break;
                    }
                }
                if(!$existe) {
                    $this->localItem->setEntradas($local["idlocal"], $entrada);
                }
            }
            foreach($bdRelLocalEntradas as $s) {
                $this->localItem->delEntradas($local["idlocal"],$s["identradas"]);
            }

            unset($local["servicios"],$local["promos"],$local["entradas"],$bdRelLocalPromo,$promos,$bdRelLocalEntradas,$entradas);
            $local["publicado"] = array_key_exists("publicado",$local)? 1:0;

            //Local
            $result = $this->localItem->updateLocal($local);
        }

        return $result;
    }

    /**
     * Eliminar local
     *
     * @return boolean
     *
     * TODO: eliminar conciertos/obras del local a eliminar
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            //$result = $this->serviciosItem->deleteServicios($this->params["idlocal"]);
            $result = $this->localItem->delEntradas($this->params["id"]);
            if($result) {
                $result = $this->localItem->deleteLocal($this->params["id"]);
            }
        }

        return $result;
    }
}