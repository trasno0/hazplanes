<?php
/**
 * Clase con metodos para operaciones con salas de un cine
 *
 * @package API
 * @author Trasno  
 */
class SalaItem {
    protected $db;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }
    
    /**
     * Coge todas las salas de un cine
     *
     * @param int $idcine id del cine 
     * @param string $order (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listSalas($idcine, $order = "numero") {
        $query = sprintf("select s.* from %ssalas s where s.idcine = %d order by s.%s", BDPREFIX, $this->db->secure_field($idcine), $this->db->secure_field($order));
        $r = $this->db->query($query);
        
        $result = array();
        while($salas = $this->db->fetch($r)) {
            $result[] = $salas;
        }
        
        return $result;
    }
    
    /**
     * Coge las salas de un cine y todos sus datos
     *
     * @param int $idsala id de las salas 
     * @return array|false
     */
     public function dataSala($idsala) {
        if(!empty($idsala)) {
            $query = sprintf("select s.* from %ssalas s where s.idsala = %d", BDPREFIX, $this->db->secure_field($idsala));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }
     
    /*
     * Eliminar una sala
     *
     * @param int $idsala id de las salas
     * @return boolean
     */
    public function deleteSala($idsala) {
        if(!empty($idsala)) {
            $query = sprintf("delete from %ssalas where idsala = %d",BDPREFIX, $this->db->secure_field($idsala));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            }
        }
        return false;
    }
    
    /*
     * Eliminar las salas de un cine
     *
     * @param int $idcine id del cine
     * @return boolean
     */
    public function delSalasCine($idcine) {
        if(!empty($idcine)) {
            $query = sprintf("delete from %scines_has_peliculas where idcine = %d",BDPREFIX, $this->db->secure_field($idcine));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %ssalas where idcine = %d",BDPREFIX, $this->db->secure_field($idcine));
                $r = $this->db->execute($query);
                if($r) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /*
     * Insertar salas de un cine
     *
     * @param array $salas datos del salas
     * @param int $salas['idcine'] identificador del cine
     * @param int $salas['numero']
     * @param int $salas['capacidad']
     * @param int $salas['filas']
     * @param boolean $salas['numerada']
     * @return int
     */
    public function addSala($salas) {
        if(!$this->checkSala($salas["idsala"])) {
            $fields = "";
            $values = "";
            foreach($salas as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %ssalas (%s) VALUES (%s)", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addSala] Error en la query: ".$query, 1);    
            }
        } else {
            throw new Exception("[addSala] Ya existe esa sala para ese cine.", 1);
        }
    }
    
    /*
     * Actualiza los campos de salas de un cine
     *
     * @param array $salas datos de sala
     * @param int $salas['idsala'] identificador de sala
     * @param int $salas['idcine'] identificador del cine
     * @param int $salas['numero']
     * @param int $salas['capacidad']
     * @param int $salas['filas']
     * @param boolean $salas['numerada']
     * @return boolean
     */
    public function updateSala($salas) {
        if($this->checkSala($salas["idsala"])) {
            $fields = "";
            foreach($salas as $key => $value) {
                if($key != "idsala" && $key != "idcine") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %ssalas set %s where idcine = %d and idsala = %d", BDPREFIX, $fields, $this->db->secure_field($salas["idcine"]), $this->db->secure_field($salas["idsala"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateSala] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateSala] No existen la sala para ese cine.", 1);
        }
    }
    
    /*
     * Comprueba si existen las salas de un cine
     * 
     * @param int $idcine id del cine a comprobar
     * @param int $numsala numero de la sala en el cine
     * @return int|false
     */
    private function checkSala($idsala) {
        $query = sprintf("select s.* from %ssalas s where s.idsala = %d", BDPREFIX, $this->db->secure_field($idsala));
        $r = $this->db->query($query);
        
        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END