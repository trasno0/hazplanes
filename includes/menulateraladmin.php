    <?php
    if($p == "adminscraperjsontobd") {
        if (isset($_GET['pag'])) { // GET
            $pag = $_GET['pag'];
        } elseif (isset($_POST['pag'])) { // Forms
            $pag = $_POST['pag'];
        } else {
            $pag = 1;
        }

        $peliculas = json_decode(file_get_contents(BASE_URI.'/temp/results.json'), true);
        if(isset($peliculas["pelicula"])) {
            $peliculas = $peliculas["pelicula"];
            $totalPelis = count($peliculas);
            $menupeliculas = '<div id="menu" style="height: auto; padding-bottom: 60px; line-height: 12px;">
                    <h2>Películas</h2><br>
                    <div>';
            $cont = 1;
            if($totalPelis > 0) {
                foreach($peliculas as $titulo => $horas) {
                    if($cont == $pag) {
                        $color = "color:red;";
                    } else {
                        $color = "color: black;";
                    }
                    $menupeliculas .= "<a href='index.php?p=adminscraperjsontobd&pag=".$cont."' style='font-size: 10px; ".$color."'>".$cont." - ".$titulo."</a><br>";
                    $cont++;
                }
                $cont = 1;
                foreach($peliculas as $titulo => $horas) {
                    if($cont < $pag) {
                        $cont++;
                        continue;
                    }
                    break;
                }
            }
            $menupeliculas .= "</div>
                </div>";
        } else {
            $menupeliculas = "No hay peliculas";
            $totalPelis = 0;
        }
    }
    if($p == "adminscraperjsonestrenostobd") {
        if (isset($_GET['pag'])) { // GET
            $pag = $_GET['pag'];
        } elseif (isset($_POST['pag'])) { // Forms
            $pag = $_POST['pag'];
        } else {
            $pag = 1;
        }

        $peliculas = json_decode(file_get_contents(BASE_URI.'/temp/estrenos.json'), true);
        if(isset($peliculas[0])) {
            $totalPelis = count($peliculas);
            $menupeliculas = '<div id="menu" style="height: auto; padding-bottom: 60px; line-height: 12px;">
                    <h2>Películas</h2><br>
                    <div>';
            $cont = 1;
            if($totalPelis > 0) {
                foreach($peliculas as $pel) {
                    $titulo = $pel["nombre"];
                    if($cont == $pag) {
                        $color = "color:red;";
                    } else {
                        $color = "color: black;";
                    }
                    $menupeliculas .= "<a href='index.php?p=adminscraperjsonestrenostobd&pag=".$cont."' style='font-size: 10px; ".$color."'>".$cont." - ".$titulo."</a><br>";
                    $cont++;
                }
                $cont = 1;
                foreach($peliculas as $pel) {
                    $titulo = $pel["nombre"];
                    $estreno = $pel["fecha"];
                    if($cont < $pag) {
                        $cont++;
                        continue;
                    }
                    break;
                }
            }
            $menupeliculas .= "</div>
                </div>";
        } else {
            $menupeliculas = "No hay peliculas";
            $totalPelis = 0;
        }
    }
    if($p == "adminscraperjsontobdservinova") {
        if (isset($_GET['pag'])) { // GET
            $pag = $_GET['pag'];
        } elseif (isset($_POST['pag'])) { // Forms
            $pag = $_POST['pag'];
        } else {
            $pag = 1;
        }

        $eventos = json_decode(file_get_contents(BASE_URI.'/temp/servinova.json'), true);
        $totalEventos = count($eventos);
        if($totalEventos > 1) {
            $menueventos = '<div id="menu" style="height: auto; padding-bottom: 60px; line-height: 12px;">
                    <h2>Eventos</h2><br>
                    <div>';
            $cont = 1;
            if($totalEventos > 1) {
                foreach($peliculas as $pel) {
                    $titulo = $pel["nombre"];
                    if($cont == $pag) {
                        $color = "color:red;";
                    } else {
                        $color = "color: black;";
                    }
                    $menueventos .= "<a href='index.php?p=adminscraperjsonestrenostobd&pag=".$cont."' style='font-size: 10px; ".$color."'>".$cont." - ".$titulo."</a><br>";
                    $cont++;
                }
                $cont = 1;
                foreach($peliculas as $pel) {
                    $titulo = $pel["nombre"];
                    $estreno = $pel["fecha"];
                    if($cont < $pag) {
                        $cont++;
                        continue;
                    }
                    break;
                }
            }
            $menueventos .= "</div>
                </div>";
        } else {
            $menueventos = "No hay eventos";
            $totalEventos = 0;
        }
    }
    ?>
    <nav id="menulateral">
        <ul>
            <h2>Administración</h2>
            <ul>
                <li><a href="index.php?p=admindashboard">Inicio</a></li>
                <li><a href="index.php?p=adminsharelinks">Share</a></li>
                <li><a href="index.php?p=adminscraperwebtojson">Scraper a JSON</a></li>
                <li><a href="index.php?p=adminscraperjsontobd">JSON a BD - Películas</a></li>
                <li><a href="index.php?p=adminscraperjsonestrenostobd">JSON a BD - Estrenos</a></li>
                <li><a href="index.php?p=adminscraperjsontobdservinova">JSON a BD - Servinova</a></li>
            </ul>
            <?php if(isset($menupeliculas)) echo $menupeliculas; ?>
            <?php if(isset($menueventos)) echo $menueventos; ?>
            <ul>
                <li><a href="index.php?p=adminlistfilm">Películas</a></li>
                <li><a href="index.php?p=adminlistconcert">Conciertos</a></li>
                <li><a href="index.php?p=adminlistteatro">Obras de teatro</a></li>
                <li><a href="index.php?p=adminlisteventos">Eventos</a></li>
                <li><a href="index.php?p=adminlistcursos">Cursos</a></li>
                <li><a href="index.php?p=adminlistdeportes">Competiciones</a></li>
                <li><a href="index.php?p=adminlistexposiciones">Exposiciones</a></li>
            </ul>
            <ul>
                <li><a href="index.php?p=adminlistcine">Cines</a></li>
                <li><a href="index.php?p=adminlistlocal">Locales</a></li>
                <li><a href="index.php?p=adminlisttheater">Auditorios</a></li>
                <li><a href="index.php?p=adminlistmuseos">Museos</a></li>
                <li><a href="index.php?p=adminlistpabellones">Pabellones</a></li>
                <li><a href="index.php?p=adminlisttiposdeporte">Tipos de deporte</a></li>
            </ul>
            <ul>
                <li><a href="index.php?p=adminlistpromo">Promociones</a></li>
                <li><a href="index.php?p=adminlistentradas">Web de entradas</a></li>
            </ul>
            <div id='searchbox'>
                <form>
                    <?php if(strpos($p,"list") !== false) { ?>
                    <script>$( function() { $("#search-field").focus(); });</script>
                    <input type='text' id='search-field' class='campo medium' style='width:158px;' value='' placeholder="Buscar..."/>
                    <?php } ?>
                </form>
            </div>
        </ul>
    </nav>