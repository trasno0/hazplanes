<?php
/**
 * Clase controlador de entradas
 *
 * @package API
 * @author Trasno
 */
class Entradas {
    private $params;
    private $general;
    private $entradasItem;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->entradasItem = new EntradasItem($db);
    }
    
    /**
     * Destructor
     *
     * @return void
     * @author trasno 
     */
    public function __destruct() {
        $this->entradasItem = null;
    }
    
    /**
     * Crear entradas
     *
     * @return int
     */
    public function createAction() {
        $entradas = array();
        foreach($this->params as $key => $value){
            $entradas[$key] = $value; 
        }
        $entradas["publicado"] = array_key_exists("publicado",$entradas)? 1:0;
        
        $result = $this->entradasItem->addEntradas($entradas);
        
        return $result;
    }
    
    /**
     * Recuperar entradas(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["identradas"]) && !empty($this->params["identradas"])) {
            $data = $this->entradasItem->dataEntradas($this->params["identradas"]);
        } else {
            if(isset($this->params["idcine"]) && !empty($this->params["idcine"])) {
                $data = $this->entradasItem->listEntradas($this->params["idcine"]);
            } else {
                $data = $this->entradasItem->listEntradas();
            }
        }
        
        return $data;
    }
    
    /**
     * Actualizar entradas
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->entradasItem->publishEntradas($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->entradasItem->unpublishEntradas($this->params["ids"]);
            } else {
                $result = false;
            }
        } else {
            $entradas = array();
            foreach($this->params as $key => $value){
                $entradas[$key] = $value; 
            }
            $entradas["publicado"] = array_key_exists("publicado",$entradas)? 1:0;
            
            $result = $this->entradasItem->updateEntradas($entradas);
        }
        
        return $result;
    }
    
    /**
     * Eliminar entradas
     *
     * @return boolean
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->entradasItem->deleteEntradas($this->params["id"]);
        }
        
        return $result;
    }
}