    <nav id="menulateral">
        <div id='secciones'>
            <ul>
                <li class='<?php echo $menuSel[0]; ?>'><div id='main-icon'></div><a href="<?php echo BASE_URL; ?>main"><span>Hoy</span></a></li>
                <li class='<?php echo $menuSel[13]; ?>'><div id='novedades-icon'></div><a href="<?php echo BASE_URL; ?>novedades"><span>Novedades</span></a></li>
                <li class='<?php echo $menuSel[14]; ?>'><div id='lugares-icon'></div><a href="<?php echo BASE_URL; ?>lugares"><span>Lugares</span></a></li>
                <li class='<?php echo $menuSel[15]; ?>'><div id='conreserva-icon'></div><a href="<?php echo BASE_URL; ?>conreserva"><span>Con reserva</span></a></li>
            </ul>
            <ul>
                <li class='mcartelera <?php echo $menuSel[1]; ?>'><div id='cartelera-icon'></div><a href="<?php echo BASE_URL; ?>cartelera"><span>Películas</span></a></li>
                <li class='mconciertos <?php echo $menuSel[2]; ?>'><div id='conciertos-icon'></div><a href="<?php echo BASE_URL; ?>conciertos"><span>Conciertos</span></a></li>
                <li class='mobrasteatro <?php echo $menuSel[3]; ?>'><div id='obrasteatro-icon'></div><a href="<?php echo BASE_URL; ?>obrasteatro"><span>Espectáculos</span></a></li>
                <li class='mexposiciones <?php echo $menuSel[5]; ?>'><div id='exposiciones-icon'></div><a href="<?php echo BASE_URL; ?>exposiciones"><span>Exposiciones</span></a></li>
                <li class='mdeportes <?php echo $menuSel[9]; ?>'><div id='deportes-icon'></div><a href="<?php echo BASE_URL; ?>deportes"><span>Deportes</span></a></li>
                <li class='minfantil <?php echo $menuSel[11]; ?>'><div id='infantil-icon'></div><a href="<?php echo BASE_URL; ?>infantil"><span>Para niños</span></a></li>
                <li class='mformacion <?php echo $menuSel[12]; ?>'><div id='formacion-icon'></div><a href="<?php echo BASE_URL; ?>formacion"><span>Cursos/Charlas</span></a></li>
                <li class='meventos <?php echo $menuSel[4]; ?>'><div id='eventos-icon'></div><a href="<?php echo BASE_URL; ?>eventos"><span>Eventos</span></a></li>
            </ul>
            <ul>
                <li class='<?php echo $menuSel[6]; ?>'><div id='upload-icon'></div><a href="<?php echo BASE_URL; ?>upload"><span>Subir evento</span></a></li>
                <li class='<?php echo $menuSel[7]; ?>'><div id='contacto-icon'></div><a href="<?php echo BASE_URL; ?>contacto"><span>Contacto</span></a></li>
                <li class='<?php echo $menuSel[8]; ?>'><div id='acercade-icon'></div><a href="<?php echo BASE_URL; ?>acercade"><span>Acerca de...</span></a></li>
                <!--<li class='<?php echo $menuSel[10]; ?>'><div id='encuesta'></div><a href="<?php echo BASE_URL; ?>encuesta"><span>Encuesta</span></a></li>-->
            </ul>
            <?php echo $publiMenu; ?>
            <ul class='credits'>
                <li>Iconos por <a href="http://icons8.com/" target="_blank">icons8</a></li>
                <li><a href='politica-cookies'>Política de cookies</a></li>
                <li><?php echo date("Y",strtotime("today")) ?> &copy; <?php echo PAGENAME." ".VERSION; ?></li>
                <li>By <a href="https://apercloud.es" target="_blank">AperCloud</a></li>
                <li><a href="https://elcambiador.es" target="_blank">elCambiador.es</a></li>
                <li><a href="https://hautaka.com" target="_blank">hautaka.com</a></li>
            </ul>
        </div>
    </nav>
